#ifndef __SPHFLUID_H__
#define __SPHFLUID_H__

#include "vector_types.h"


class SPHFluid
{
	private:
		// simulation parameters
		float dt;
		float kappa;
		float rho0;
		float mass;
		float h, h2, h6, h9;

		// particle grid 
		int numParticles, numCells, numVoxelVertices;
		int octtreeBufferSize;
		int3 particleGridDim;
		float3 particleGridSize;

		// host variables
		float4 *h_pos;
		float4 *h_vel;
		float4 *h_acc;
		float *h_rho, *h_rho0, *h_pres;
		float *h_output;
		int *h_cellStartIndex;
		int *h_cellEndIndex;
		int *h_cellIndex;
		int *h_particleIndex;
		int *h_voxelVertexFlags;
		float *h_voxelVertexValues;
		float4 *h_spos;
		float4 *h_svel;

		float4 *h_minPos;
		float4 *h_maxPos;
		int *h_child;
		int *h_start;
		int *h_sorted;
		int *h_count;


		// device variables
		float4 *d_pos;
		float4 *d_vel;
		float4 *d_acc;
		float *d_rho, *d_rho0, *d_pres;
		float *d_output;
		int *d_cellStartIndex;
		int *d_cellEndIndex;
		int *d_cellHash;
		int *d_particleIndex;
		int *d_voxelVertexFlags;
		float *d_voxelVertexValues;
		float4 *d_spos;
		float4 *d_svel;

		float4 *d_minPos;
		float4 *d_maxPos;
		float3 *d_minStack;
		float3 *d_maxStack;
		int *d_nodeStack;
		int *d_mutex;
		int *d_pindex;
		int *d_child;
		int *d_start;
		int *d_sorted;
		int *d_count;

		// used for timing
		float elapsedTime;
		cudaEvent_t start, stop;


	public:
		SPHFluid();
		SPHFluid(int numParticles, int3 particleGridDim, float3 particleGridSize);
		SPHFluid(const SPHFluid &spf);
		SPHFluid& operator=(const SPHFluid &spf);
		~SPHFluid();

		void update();
		void updateDensityOnly();
		void setInitialCondition(float3 *pos);
		void initialCondition();

		int GetNumberOfParticles();
		int GetNumberOfVoxels();
		int GetNumberOfVoxelVertices();
		float GetTimestep();
		float GetGasStiffness();
		float GetParticleRestDensity();
		float GetParticleMass();
		float GetVoxelWidth();
		float4* GetPosition();
		float4* GetVelocity();
		float* GetDensity();
		float* GetTighlyPackedPosition();
		float* GetVoxelVertexValues();
		float GetElapsedTime();
		size_t GetDeviceMemoryUsed();

	private:
		void allocateMemory();
		void deallocateMemory();

		void UniformFiniteGridAlgorithm();
		void UniformFiniteGridDensityOnly();
		void OcttreeInfiniteDomainAlgorithm();
};

#endif