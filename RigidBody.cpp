#include "RigidBody.h"


RigidBody::RigidBody(std::string objFilePath)
{
    if(!objParser.loadObj(objFilePath)){
        std::cout << "Failed to load obj file for rigidbody" << std::endl;
    }
}


RigidBody::~RigidBody()
{
}


void RigidBody::processEvents(sf::Event event)
{

}


void RigidBody::start()
{
    vertices = objParser.getVertices();

    int numVertices = vertices.size() / 3;

    glm::vec3 centre = glm::vec3(0.0f, 0.0f, 0.0f);
    for(int i = 0; i < numVertices; i++){
        centre.x += vertices[3*i];
        centre.y += vertices[3*i+1];
        centre.z += vertices[3*i+2];
    }
    centre /= numVertices;
    centre = glm::vec3(model*glm::vec4(centre, 1.0f));

    float maxRadius = 0.0f;
    for(int i = 0; i < numVertices; i++){
        glm::vec4 temp;
        temp.x = vertices[3*i];
        temp.y = vertices[3*i+1];
        temp.z = vertices[3*i+2];
        temp.w = 1.0f;

        temp = model*temp;

        float dx = (centre.x - temp.x);
        float dy = (centre.y - temp.y);
        float dz = (centre.z - temp.z);
        float radius2 = dx*dx + dy*dy + dz*dz;  
        if(maxRadius < radius2){
            maxRadius = radius2;
        }
    }

    Shader shader("shaders/directional.vs", "shaders/directional.frag");

    renderData.drawUsage = GL_TRIANGLES;
    renderData.drawMode = GL_DYNAMIC_DRAW;
    renderData.shader = shader;
    renderData.material = material;

    renderData.centre = centre;
    renderData.radius = sqrt(maxRadius);
    renderData.positions = objParser.getVertices();
    renderData.normals = objParser.getNormals();
    renderData.texCoords = objParser.getTextures();

    renderData.model = model;
}


void RigidBody::update()
{
    renderData.positions = vertices;
}


RenderData& RigidBody::getRenderData()
{
    return renderData;
}

int RigidBody::getNumParticles(){ return numParticles; }