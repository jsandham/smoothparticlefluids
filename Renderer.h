#ifndef __RENDERER_H__
#define __RENDERER_H__

#include <vector>

#include "external/stb_image/stb_image.h"

#define GLM_FORCE_RADIANS

#include "../../glm/glm/glm.hpp"
#include "../../glm/glm/gtc/matrix_transform.hpp"
#include "../../glm/glm/gtc/type_ptr.hpp"

#include "OctTree.h"
#include "Light.h"
#include "RenderData.h"


class Renderer
{
	private:
		//std::vector<GLuint> dirLightDepthMapFBO;
		//std::vector<GLuint> dirLightDepthMapTex;
		GLenum framebufferStatus;
		GLenum error;

		GLuint depthMapFBO;
		GLuint depthCubeMapFBO;
		GLuint depthMapTex;
		GLuint depthCubeMapTex;
		Shader depthShader;
		Shader debugDepthShader;

		std::vector<DirectionalLight*> directionalLights;
		std::vector<PointLight*> pointLights;
		std::vector<SpotLight*> spotLights;
		std::vector<AreaLight*> areaLights;
		std::map<std::string, RenderData*> renderData;

	public:
		glm::mat4 view;
		glm::mat4 projection;
		glm::vec3 cameraPos;
		// glm::vec3 backgroundColor;

	public:
		Renderer();
		~Renderer();

		void init();
		void render();

		void add(DirectionalLight *light);
		void add(PointLight *light);
		void add(SpotLight *light);
		void add(AreaLight *light);
		void add(std::string name, RenderData *data);
		void remove(std::string name);
};

#endif