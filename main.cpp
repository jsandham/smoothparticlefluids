#include <iostream>

#include "Scene.h"
#include "Light.h"
#include "Fluid.h"
#include "RigidBody.h"

#include "external/glm/glm/glm.hpp"
#include "external/glm/glm/gtc/matrix_transform.hpp"
#include "external/glm/glm/gtx/transform.hpp"
#include "external/glm/glm/gtc/type_ptr.hpp"


int main()
{
	Scene scene;

	glm::mat4 scale;
	glm::mat4 rotate;
	glm::mat4 translate;

	DirectionalLight directionalLight;
	directionalLight.direction = glm::vec3(0.0f, -4.0f, -1.0f);
	scene.add("directional light", &directionalLight);

	// PointLight pointLight;
	// pointLight.position = glm::vec3(1.19f, 4.74f, 0.5f);
	// scene.add("point light", &pointLight);

	// SpotLight spotLight;
	// spotLight.position = glm::vec3(1.0f, 6.0f, 1.5f);
	// spotLight.direction = glm::vec3(0.0f, -1.3f, -0.1f);
	// scene.add("spot light", &spotLight);

	// SpotLight spotLight2;
	// spotLight2.position = glm::vec3(1.0f, 2.0f, 2.0f);
	// spotLight2.direction = glm::vec3(-1.0f, -1.5f, -1.0f);
	// scene.add("spot light2", &spotLight2);

	// AreaLight areaLight;
	// scene.add("area light", &areaLight);

	// materials
	Material gold;
	gold.texFilename = "textures/gold.png";
	gold.shininess = 51.2f;
	gold.ambient = glm::vec3(0.247f, 0.199f, 0.0745f);
	gold.diffuse = glm::vec3(0.752f, 0.606f, 0.226f);
	gold.specular = glm::vec3(0.628f, 0.555f, 0.366f);

	// Material particle;
	// particle.texFilename = "textures/gold.png";
	// particle.shininess = 51.2f;
	// particle.ambient = glm::vec3(0.247f, 0.199f, 0.0745f);
	// particle.diffuse = glm::vec3(0.752f, 0.606f, 0.226f);
	// particle.specular = glm::vec3(0.628f, 0.555f, 0.366f);

	Fluid fluid("external/obj/cube.obj");
	// fluid.color = glm::vec3(1.0f, 0.27f, 1.0f);
	scale = glm::scale(glm::mat4(1.0f), glm::vec3(1.0f, 1.0f, 1.0f));
	translate = glm::translate(glm::mat4(1.0f), glm::vec3(-1.0f, 0.0f, 0.0f));
	fluid.model = translate*scale;
	fluid.material = gold;
	scene.add("fluid", &fluid);

	// Particles particles("external/obj/cube.obj");
	// //particles.entityColor = glm::vec3(1.0f, 0.27f, 1.0f);
	// scale = glm::scale(glm::mat4(1.0f), glm::vec3(1.0f, 1.0f, 1.0f));
	// translate = glm::translate(glm::mat4(1.0f), glm::vec3(-1.0f, 0.0f, 0.0f));
	// particles.model = translate*scale;
	// scene.add("particles", &particles);

	RigidBody teapot("external/obj/teapot.obj");
	scale = glm::scale(glm::mat4(1.0f), glm::vec3(0.1f, 0.1f, 0.1f));
	rotate = glm::rotate(glm::mat4(1.0f), 1.5f, glm::vec3(1.0f, 0.0f, 0.0f));
	translate = glm::translate(glm::mat4(1.0f), glm::vec3(1.0f, 2.0f, 0.2f));
	teapot.model = translate*rotate*scale;
	teapot.material = gold;
	scene.add("teapot", &teapot);

	RigidBody suzanne("external/obj/suzanne.obj");
	scale = glm::scale(glm::mat4(1.0f), glm::vec3(0.5f, 0.5f, 0.5f));
	rotate = glm::rotate(glm::mat4(1.0f), 1.5f, glm::vec3(1.0f, 0.0f, 0.0f));
	translate = glm::translate(glm::mat4(1.0f), glm::vec3(-0.4f, 3.0f, 0.8f));
	suzanne.model = translate*rotate*scale;
	scene.add("suzanne", &suzanne);

	// RigidBody bunny("external/obj/bunny2.obj");
	// scale = glm::scale(glm::mat4(1.0f), glm::vec3(4.0f, 4.0f, 4.0f));
	// rotate = glm::rotate(glm::mat4(1.0f), 1.5f, glm::vec3(1.0f, 0.0f, 0.0f));
	// translate = glm::translate(glm::mat4(1.0f), glm::vec3(-1.5f, 1.5f, 0.0f));
	// bunny.model = translate*rotate*scale;
	// scene.add("bunny", &bunny);

	// RigidBody dragon("external/obj/dragon.obj");
	// scale = glm::scale(glm::mat4(1.0f), glm::vec3(1.0f, 1.0f, 1.0f));
	// rotate = glm::rotate(glm::mat4(1.0f), 1.5f, glm::vec3(1.0f, 0.0f, 0.0f));
	// translate = glm::translate(glm::mat4(1.0f), glm::vec3(-1.0f, 0.5f, 0.0f));
	// dragon.model = translate*rotate*scale;
	// scene.add("dragon", &dragon);

	scene.run();
}
