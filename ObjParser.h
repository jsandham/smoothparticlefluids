#ifndef __OBJPARSER_H__
#define __OBJPARSER_H__

#include <string>
#include <vector>


class ObjParser
{
	private:
		// vertex data
		std::vector<float> v;
		std::vector<float> vt;
		std::vector<float> vn;

		// element data
		std::vector<int> p;
		std::vector<int> l;
		std::vector<int> f_v;
		std::vector<int> f_vt;
		std::vector<int> f_vn;


		std::vector<float> vertices;
		std::vector<float> textures;
		std::vector<float> normals;


	public:
		ObjParser();
		~ObjParser();

		bool loadObj(std::string filename);
		
		std::vector<float> getVertices();
		std::vector<float> getTextures();
		std::vector<float> getNormals();
};

#endif