#ifndef __SHAPE_H__
#define __SHAPE_H__

#define GLM_FORCE_RADIANS

#include "external/glm/glm/glm.hpp"
#include "external/glm/glm/gtc/matrix_transform.hpp"
#include "external/glm/glm/gtc/type_ptr.hpp"


typedef struct Sphere
{
	glm::vec3 position;
	float radius;
	Sphere(glm::vec3 position, float radius) : position(position), radius(radius)
	{
	}
}Sphere;


typedef struct Box
{
	glm::vec3 position;
	float width;
	float height;
	float depth;
	Box(glm::vec3 position, float width, float height, float depth) : position(position), width(width), height(height), depth(depth)
	{
	}
}Box;


typedef struct Cylinder
{
	glm::vec3 position;
	float height;
	float radius;
	Cylinder(glm::vec3 position, float height, float radius) : position(position), height(height), radius(radius)
	{
	}
}Cylinder;


typedef struct Cone
{
	glm::vec3 position;
	float height;
	float radius;
	Cone(glm::vec3 position, float height, float radius) : position(position), height(height), radius(radius)
	{
	}
}Cone;


#endif