#ifndef __OCTTREE_H__
#define __OCTTREE_H__

#define GLM_FORCE_RADIANS

#include "external/glm/glm/glm.hpp"
#include "external/glm/glm/gtc/matrix_transform.hpp"
#include "external/glm/glm/gtx/transform.hpp"
#include "external/glm/glm/gtc/type_ptr.hpp"

class Node
{
	public:
		glm::vec3 point;
		Node* children[8];
		
		Node(glm::vec3 point);
};


class OctTree
{
	private:
		glm::vec3 minAABBPoint;
		glm::vec3 maxAABBPoint;
		Node *root;

	public:
		OctTree(glm::vec3 min, glm::vec3 max);
		OctTree(const OctTree &tree);
		OctTree& operator=(const OctTree &tree);
		~OctTree();

		void insert(glm::vec3 point);

	private:
		void deleteTree();
		int ComputeQuadrant(glm::vec3 min, glm::vec3 max, glm::vec3 point);

};

#endif