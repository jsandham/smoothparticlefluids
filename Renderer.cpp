#include <cstddef>

#include "Renderer.h"

const unsigned int SHADOW_WIDTH = 1200, SHADOW_HEIGHT = 1200;


Renderer::Renderer()
{
	depthShader.compile("shaders/depth.vs", "shaders/depth.frag");
	debugDepthShader.compile("shaders/debugDepth.vs", "shaders/debugDepth.frag");
}

Renderer::~Renderer()
{
}


void Renderer::add(DirectionalLight *light)
{
	directionalLights.push_back(light);
}


void Renderer::add(PointLight *light)
{
	pointLights.push_back(light);
}


void Renderer::add(SpotLight *light)
{
	spotLights.push_back(light);
}	


void Renderer::add(AreaLight *light)
{
	areaLights.push_back(light);
}


void Renderer::add(std::string name, RenderData *data)
{
	renderData[name] = data;
}


void Renderer::remove(std::string name)
{
	renderData.erase(name);
}


void Renderer::init()
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);
	glEnable(GL_TEXTURE_CUBE_MAP);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// create depth map texture for shadow mapping
	glGenTextures(1, &depthMapTex);
	glBindTexture(GL_TEXTURE_2D, depthMapTex);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 
	glBindTexture(GL_TEXTURE_2D, 0); 

	// create depth map frame buffer and attach shadow mapping depth texture
	glGenFramebuffers(1, &depthMapFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMapTex, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	if((framebufferStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER)) != GL_FRAMEBUFFER_COMPLETE){
		std::cout << "ERROR: FRAMEBUFFER IS NOT COMPLETE" << framebufferStatus << std::endl;
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0); 


	// create depth cube map texture for shadow mapping with point lights
	glGenTextures(1, &depthCubeMapTex);
	glBindTexture(GL_TEXTURE_CUBE_MAP, depthCubeMapTex);
	for(unsigned int i = 0; i < 6; i++){
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); 
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	// create depth cube map frame buffer and attach shadow mapping depth texture
	// glGenFramebuffers(1, &depthCubeMapFBO);
	// glBindFramebuffer(GL_FRAMEBUFFER, depthCubeMapFBO);
	// glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthCubeMapTex, 0);
	// glDrawBuffer(GL_NONE);
	// glReadBuffer(GL_NONE);
	// if((framebufferStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER)) != GL_FRAMEBUFFER_COMPLETE){
	// 	std::cout << "ERROR: FRAMEBUFFER IS NOT COMPLETE " << framebufferStatus << std::endl;
	// }
	// glBindFramebuffer(GL_FRAMEBUFFER, 0); 


	// load textures for each render object
	std::map<std::string, RenderData*>::iterator it;
	for(it = renderData.begin(); it != renderData.end(); it++){
		RenderData *rd = it->second;
	
		if(!rd->material.texFilename.empty())
		{
			std::cout << "Loading texture: " << rd->material.texFilename << std::endl;

			glGenTextures(1, &rd->tex);
			glBindTexture(GL_TEXTURE_2D, rd->tex);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

			// load and generate the texture
			int width, height, nrChannels;
			rd->textureData = stbi_load(rd->material.texFilename.c_str(), &width, &height, &nrChannels, 0);
			if (rd->textureData)
			{
			    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, rd->textureData);
			    glGenerateMipmap(GL_TEXTURE_2D);
			}
			else
			{
			    std::cout << "ERROR: FAILED TO LOAD TEXTURE" << std::endl;
			}
			stbi_image_free(rd->textureData);
			glBindTexture(GL_TEXTURE_2D, 0);
		}
	}

	while((error = glGetError()) !=GL_NO_ERROR){
		std::cout << "ERROR: RENDERER INIT CALL FAILED WITH ERROR CODE: " << error << std::endl;
	}
}

void Renderer::render()
{
	// for(int i=0; i < directionalLights.size(); i++){
	// 	glm::vec3 direction = -directionalLights[i]->direction;

	// 	glm::mat4 lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, 0.1f, 100.0f);
	//     glm::mat4 lightView = glm::lookAt(direction, glm::vec3( 0.0f, 0.0f, 0.0f), glm::vec3( 0.0f, 0.0f,  1.0f));

	// 	// render depth map to depthMapTex in frame buffer
	//     glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	//     glBindFramebuffer(GL_FRAMEBUFFER, dirLightDepthMapFBO[i]);
	//     glClear(GL_DEPTH_BUFFER_BIT);
	//     std::map<std::string, RenderData*>::iterator it;
	// 	for(it = renderData.begin(); it != renderData.end(); it++){
	// 		RenderData *rd = it->second;

	// 		GLenum drawMode = rd->drawMode;
	// 		GLenum drawUsage = rd->drawUsage;

	// 		int numVertices = rd->positions.size() / 3;
	// 		float* positions = &(rd->positions[0]);

	// 		Shader *shader = &depthShader;

	//     	GLuint vao = rd->vao;
	// 		GLuint vbo = rd->vbo[0];
	//     	glGenBuffers(1, &vbo);
	// 		glGenVertexArrays(1, &vao);

	// 		shader->use();

	// 		GLint position = shader->getAttrib("position");

	// 		shader->setMat4("model", rd->model);
	// 		shader->setMat4("view", lightView);
	// 		shader->setMat4("projection", lightProjection);

	// 		/* Create vertex array object */
	// 		glBindVertexArray(vao);
	// 		glBindBuffer(GL_ARRAY_BUFFER, vbo);
	// 		glBufferData(GL_ARRAY_BUFFER, 3*numVertices*sizeof(float), positions, drawMode);
	// 		glEnableVertexAttribArray(position);
	// 		glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
	// 		glBindVertexArray(0);

	// 		glBindVertexArray(vao);
	// 		glDrawArrays(drawUsage, 0, numVertices);
	// 	    glBindVertexArray(0);

	// 	    // This must be done every frame for SFML to be able to render text
	// 	    glDeleteBuffers(1, &vbo);
	// 		glDeleteVertexArrays(1, &vao);
	// 	}
	//     glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//     glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	// }

	// spotlight
	// glm::vec3 direction = -spotLights[0]->direction;
	// glm::vec3 position = spotLights[0]->position;
	// glm::mat4 lightProjection = glm::perspective(45.0f, 1.0f * 640 / 480, 0.1f, 100.0f);
 //    glm::mat4 lightView = glm::lookAt(position, position - direction, glm::vec3( 0.0f, 0.0f,  1.0f));

    // directional light
	glm::vec3 direction = -directionalLights[0]->direction;
	glm::mat4 lightProjection = glm::ortho(-5.0f, 5.0f, -5.0f, 5.0f, 0.1f, 100.0f);
    glm::mat4 lightView = glm::lookAt(direction, glm::vec3( 0.0f, 0.0f, 0.0f), glm::vec3( 0.0f, 0.0f,  1.0f));

	// render depth map to depthMapTex in frame buffer
    glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
    glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
    glClear(GL_DEPTH_BUFFER_BIT);
    std::map<std::string, RenderData*>::iterator it;
	for(it = renderData.begin(); it != renderData.end(); it++){
		RenderData *rd = it->second;

		GLenum drawMode = rd->drawMode;
		GLenum drawUsage = rd->drawUsage;

		int numVertices = rd->positions.size() / 3;
		float* positions = &(rd->positions[0]);

		Shader *shader = &depthShader;

    	GLuint vao = rd->vao;
		GLuint vbo = rd->vbo[0];
    	glGenBuffers(1, &vbo);
		glGenVertexArrays(1, &vao);

		shader->use();

		GLint position = shader->getAttrib("position");

		shader->setMat4("model", rd->model);
		shader->setMat4("view", lightView);
		shader->setMat4("projection", lightProjection);

		/* Create vertex array object */
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, 3*numVertices*sizeof(float), positions, drawMode);
		glEnableVertexAttribArray(position);
		glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
		glBindVertexArray(0);

		glBindVertexArray(vao);
		glDrawArrays(drawUsage, 0, numVertices);
	    glBindVertexArray(0);

	    // This must be done every frame for SFML to be able to render text
	    glDeleteBuffers(1, &vbo);
		glDeleteVertexArrays(1, &vao);
	}
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);


 //    // render depth map to quad for testing (comment out when done)
 //    glClearColor(0.15f, 0.15f, 0.15f, 0.0f);
	// glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// float quadVertices[] = {-1.0f,  1.0f, 0.0f, -1.0f, -1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, -1.0f, 0.0f};
 //    float quadTexCoords[] = {0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,1.0f, 0.0f};
 //    GLuint quadVBO[2];
 //    GLuint quadVAO;
 //    glGenBuffers(2, &quadVBO[0]);
 //    glGenVertexArrays(1, &quadVAO);
    
 //    debugDepthShader.use();

 //    GLint position = debugDepthShader.getAttrib("position");
	// GLint texture = debugDepthShader.getAttrib("atexture");

	// debugDepthShader.setInt("texture1", 0);

	// /* Create vertex array object */
	// glBindVertexArray(quadVAO);
	// glBindBuffer(GL_ARRAY_BUFFER, quadVBO[0]);
	// glBufferData(GL_ARRAY_BUFFER, 3*4*sizeof(float), quadVertices, GL_DYNAMIC_DRAW);
	// glEnableVertexAttribArray(position);
	// glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);

	// glActiveTexture(GL_TEXTURE0);
	// glBindTexture(GL_TEXTURE_2D, depthMapTex);
	// glBindBuffer(GL_ARRAY_BUFFER, quadVBO[1]);
	// glBufferData(GL_ARRAY_BUFFER, 2*4*sizeof(float), quadTexCoords, GL_DYNAMIC_DRAW);
	// glEnableVertexAttribArray(texture);
	// glVertexAttribPointer(texture, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*)0);
	// glBindVertexArray(0);

	// glBindVertexArray(quadVAO);
	// glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
 //    glBindVertexArray(0);

 //    // This must be done every frame for SFML to be able to render text
 //    glDeleteBuffers(2, &quadVBO[0]);
	// glDeleteVertexArrays(1, &quadVAO);



	glClearColor(0.15f, 0.15f, 0.15f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, depthMapTex);

	for(it = renderData.begin(); it != renderData.end(); it++){
		RenderData *rd = it->second;

		GLenum drawMode = rd->drawMode;
		GLenum drawUsage = rd->drawUsage;

		int numVertices = rd->positions.size() / 3;
		float* positions = &(rd->positions[0]);
		float* normals = &(rd->normals[0]);
		float* texCoords = &(rd->texCoords[0]);

		Shader *shader = &rd->shader;

    	// This must be done every frame for SFML to be able to render text
    	int numVBO = 3;
    	GLuint vao = rd->vao;
		GLuint vbo[3];
		vbo[0] = rd->vbo[0];
		vbo[1] = rd->vbo[1];
		vbo[2] = rd->vbo[2];
    	glGenBuffers(numVBO, &vbo[0]);
		glGenVertexArrays(1, &vao);

		shader->use();

		GLint position = shader->getAttrib("position");
		GLint normal = shader->getAttrib("normal");
		GLint texture = shader->getAttrib("atexture");

		shader->setMat4("model", rd->model);
		shader->setMat4("view", view);
		shader->setMat4("projection", projection);
		shader->setMat4("lightView", lightView);
		shader->setMat4("lightProjection", lightProjection);

		shader->setFloat("pointRadius", 15.0f);
		shader->setFloat("pointScale", 2.0f);
		shader->setVec3("cameraPos", cameraPos);

		// material
		shader->setFloat("material.shininess", rd->material.shininess);
		shader->setVec3("material.ambient", rd->material.ambient);
		shader->setVec3("material.diffuse", rd->material.diffuse);
		shader->setVec3("material.specular", rd->material.specular);

		// lights
		for(int j=0;j<directionalLights.size();j++){
			shader->setVec3("dirLight.direction", directionalLights[j]->direction);
			shader->setVec3("dirLight.ambient", directionalLights[j]->ambient);
			shader->setVec3("dirLight.diffuse", directionalLights[j]->diffuse);
			shader->setVec3("dirLight.specular", directionalLights[j]->specular);
		}

		for(int j=0;j<spotLights.size();j++){
			shader->setVec3("spotLight.position", spotLights[j]->position);
			shader->setVec3("spotLight.direction", spotLights[j]->direction);
			shader->setVec3("spotLight.ambient", spotLights[j]->ambient);
			shader->setVec3("spotLight.diffuse", spotLights[j]->diffuse);
			shader->setVec3("spotLight.specular", spotLights[j]->specular);
			shader->setFloat("spotLight.constant", spotLights[j]->constant);
			shader->setFloat("spotLight.linear", spotLights[j]->linear);
			shader->setFloat("spotLight.quadratic", spotLights[j]->quadratic);
			shader->setFloat("spotLight.cutOff", spotLights[j]->cutOff);
			shader->setFloat("spotLight.outerCutOff", spotLights[j]->outerCutOff);
		}

		for(int j=0;j<pointLights.size();j++){
			shader->setVec3("pointLight.position", pointLights[j]->position);
			shader->setVec3("pointLight.ambient", pointLights[j]->ambient);
			shader->setVec3("pointLight.diffuse", pointLights[j]->diffuse);
			shader->setVec3("pointLight.specular", pointLights[j]->specular);
			shader->setFloat("pointLight.constant", pointLights[j]->constant);
			shader->setFloat("pointLight.linear", pointLights[j]->linear);
			shader->setFloat("pointLight.quadratic", pointLights[j]->quadratic);
		}

		for(int j=0;j<areaLights.size();j++){
			shader->setVec3("areaLight.position", areaLights[j]->position);
			shader->setVec3("areaLight.ambient", areaLights[j]->ambient);
			shader->setVec3("areaLight.diffuse", areaLights[j]->diffuse);
			shader->setVec3("areaLight.specular", areaLights[j]->specular);
		}

		shader->setInt("texture1", 0);
		shader->setInt("shadowMap", 1);

		glActiveTexture(GL_TEXTURE0);
       	glBindTexture(GL_TEXTURE_2D, rd->tex);

		/* Create vertex array object */
		glBindVertexArray(vao);
		glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
		glBufferData(GL_ARRAY_BUFFER, 3*numVertices*sizeof(float), positions, drawMode);
		glEnableVertexAttribArray(position);
		glVertexAttribPointer(position, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);

		if(normal != -1){
			glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
			glBufferData(GL_ARRAY_BUFFER, 3*numVertices*sizeof(float), normals, drawMode);
			glEnableVertexAttribArray(normal);
			glVertexAttribPointer(normal, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (GLvoid*)0);
		}

		if(texture != -1){
			glBindBuffer(GL_ARRAY_BUFFER, vbo[2]);
			glBufferData(GL_ARRAY_BUFFER, 2*numVertices*sizeof(float), texCoords, drawMode);
			glEnableVertexAttribArray(texture);
			glVertexAttribPointer(texture, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (GLvoid*)0);
		}
		glBindVertexArray(0);

		glBindVertexArray(vao);
		glDrawArrays(drawUsage, 0, numVertices);
	    glBindVertexArray(0);

	    // This must be done every frame for SFML to be able to render text
	    glDeleteBuffers(numVBO, &vbo[0]);
		glDeleteVertexArrays(1, &vao);
	}

	while((error = glGetError()) !=GL_NO_ERROR){
		std::cout << "ERROR: RENDERER RENDER CALL FAILED WITH ERROR CODE: " << error << std::endl;
	}
}