#ifndef __CAMERA_H__
#define __CAMERA_H__

#include <iostream>

#define GLM_FORCE_RADIANS
#include <GL/gl.h>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "external/glm/glm/glm.hpp"
#include "external/glm/glm/gtc/matrix_transform.hpp"
#include "external/glm/glm/gtc/type_ptr.hpp"

#include "Frustum.h"

enum KEY
{
	LEFT,
	RIGHT,
	UP,
	DOWN
};


class Camera
{
	private:
		static const GLfloat PAN_SENSITIVITY;
		static const GLfloat SCROLL_SENSITIVITY;
		static const GLfloat TRANSLATE_SENSITIVITY;

		Frustum *frustum;

		glm::vec3 position;
		glm::vec3 front;
		glm::vec3 up;
		glm::vec3 right;
		glm::vec3 worldUp;

		glm::vec2 mouseDelta;
		glm::vec2 lastPos; 

		glm::mat4 projection;

	public:
		Camera(glm::vec3 position, glm::vec3 front, glm::vec3 up);
		Camera(const Camera &camera);
		Camera& operator=(const Camera &camera);
		~Camera();

		void processEvents(sf::Event event);
		void update();

		glm::vec3 getPosition() const;
		glm::vec3 getFront() const;
		glm::vec3 getUp() const;
		glm::vec3 getRight() const;
		glm::mat4 getViewMatrix() const;
		glm::mat4 getProjMatrix() const;

		void setPosition(glm::vec3 position);
		void setFront(glm::vec3 front);
		void setUp(glm::vec3 up);

		float getHeight() const;

		std::vector<float> getFrustumTriVertices();
		std::vector<float> getFrustumTriNormals();

		int checkPointInFrustum(glm::vec3 point);
		int checkSphereInFrustum(glm::vec3 centre, float radius);
		int checkAABBInFrustum(glm::vec3 min, glm::vec3 max);

	private:
		void processKeyboardInput(sf::Keyboard::Key key);
		void processMouseButtonInput(GLfloat x, GLfloat y);
		void processMouseScrollInput(GLfloat scroll);
		void mouseButtonPressed(GLfloat x, GLfloat y);
		void mouseButtonReleased();

};


#endif