#ifndef __RENDERDATA_H__
#define __RENDERDATA_H__

#include <vector>

#include "Shader.h"
#include "Material.h"

#define GLM_FORCE_RADIANS

#include "../../glm/glm/glm.hpp"
#include "../../glm/glm/gtc/matrix_transform.hpp"
#include "../../glm/glm/gtc/type_ptr.hpp"

// Each entity in the world that will be rendered has a RenderData object. The 
// entity controls updating the data in this RenderData object (usually in the
// entities start and update methods). The entities RenderData object is then 
// passed to the Renderer class for rendering.
class RenderData
{
	public:
		GLuint vao;
		GLuint vbo[3];
		GLuint tex;

		GLenum drawMode;
		GLenum drawUsage; 

		bool isActive;

		Shader shader;
		Material material;
		
		glm::vec3 centre;
		float radius;
		std::vector<float> positions;
		std::vector<float> normals;
		std::vector<float> texCoords;

		glm::mat4 model;

		unsigned char* textureData;

};


#endif