#include <iostream>
#include <random>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/copy.h>
#include "SPHFluid.cuh"
#include "kernels.cuh"
#include "util_kernels.cuh"
#include "cuda_util.cuh"

#include <stdio.h>

#include <cuda.h>

SPHFluid::SPHFluid()
{

}


SPHFluid::SPHFluid(int numParticles, int3 particleGridDim, float3 particleGridSize)
{
	this->numParticles = numParticles;
	this->particleGridDim = particleGridDim;
	this->particleGridSize = particleGridSize;

	dt = 0.0075f;
	kappa = 1.0f;
	rho0 = 1000.0f;
	mass = 0.01f;
	h = particleGridSize.x / particleGridDim.x;
	h2 = h * h;
	h6 = h2 * h2 * h2;
	h9 = h6 * h2 * h;
	numCells = particleGridDim.x * particleGridDim.y * particleGridDim.z;
	numVoxelVertices = (particleGridDim.x+1) * (particleGridDim.y+1) * (particleGridDim.z+1);
	octtreeBufferSize = 2*numParticles + 12000;

	std::cout<<particleGridDim.x<<" "<<particleGridDim.y<<" "<<particleGridDim.z<<std::endl;

	// allocate memory 
	allocateMemory();
}


SPHFluid::SPHFluid(const SPHFluid &spf)
{
	numParticles = spf.numParticles;
	particleGridDim = spf.particleGridDim;
	particleGridSize = spf.particleGridSize;

	dt = spf.dt;
	kappa = spf.kappa;
	rho0 = spf.rho0;
	mass = spf.mass;
	h = spf.h;
	h2 = spf.h2;
	h6 = spf.h6;
	h9 = spf.h9;
	numCells = spf.numCells;
	numVoxelVertices = spf.numVoxelVertices;

	// allocate memory 
	allocateMemory();
}


SPHFluid& SPHFluid::operator=(const SPHFluid &spf)
{
	if(this != &spf){
		// free old memory 
		deallocateMemory();

		numParticles = spf.numParticles;
		particleGridDim = spf.particleGridDim;
		particleGridSize = spf.particleGridSize;

		dt = spf.dt;
		kappa = spf.kappa;
		rho0 = spf.rho0;
		mass = spf.mass;
		h = spf.h;
		h2 = spf.h2;
		h6 = spf.h6;
		h9 = spf.h9;
		numCells = spf.numCells;
		numVoxelVertices = spf.numVoxelVertices;

		// reallocate memory
		allocateMemory(); 
	}

	return *this;
}


SPHFluid::~SPHFluid()
{
	// free old memory 
	deallocateMemory();
}


void SPHFluid::allocateMemory()
{
	// allocate memory on host
	h_pos = new float4[numParticles];
	h_vel = new float4[numParticles];
	h_rho = new float[numParticles];
	h_rho0 = new float[numParticles];
	h_pres = new float[numParticles];
	h_acc = new float4[numParticles];
	h_output = new float[3*numParticles];
	h_cellStartIndex = new int[numCells];
	h_cellEndIndex = new int[numCells];
	h_cellIndex = new int[numParticles];
	h_particleIndex = new int[numParticles];
	h_voxelVertexFlags = new int[numVoxelVertices];
	h_voxelVertexValues = new float[numVoxelVertices];
	h_spos = new float4[numParticles];
	h_svel = new float4[numParticles];

	h_minPos = new float4;
	h_maxPos = new float4;
	h_child = new int[8*octtreeBufferSize];
	h_start = new int[octtreeBufferSize];
	h_sorted = new int[octtreeBufferSize];
	h_count = new int[octtreeBufferSize];

	// allocate memory on device
	// gpuErrchk(cudaMalloc((void**)&d_pos, numParticles*sizeof(float4)));
	// gpuErrchk(cudaMalloc((void**)&d_vel, numParticles*sizeof(float4)));
	// gpuErrchk(cudaMalloc((void**)&d_rho, numParticles*sizeof(float)));
	// gpuErrchk(cudaMalloc((void**)&d_rho0, numParticles*sizeof(float)));
	// gpuErrchk(cudaMalloc((void**)&d_pres, numParticles*sizeof(float)));
	// gpuErrchk(cudaMalloc((void**)&d_acc, numParticles*sizeof(float4)));
	// gpuErrchk(cudaMalloc((void**)&d_output, 3*numParticles*sizeof(float)));
	// gpuErrchk(cudaMalloc((void**)&d_cellStartIndex, numCells*sizeof(int)));
	// gpuErrchk(cudaMalloc((void**)&d_cellEndIndex, numCells*sizeof(int)));
	// gpuErrchk(cudaMalloc((void**)&d_cellHash, numParticles*sizeof(int)));
	// gpuErrchk(cudaMalloc((void**)&d_particleIndex, numParticles*sizeof(int)));
	// gpuErrchk(cudaMalloc((void**)&d_voxelVertexFlags, numVoxelVertices*sizeof(int)));
	// gpuErrchk(cudaMalloc((void**)&d_voxelVertexValues, numVoxelVertices*sizeof(float)));
	// gpuErrchk(cudaMalloc((void**)&d_spos, numParticles*sizeof(float4)));
	// gpuErrchk(cudaMalloc((void**)&d_svel, numParticles*sizeof(float4)));

	// gpuErrchk(cudaMemset(d_voxelVertexFlags, 0, numVoxelVertices*sizeof(int)));
	// gpuErrchk(cudaMemset(d_cellStartIndex, -1, numCells*sizeof(int)));
	// gpuErrchk(cudaMemset(d_cellEndIndex, -1, numCells*sizeof(int)));

	// gpuErrchk(cudaMalloc((void**)&d_minPos, sizeof(float4)));
	// gpuErrchk(cudaMalloc((void**)&d_maxPos, sizeof(float4)));
	// gpuErrchk(cudaMalloc((void**)&d_minStack, 32*numParticles*sizeof(float3)));
	// gpuErrchk(cudaMalloc((void**)&d_maxStack, 32*numParticles*sizeof(float3)));
	// gpuErrchk(cudaMalloc((void**)&d_nodeStack, 32*numParticles*sizeof(int)));
	// gpuErrchk(cudaMalloc((void**)&d_mutex, sizeof(int)));
	// gpuErrchk(cudaMalloc((void**)&d_pindex, sizeof(int)));
	// gpuErrchk(cudaMalloc((void**)&d_child, 8*octtreeBufferSize*sizeof(int)));
	// gpuErrchk(cudaMalloc((void**)&d_start, octtreeBufferSize*sizeof(int)));
	// gpuErrchk(cudaMalloc((void**)&d_sorted, octtreeBufferSize*sizeof(int)));
	// gpuErrchk(cudaMalloc((void**)&d_count, octtreeBufferSize*sizeof(int)));
}

void SPHFluid::deallocateMemory()
{
	// free memory on host
	delete [] h_pos;
	delete [] h_vel;
	delete [] h_rho;
	delete [] h_rho0;
	delete [] h_pres;
	delete [] h_acc;
	delete [] h_cellStartIndex;
	delete [] h_cellEndIndex;
	delete [] h_cellIndex;
	delete [] h_particleIndex;
	delete [] h_voxelVertexFlags;
	delete [] h_voxelVertexValues;
	delete [] h_spos;
	delete [] h_svel;

	delete h_minPos;
	delete h_maxPos;
	delete [] h_child;
	delete [] h_start;
	delete [] h_sorted;
	delete [] h_count;

	// free memory on device
	// gpuErrchk(cudaFree(d_pos));
	// gpuErrchk(cudaFree(d_vel));
	// gpuErrchk(cudaFree(d_rho));
	// gpuErrchk(cudaFree(d_rho0));
	// gpuErrchk(cudaFree(d_pres));
	// gpuErrchk(cudaFree(d_acc));	
	// gpuErrchk(cudaFree(d_output));
	// gpuErrchk(cudaFree(d_cellStartIndex));
	// gpuErrchk(cudaFree(d_cellEndIndex));
	// gpuErrchk(cudaFree(d_cellHash));
	// gpuErrchk(cudaFree(d_particleIndex));
	// gpuErrchk(cudaFree(d_voxelVertexFlags));
	// gpuErrchk(cudaFree(d_voxelVertexValues));
	// gpuErrchk(cudaFree(d_spos));
	// gpuErrchk(cudaFree(d_svel));

	// gpuErrchk(cudaFree(d_minPos));
	// gpuErrchk(cudaFree(d_maxPos));
	// gpuErrchk(cudaFree(d_minStack));
	// gpuErrchk(cudaFree(d_maxStack));
	// gpuErrchk(cudaFree(d_nodeStack));
	// gpuErrchk(cudaFree(d_mutex));
	// gpuErrchk(cudaFree(d_pindex));
	// gpuErrchk(cudaFree(d_child));
	// gpuErrchk(cudaFree(d_start));
	// gpuErrchk(cudaFree(d_sorted));
	// gpuErrchk(cudaFree(d_count));
}


void SPHFluid::initialCondition()
{
	std::default_random_engine generator;
	std::uniform_real_distribution<float> distributionx(0.0f, particleGridSize.x);
	std::uniform_real_distribution<float> distributiony(0.0f, particleGridSize.y);
	std::uniform_real_distribution<float> distributionz(0.0f, particleGridSize.z);

	// loop through all particles
	for (int i = 0; i < numParticles; i++){
		h_pos[i].x = 1.0f*distributionx(generator);
		h_pos[i].y = 0.25f*distributiony(generator);
		h_pos[i].z = 0.5f*distributionz(generator);
		h_pos[i].w = 0.0f;

		h_vel[i].x = 0.0f;
		h_vel[i].y = 0.0f;
		h_vel[i].z = 0.0f;
		h_vel[i].w = 0.0f;

		h_rho0[i] = rho0;
	}


	// set initial position in host output array
	for(int i=0;i<numParticles;i++){
		h_output[3*i] = h_pos[i].x;
		h_output[3*i+1] = h_pos[i].y;
		h_output[3*i+2] = h_pos[i].z;
	}

	// gpuErrchk(cudaMemcpy(d_pos, h_pos, numParticles*sizeof(float4), cudaMemcpyHostToDevice));
	// gpuErrchk(cudaMemcpy(d_vel, h_vel, numParticles*sizeof(float4), cudaMemcpyHostToDevice));
	// gpuErrchk(cudaMemcpy(d_rho0, h_rho0, numParticles*sizeof(float), cudaMemcpyHostToDevice));
}


void SPHFluid::setInitialCondition(float3 *pos)
{
	for(int i = 0; i < numParticles; i++){
		h_pos[i].x = pos[i].x;
		h_pos[i].y = pos[i].y;
		h_pos[i].z = pos[i].z;
		h_pos[i].w = 0.0f;

		h_vel[i].x = 0.0f;
		h_vel[i].y = 0.0f;
		h_vel[i].z = 0.0f;
		h_vel[i].w = 0.0f;

		h_rho0[i] = rho0;
	}

	// set initial position in host output array
	for(int i = 0; i < numParticles; i++){
		h_output[3*i] = h_pos[i].x;
		h_output[3*i+1] = h_pos[i].y;
		h_output[3*i+2] = h_pos[i].z;
	}

	// gpuErrchk(cudaMemcpy(d_pos, h_pos, numParticles*sizeof(float4), cudaMemcpyHostToDevice));
	// gpuErrchk(cudaMemcpy(d_vel, h_vel, numParticles*sizeof(float4), cudaMemcpyHostToDevice));
	// gpuErrchk(cudaMemcpy(d_rho0, h_rho0, numParticles*sizeof(float), cudaMemcpyHostToDevice));
}


void SPHFluid::update()
{
	elapsedTime = 0;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start,0);

	// OcttreeInfiniteDomainAlgorithm();

	UniformFiniteGridAlgorithm();

	cudaEventRecord(stop,0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&elapsedTime, start, stop);
}


void SPHFluid::updateDensityOnly()
{
	UniformFiniteGridDensityOnly();
}


int SPHFluid::GetNumberOfParticles()
{
	return numParticles;
}


int SPHFluid::GetNumberOfVoxels()
{
	return numCells;
}


int SPHFluid::GetNumberOfVoxelVertices()
{
	return numVoxelVertices;
}


float SPHFluid::GetTimestep()
{
	return dt;
}


float SPHFluid::GetGasStiffness()
{
	return kappa;
}


float SPHFluid::GetParticleRestDensity()
{
	return rho0;
}


float SPHFluid::GetParticleMass()
{
	return mass;
}


float SPHFluid::GetVoxelWidth()
{
	return h;
}


float4* SPHFluid::GetPosition()
{
	return h_pos;
}

float4* SPHFluid::GetVelocity()
{
	return h_vel;
}


float* SPHFluid::GetDensity()
{
	return h_rho;
}


float* SPHFluid::GetTighlyPackedPosition()
{
	return h_output;
}

float* SPHFluid::GetVoxelVertexValues()
{
	return h_voxelVertexValues;
}


float SPHFluid::GetElapsedTime()
{
	return elapsedTime;
}

size_t SPHFluid::GetDeviceMemoryUsed()
{
	// we assume 4 byte int/float's
	size_t numCellsLengthArrays = 2 * 4 * numCells;
	size_t numParticlesLengthArrays = 2256 * 4 * numParticles;
	size_t numVoxelVerticesLengthArrays = 2 * 4 * numVoxelVertices;

	return numCellsLengthArrays + numParticlesLengthArrays + numVoxelVerticesLengthArrays;
}


void SPHFluid::UniformFiniteGridAlgorithm()
{
	dim3 gridSize(256,1,1);
	dim3 blockSize(256,1,1);
	build_spatial_grid_kernel<<< gridSize, blockSize >>>
		(
			d_pos, 
			d_particleIndex, 
			d_cellHash, 
			numParticles, 
			particleGridDim,
			particleGridSize
		);

	thrust::device_ptr<int> t_a(d_cellHash);
	thrust::device_ptr<int> s_a(d_particleIndex);
	thrust::sort_by_key(t_a, t_a + numParticles, s_a);

	reorder_particles_kernel<<< gridSize, blockSize >>>
		(
			d_pos,
			d_spos,
			d_vel,
			d_svel,
			d_cellStartIndex,
			d_cellEndIndex,
			d_cellHash,
			d_particleIndex,
			numParticles
		);

	calculate_particle_density_kernel<<< gridSize, blockSize >>>
		(
			d_spos,  
			d_rho, 
			d_rho0,
			d_pres,
			d_cellStartIndex,
			d_cellEndIndex,
			d_cellHash,
			d_particleIndex,
			numParticles,
			kappa,
			h2,
			h9,
			particleGridDim
		);

	calculate_pressure_acceleration_kernel<<< gridSize, blockSize >>>
		(
			d_spos, 
			d_svel,
			d_acc, 
			d_rho,
			d_pres,
			d_cellStartIndex,
			d_cellEndIndex,
			d_cellHash,
			d_particleIndex,
			numParticles,
			h,
			h6,
			particleGridDim
		);

	update_particles_kernel<<< gridSize, blockSize >>>
		(
			d_spos,
			d_svel,
			d_acc,
			dt,
			h,
			numParticles,
			particleGridSize
		);

	copy_sph_arrays_kernel<<< gridSize, blockSize >>>
		(
			d_pos,
			d_spos,
			d_vel,
			d_svel,
			d_output,
			numParticles
		);

	scan_particles_and_mark_voxel_vertices_kernel<<< gridSize, blockSize >>>
		(
			d_pos, 
			d_voxelVertexFlags, 
			numParticles, 
			numCells,
			particleGridDim,
			particleGridSize
		);

	generate_density_at_voxel_vertices<<< gridSize, blockSize >>>
		(
			d_pos, 
			d_rho, 
			d_cellStartIndex,
			d_cellEndIndex,
			d_voxelVertexFlags, 
			d_voxelVertexValues, 
			numVoxelVertices,
			h2,
			h9,
			particleGridDim,
			particleGridSize
		);

	set_array_to_value<int><<< gridSize, blockSize >>>(d_cellStartIndex, -1, numCells);
	set_array_to_value<int><<< gridSize, blockSize >>>(d_cellEndIndex, -1, numCells);
	set_array_to_value<int><<< gridSize, blockSize >>>(d_voxelVertexFlags, 0, numVoxelVertices);

	gpuErrchk(cudaMemcpy(h_output, d_output, 3*numParticles*sizeof(float), cudaMemcpyDeviceToHost));
	gpuErrchk(cudaMemcpy(h_pos, d_pos, numParticles*sizeof(float4), cudaMemcpyDeviceToHost));
	gpuErrchk(cudaMemcpy(h_rho, d_rho, numParticles*sizeof(float), cudaMemcpyDeviceToHost));
	gpuErrchk(cudaMemcpy(h_voxelVertexFlags, d_voxelVertexFlags, numVoxelVertices*sizeof(int), cudaMemcpyDeviceToHost));
	gpuErrchk(cudaMemcpy(h_voxelVertexValues, d_voxelVertexValues, numVoxelVertices*sizeof(float), cudaMemcpyDeviceToHost));
}


void SPHFluid::UniformFiniteGridDensityOnly()
{
	dim3 gridSize(256,1,1);
	dim3 blockSize(256,1,1);
	build_spatial_grid_kernel<<< gridSize, blockSize >>>
		(
			d_pos, 
			d_particleIndex, 
			d_cellHash, 
			numParticles, 
			particleGridDim,
			particleGridSize
		);

	thrust::device_ptr<int> t_a(d_cellHash);
	thrust::device_ptr<int> s_a(d_particleIndex);
	thrust::sort_by_key(t_a, t_a + numParticles, s_a);

	reorder_particles_kernel<<< gridSize, blockSize >>>
		(
			d_pos,
			d_spos,
			d_vel,
			d_svel,
			d_cellStartIndex,
			d_cellEndIndex,
			d_cellHash,
			d_particleIndex,
			numParticles
		);

	calculate_particle_density_kernel<<< gridSize, blockSize >>>
		(
			d_spos,  
			d_rho, 
			d_rho0,
			d_pres,
			d_cellStartIndex,
			d_cellEndIndex,
			d_cellHash,
			d_particleIndex,
			numParticles,
			kappa,
			h2,
			h9,
			particleGridDim
		);

	scan_particles_and_mark_voxel_vertices_kernel<<< gridSize, blockSize >>>
		(
			d_pos, 
			d_voxelVertexFlags, 
			numParticles, 
			numCells,
			particleGridDim,
			particleGridSize
		);

	generate_density_at_voxel_vertices<<< gridSize, blockSize >>>
		(
			d_pos, 
			d_rho, 
			d_cellStartIndex,
			d_cellEndIndex,
			d_voxelVertexFlags, 
			d_voxelVertexValues, 
			numVoxelVertices,
			h2,
			h9,
			particleGridDim,
			particleGridSize
		);

	set_array_to_value<int><<< gridSize, blockSize >>>(d_cellStartIndex, -1, numCells);
	set_array_to_value<int><<< gridSize, blockSize >>>(d_cellEndIndex, -1, numCells);
	set_array_to_value<int><<< gridSize, blockSize >>>(d_voxelVertexFlags, 0, numVoxelVertices);

	gpuErrchk(cudaMemcpy(h_voxelVertexFlags, d_voxelVertexFlags, numVoxelVertices*sizeof(int), cudaMemcpyDeviceToHost));
	gpuErrchk(cudaMemcpy(h_voxelVertexValues, d_voxelVertexValues, numVoxelVertices*sizeof(float), cudaMemcpyDeviceToHost));
}


void SPHFluid::OcttreeInfiniteDomainAlgorithm()
{
	dim3 gridSize(256,1,1);
	dim3 blockSize(256,1,1);
	set_variable_to_value<int><<< gridSize, blockSize >>>(d_mutex, 0);
	set_variable_to_value<int><<< gridSize, blockSize >>>(d_pindex, numParticles);

	set_array_to_value<int><<< gridSize, blockSize >>>(d_child, -1, 8*octtreeBufferSize);
	set_array_to_value<int><<< gridSize, blockSize >>>(d_start, -1, octtreeBufferSize);
	set_array_to_value<int><<< gridSize, blockSize >>>(d_sorted, 0, octtreeBufferSize);
	set_array_to_value<int><<< gridSize, blockSize >>>(d_count, 0, octtreeBufferSize);

	gpuErrchk(cudaMemset(d_minPos, 0, sizeof(float4)));
	gpuErrchk(cudaMemset(d_maxPos, 0, sizeof(float4)));

	compute_bounding_box_kernel<<< gridSize, blockSize >>>
	(
		d_pos, 
		d_maxPos, 
		d_minPos, 
		d_mutex, 
		numParticles
	);

	gpuErrchk(cudaMemcpy(h_minPos, d_minPos, sizeof(float4), cudaMemcpyDeviceToHost));
	gpuErrchk(cudaMemcpy(h_maxPos, d_maxPos, sizeof(float4), cudaMemcpyDeviceToHost));

	std::cout << "min x: " << h_minPos->x << "  min y: " << h_minPos->y << "  min z: " << h_minPos->z << std::endl;
	std::cout << "max x: " << h_maxPos->x << "  max y: " << h_maxPos->y << "  max z: " << h_maxPos->z << std::endl;

	build_octtree_kernel<<< gridSize, blockSize >>>
	(
		d_pos, 
		d_count, 
		d_child, 
		d_pindex, 
		d_minPos,
		d_maxPos, 
		numParticles
	);

	// sort_kernel<<< gridSize, blockSize >>>
	// (
	// 	d_count, 
	// 	d_start, 
	// 	d_sorted, 
	// 	d_child, 
	// 	d_pindex, 
	// 	numParticles
	// );

	// calculate_particle_density_octtree_kernel<<< gridSize, blockSize >>>
	// (
	// 	d_pos, 
	// 	d_rho,
	// 	d_rho0,
	// 	d_pres,  
	// 	d_sorted, 
	// 	d_child, 
	// 	d_nodeStack,
	// 	d_minStack,
	// 	d_maxStack,
	// 	d_minPos,
	// 	d_maxPos, 
	// 	numParticles,
	// 	kappa,
	// 	h2,
	// 	h9
	// );

	// gpuErrchk(cudaMemcpy(h_sorted, d_sorted, 2*numParticles*sizeof(float), cudaMemcpyDeviceToHost));
	// int count = 0;
	// for(int i=0;i<2*numParticles;i++){
	// 	count += h_sorted[i];
	// 	std::cout << h_sorted[i] << "  ";
	// }
	// std::cout<<""<<std::endl;
	// std::cout<<"count: " << count << std::endl;

	//gpuErrchk(cudaMemcpy(h_sorted, d_sorted, 2*numParticles*sizeof(float), cudaMemcpyDeviceToHost));
	// gpuErrchk(cudaMemcpy(h_pos, d_pos, numParticles*sizeof(float4), cudaMemcpyDeviceToHost));
	gpuErrchk(cudaMemcpy(h_child, d_child, 8*2*numParticles*sizeof(float), cudaMemcpyDeviceToHost));
	//gpuErrchk(cudaMemcpy(h_rho0, d_rho0, numParticles*sizeof(float), cudaMemcpyDeviceToHost));
	//gpuErrchk(cudaMemcpy(h_pres, d_pres, numParticles*sizeof(float), cudaMemcpyDeviceToHost));
	// gpuErrchk(cudaMemcpy(h_rho, d_rho, numParticles*sizeof(float), cudaMemcpyDeviceToHost));
	// gpuErrchk(cudaMemcpy(h_count, d_count, numParticles*sizeof(int), cudaMemcpyDeviceToHost));
	
	// for(int i=0; i < numParticles; i++){
	// 	// std::cout << h_pres[i] << "  ";
		// std::cout << h_rho[i] << "  ";
	// 	// std::cout << h_count[i] << "  ";
	// }
	// std::cout<<""<<std::endl;
	std::cout<<""<<std::endl;
	for(int i=0; i < 8*2*numParticles; i++){
		std::cout << h_child[i] << "  ";
	}
	std::cout<<""<<std::endl;


	// calculate_particle_forces_octtree_kernel<<< gridSize, blockSize >>>
	// (
	// 	d_pos,
	// 	d_vel,
	// 	d_acc, 
	// 	d_rho,
	// 	d_pres,  
	// 	d_sorted, 
	// 	d_child, 
	// 	d_nodeStack,
	// 	d_minStack,
	// 	d_maxStack,
	// 	d_minPos,
	// 	d_maxPos, 
	// 	numParticles,
	// 	h,
	// 	h6
	// );

	// gpuErrchk(cudaMemcpy(h_acc, d_acc, numParticles*sizeof(float4), cudaMemcpyDeviceToHost));
	// for(int i=0;i<numParticles;i++){
	// 	std::cout << h_acc[i].x << "  " << h_acc[i].y << "  " << h_acc[i].z << "  ";
	// }
	// std::cout<<""<<std::endl;

	// update_particles_octtree_kernel<<< gridSize, blockSize >>>
	// (
	// 	d_pos, 
	// 	d_vel, 
	// 	d_acc,
	// 	d_output,
	// 	dt, 
	// 	h,
	// 	numParticles,
	// 	particleGridSize
	// );

	// gpuErrchk(cudaMemcpy(h_output, d_output, 3*numParticles*sizeof(float), cudaMemcpyDeviceToHost));
}