#include <string>
#include <sstream>
#include <iomanip>
#include <cstddef>
#include <iostream>

#include <cuda.h>
#include <cuda_runtime.h>

#include "PhysicsEngine.h"


PhysicsEngine::PhysicsEngine()
{
	numFluidParticles = 0;
	numRigidBodyParticles = 0;

	sphFluid = NULL; 
	marchingCubes = NULL;
	sphRigidBody = NULL;

	setDefaultConfig();
}


PhysicsEngine::PhysicsEngine(const PhysicsEngine &engine)
{
	numFluidParticles = engine.numFluidParticles;
	numRigidBodyParticles = engine.numFluidParticles;

	setDefaultConfig();
}


PhysicsEngine& PhysicsEngine::operator=(const PhysicsEngine &engine)
{
	if(this != &engine){
		numFluidParticles = engine.numFluidParticles;
		numRigidBodyParticles = engine.numFluidParticles;

		setDefaultConfig();
	}

	return *this;
}


PhysicsEngine::~PhysicsEngine()
{
	if(sphFluid != NULL){ 
		delete sphFluid; 
		//delete marchingCubes;
	}
	if(sphRigidBody != NULL){ delete sphRigidBody; }
}


void PhysicsEngine::init()
{
	if(numFluidParticles > 0){
		sphFluid = new SPHFluid(numFluidParticles, physicsDomain.pointsPerSide, physicsDomain.max);
		sphFluid->initialCondition();

		//marchingCubes = new MarchingCubes(numFluidParticles, physicsDomain.pointsPerSide, physicsDomain.max);
	}

	if(numRigidBodyParticles > 0){
		sphRigidBody = new SPHRigidBody();

		for(itr = rigidBodies.begin(); itr != rigidBodies.end(); itr++){
			
		}
	}
}


void PhysicsEngine::update()
{
	for(itf = fluids.begin(); itf != fluids.end(); itf++){
		Fluid *fluid = itf->second;
		
		if(fluid->isSimulationOn){
			sphFluid->update();
		}
		else{
			sphFluid->updateDensityOnly();
		}

		std::vector<float> vert;
		std::vector<float> norm;

		if(fluid->isMarchingCubesOn){ 
			marchingCubes->runMarchingCubesAlgorithm(sphFluid->GetVoxelVertexValues(), sphFluid->GetNumberOfVoxelVertices(), sphFluid->GetParticleRestDensity());

			vert.assign(marchingCubes->GetTriangleVertices(), marchingCubes->GetTriangleVertices() + marchingCubes->GetNumberOfVertices());
			norm.assign(marchingCubes->GetTriangleNormals(), marchingCubes->GetTriangleNormals() + marchingCubes->GetNumberOfVertices());

			fluid->update(vert, norm);
		}
		else{
			vert.assign(sphFluid->GetTighlyPackedPosition(), sphFluid->GetTighlyPackedPosition() + sphFluid->GetNumberOfParticles());

			fluid->update(vert);
		}
	}

	for(itr = rigidBodies.begin(); itr != rigidBodies.end(); itr++){
		RigidBody *rigidBody = itr->second;

		rigidBody->update();
	}
}


void PhysicsEngine::restart()
{
	if(sphFluid != NULL){ 
		delete sphFluid; 
		//delete marchingCubes;
	}
	if(sphRigidBody != NULL){ delete sphRigidBody; }

	init();
}


void PhysicsEngine::add(std::string name, Particles *particle)
{
	particles[name] = particle;

	numParticles += particle->getNumParticles();
}


void PhysicsEngine::add(std::string name, Fluid *fluid)
{
	fluids[name] = fluid;	

	numFluidParticles += fluid->getNumParticles();
}


void PhysicsEngine::add(std::string name, RigidBody *rigidBody)
{
	rigidBodies[name] = rigidBody;	

	numRigidBodyParticles += rigidBody->getNumParticles();
}


void PhysicsEngine::remove(std::string name)
{
	rigidBodies.erase(name);
}


void PhysicsEngine::setDefaultConfig()
{
	physicsDomain.h = 0.03125;
	physicsDomain.min = make_float3(0.0f, 0.0f, 0.0f);
	physicsDomain.max = make_float3(1.0f, 2.0f, 1.0f);

	physicsDomain.pointsPerSide.x = (int)(physicsDomain.max.x / physicsDomain.h);
	physicsDomain.pointsPerSide.y = (int)(physicsDomain.max.y / physicsDomain.h);
	physicsDomain.pointsPerSide.z = (int)(physicsDomain.max.z / physicsDomain.h);

	physicsDomain.max.x = physicsDomain.h * physicsDomain.pointsPerSide.x;
	physicsDomain.max.y = physicsDomain.h * physicsDomain.pointsPerSide.y;
	physicsDomain.max.z = physicsDomain.h * physicsDomain.pointsPerSide.z;

	std::cout << physicsDomain.max.x << "  " << physicsDomain.max.y << "  " << physicsDomain.max.z << std::endl;

	numParticles = 0;
	numFluidParticles = 0;
	numRigidBodyParticles = 0;
}