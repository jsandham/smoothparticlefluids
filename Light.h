#ifndef __LIGHT_H__
#define __LIGHT_H__


#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#define GLM_FORCE_RADIANS

#include "external/glm/glm/glm.hpp"
#include "external/glm/glm/gtc/matrix_transform.hpp"
#include "external/glm/glm/gtx/transform.hpp"
#include "external/glm/glm/gtc/type_ptr.hpp"


typedef enum LightType
{
	DIRECTIONAL = 0,
	POINT,
	SPOT,
	AREA
}LightType;


class Light
{
	public:
		virtual LightType getType() = 0;
};


class DirectionalLight : public Light
{
	public:
		glm::vec3 direction;
		glm::vec3 ambient;
		glm::vec3 diffuse;
		glm::vec3 specular;

	public:
		DirectionalLight();

		LightType getType();
};


class PointLight : public Light
{
	public:
		float constant;
		float linear;
		float quadratic;
		glm::vec3 position;
		glm::vec3 ambient;
		glm::vec3 diffuse;
		glm::vec3 specular;

	public:
		PointLight();

		LightType getType();
};


class SpotLight : public Light
{
	public:
		float constant;
		float linear;
		float quadratic;
		float cutOff;
		float outerCutOff;
		glm::vec3 position;
		glm::vec3 direction;
		glm::vec3 ambient;
		glm::vec3 diffuse;
		glm::vec3 specular;

	public:
		SpotLight();

		LightType getType();
};


class AreaLight : public Light
{
	public:
		glm::vec3 position;
		glm::vec3 ambient;
		glm::vec3 diffuse;
		glm::vec3 specular;

	public:
		AreaLight();

		LightType getType();
};



#endif