#ifndef __PHYSICSENGINE_H__
#define __PHYSICSENGINE_H__

#include <ctime>

#include "Particles.h"
#include "Fluid.h"
#include "RigidBody.h"
#include "SPHParticles.cuh"
#include "SPHFluid.cuh"
#include "SPHRigidBody.cuh"
#include "MarchingCubes.cuh"


typedef struct PhysicsDomain
{
	float h;
	float3 min;
	float3 max;
	int3 pointsPerSide;
}PhysicsDomain;


class PhysicsEngine
{
	private:
		int numParticles;
		int numFluidParticles;
		int numRigidBodyParticles;
		PhysicsDomain physicsDomain;

		SPHParticles *sphParticles;
		SPHFluid *sphFluid;
		SPHRigidBody *sphRigidBody;
		MarchingCubes *marchingCubes;

		std::map<std::string, Particles*> particles;
		std::map<std::string, Fluid*> fluids;
		std::map<std::string, RigidBody*> rigidBodies;
		std::map<std::string, Fluid*>::iterator itf;
		std::map<std::string, RigidBody*>::iterator itr;

	public:
		PhysicsEngine();
		PhysicsEngine(const PhysicsEngine &engine);
		PhysicsEngine& operator=(const PhysicsEngine &engine);
		~PhysicsEngine();

		void init();
		void update();
		void restart();
		void add(std::string name, Particles *particle);
		void add(std::string name, Fluid *fluid);
		void add(std::string name, RigidBody *rigidBody);
		void remove(std::string name);

	private:
		void setDefaultConfig();
};

#endif