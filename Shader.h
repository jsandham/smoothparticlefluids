#ifndef __SHADER_H__
#define __SHADER_H__

#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>

#include <GL/glew.h>

#define GLM_FORCE_RADIANS

#include "external/glm/glm/glm.hpp"
#include "external/glm/glm/gtc/matrix_transform.hpp"
#include "external/glm/glm/gtx/transform.hpp"
#include "external/glm/glm/gtc/type_ptr.hpp"

class Shader
{
	private:
		GLint success;
		GLuint Program;
		std::vector<std::string> attributes;
		std::vector<std::string> uniforms;
		std::vector<GLenum> attributeTypes;
		std::vector<GLenum> uniformTypes;

	public:
		Shader();
		Shader(const GLchar* vertexShaderPath, const GLchar* fragmentShaderPath);
		
		void use();
		bool compile(const GLchar* vertexShaderPath, const GLchar* fragmentShaderPath);

		GLint getAttrib(const char *name);
		GLint getUniform(const char *name);

		std::vector<std::string> getAttributes();
		std::vector<std::string> getUniforms();
		std::vector<GLenum> getAttributeTypes();
		std::vector<GLenum> getUniformTypes();

		void setBool(std::string name, bool value);
		void setInt(std::string name, int value);
		void setFloat(std::string name, float value);
		void setVec2(std::string name, glm::vec2 &vec);
		void setVec3(std::string name, glm::vec3 &vec);
		void setVec4(std::string name, glm::vec4 &vec);
		void setMat2(std::string name, glm::mat2 &mat);
		void setMat3(std::string name, glm::mat3 &mat);
		void setMat4(std::string name, glm::mat4 &mat);
};

#endif