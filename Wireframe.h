#ifndef __WIREFRAME_H__
#define __WIREFRAME_H__

class Wireframe
{
	private:
		int numVertices;
		float* vertices;

	public:
		Wireframe();
		Wireframe(const Wireframe &frame);
		Wireframe& operator=(const Wireframe &frame);
		~Wireframe();

		int GetNumberOfVertices();
		float* GetVertices();
};

#endif