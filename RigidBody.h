#ifndef __RIGIDBODYBLOCKS_H__
#define __RIGIDBODYBLOCKS_H__

#include <vector>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include <string>

#define GLM_FORCE_RADIANS

#include "../../glm/glm/glm.hpp"
#include "../../glm/glm/gtc/matrix_transform.hpp"
#include "../../glm/glm/gtc/type_ptr.hpp"

#include "ObjParser.h"
#include "RenderData.h"
#include "Material.h"


class RigidBody
{
	private:
		ObjParser objParser;
		RenderData renderData;

		int numParticles;
		std::vector<float> vertices;

	public:
		glm::mat4 model;

		Material material;

	public:
		RigidBody(std::string objFilePath);
		~RigidBody();

		void processEvents(sf::Event event);
		void start();
		void update();

		void loadObjFile(std::string filename);

		RenderData& getRenderData();

		int getNumParticles();
};

#endif