#ifndef __KERNELS_CUH__
#define __KERNELS_CUH__

#include "vector_types.h"


__device__ int3 calcGridPosition(float4 pos, int3 grid, float3 gridSize);

__device__ int calcCellIndex(int3 gridPos, int3 grid);

__device__ int calcGridHash(int3 gridPos, int numBuckets);

__device__ int calcClosestVoxelVertexIndex(float4 pos, int3 grid, float3 gridSize);

__device__ int calcVoxelVertexIndex(int3 vertexGridPos, int3 grid);

__device__ int3 calcVoxelVertexPosition(int voxelVertexIndex, int3 grid);

__device__ int3 calcFirstVoxelVertexPosition(int voxelIndex, int3 grid);

__device__ float3 vertexInterp(float3 p1, float3 p2, float v1, float v2, float isoValue);

__device__ float3 calcNormal(float3 p0, float3 p1, float3 p2);

__device__ void boundary(float4 *pos, float4 *vel, float h);

__global__ void build_spatial_grid_kernel
	(
		float4 *pos, 
		int *particlesIndex, 
		int *cellIndex, 
		int numParticles, 
		int3 grid,
		float3 gridSize
	);

__global__ void reorder_particles_kernel
	(
		float4 *pos,
		float4 *spos,
		float4 *vel,
		float4 *svel,
		int *cellStartIndex,
		int *cellEndIndex,
		int *cellIndex,
		int *particleIndex,
		int numParticles
	);

__global__ void calculate_particle_density_kernel
	(
		float4 *pos, 
		float *rho, 
		float *rho0,
		float *pres,
		int *cellStartIndex,
		int *cellEndIndex,
		int *cellIndex,
		int *particleIndex,
		int numParticles,
		float kappa,
		float h2,
		float h9,
		int3 grid
	);


__global__ void calculate_pressure_acceleration_kernel
	(
		float4 *pos, 
		float4 *vel,
		float4 *acc, 
		float *rho,
		float *pres,
		int *cellStartIndex,
		int *cellEndIndex,
		int *cellIndex,
		int *particleIndex,
		int numParticles,
		float h,
		float h6,
		int3 grid
	);


__global__ void update_particles_kernel
	(
		float4 *pos, 
		float4 *vel, 
		float4 *acc, 
		float dt, 
		float h,
		int numParticles,
		float3 gridSize
	);


__global__ void copy_sph_arrays_kernel
	(
		float4 *pos,
		float4 *spos,
		float4 *vel,
		float4 *svel,
		float *output,
		int numParticles
	);

__global__ void scan_particles_and_mark_voxel_vertices_kernel
	(
		float4 *pos,
		int *voxelVertexFlags,
		int numParticles,
		int numVoxels,
		int3 grid,
		float3 gridSize
	);


__global__ void generate_density_at_voxel_vertices
	(
		float4 *pos, 
		float *rho, 
		int *cellStartIndex,
		int *cellEndIndex,
		int *voxelVertexFlags, 
		float *voxelVertexValues, 
		int numVoxelVertices,
		float h2,
		float h9,
		int3 grid,
		float3 gridSize
	);


__global__ void compute_bounding_box_kernel
	(
		float4 *pos, 
		float4 *maxPos, 
		float4 *minPos, 
		int *mutex, 
		int numParticles
	);


__global__ void build_octtree_kernel
	(
		float4 *pos, 
		int *count, 
		int *child, 
		int *pindex, 
		float4 *minPos,
		float4 *maxPos, 
		int numParticles
	);


__global__ void sort_kernel
	(
		int *count, 
		int *start, 
		int *sorted, 
		int *child, 
		int *pindex, 
		int numParticles
	);


__global__ void calculate_particle_density_octtree_kernel
	(
		float4 *pos, 
		float *rho,
		float *rho0,
		float *pres,  
		int *sorted, 
		int *child, 
		int *nodeStack,
		float3 *minStack,
		float3 *maxStack,
		float4 *minPos,
		float4 *maxPos, 
		int numParticles,
		float kappa,
		float h2,
		float h9
	);


__global__ void calculate_particle_forces_octtree_kernel
	(
		float4 *pos,
		float4 *vel,
		float4 *acc, 
		float *rho,
		float *pres,  
		int *sorted, 
		int *child, 
		int *nodeStack,
		float3 *minStack,
		float3 *maxStack,
		float4 *minPos,
		float4 *maxPos, 
		int numParticles,
		float h,
		float h6
	);


__global__ void update_particles_octtree_kernel
	(
		float4 *pos, 
		float4 *vel, 
		float4 *acc,
		float *output,
		float dt, 
		float h,
		int numParticles,
		float3 gridSize
	);






__global__ void classify_voxels_kernel
	(
		float *voxelVertexValues,
		int *triPerVoxel, 
		int *voxelOccupied,
		int numVoxels,
		int3 grid,
		float isoValue
	);


__global__ void compact_voxels_kernel
	(
		int *compactVoxels,
		int *voxelOccupied,
		int *voxelOccupiedScan,
		int numVoxels
	);


__global__ void generate_triangles_kernel
	(
		float *triangleVertices,
		float *normals,
		float *voxelVertexValues, 
		int *compactVoxels,
		int *triPerVoxelScan,
		int numVoxels,
		int numActiveVoxels,
		int3 grid,
		float3 gridSize,
		float isoValue
	);


__global__ void voxelize_kernel
	(
		float* triangleVertices, 
		int *voxelTable, 
		int numVertices,
		int numVoxels,
		float h,
		int3 grid
	);





__global__ void	calculate_collisions_kernel
	(
		float4 *pos,  
		int *cellStartIndex,
		int *cellEndIndex,
		int *cellIndex,
		int *particleIndex,
		int numParticles,
		int3 grid
	);


#endif