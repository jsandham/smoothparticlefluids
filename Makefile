#Makefile
#define variables
cpp= main.o Scene.o Particles.o Fluid.o PhysicsEngine.o Camera.o Shader.o Grid.o Wireframe.o RigidBody.o ObjParser.o Renderer.o OctTree.o Frustum.o Light.o Material.o
cuda= SPHFluid.o SPHRigidBody.o SPHParticles.o MarchingCubes.o kernels.o
external= stb_image_implementation.o
objects= $(cpp) $(cuda) $(external)

#compiler
NVCC= nvcc    #nvidia cuda compiler
DEBUG=# -v -w -g -lineinfo  #debug flags
OPT= -O2 -arch=compute_30 -maxrregcount=32 -use_fast_math   #optimization flag
LIBS= -lGLU -lGL -lGLEW -lsfml-graphics -lsfml-window -lsfml-system

execname= main


build: $(objects)
	$(NVCC) $(DEBUG) $(OPT) -o $(execname) $(objects) $(LIBS)

#cpp
Material.o: Material.cpp
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c Material.cpp

Light.o: Light.cpp
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c Light.cpp

Frustum.o: Frustum.cpp
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c Frustum.cpp

OctTree.o: OctTree.cpp
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c OctTree.cpp

ObjParser.o: ObjParser.cpp
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c ObjParser.cpp

Renderer.o: Renderer.cpp
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c Renderer.cpp

RigidBody.o: RigidBody.cpp
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c RigidBody.cpp

Wireframe.o: Wireframe.cpp
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c Wireframe.cpp

Grid.o: Grid.cpp
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c Grid.cpp

Shader.o: Shader.cpp
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c Shader.cpp

Camera.o: Camera.cpp
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c Camera.cpp

PhysicsEngine.o: PhysicsEngine.cpp
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c PhysicsEngine.cpp

Fluid.o: Fluid.cpp
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c Fluid.cpp

Particles.o: Particles.cpp
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c Particles.cpp

Scene.o: Scene.cpp
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c Scene.cpp

stb_image_implementation.o: stb_image_implementation.cpp
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c stb_image_implementation.cpp

main.o: main.cpp
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c main.cpp


#cuda
kernels.o: kernels.cu
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c kernels.cu

SPHFluid.o: SPHFluid.cu
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c SPHFluid.cu

SPHRigidBody.o: SPHRigidBody.cu
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c SPHRigidBody.cu

SPHParticles.o: SPHParticles.cu
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c SPHParticles.cu

MarchingCubes.o: MarchingCubes.cu
	$(NVCC) $(DEBUG) $(OPT) -std=c++11 -c MarchingCubes.cu


clean:
	#cpu
	rm main.o 
	rm Scene.o
	rm Fluid.o
	rm Particles.o
	rm PhysicsEngine.o 
	rm Camera.o 
	rm Shader.o
	rm Grid.o
	rm Wireframe.o
	rm RigidBody.o
	rm ObjParser.o
	rm Renderer.o
	rm OctTree.o
	rm Frustum.o
	rm Light.o
	rm Material.o
	#cuda
	rm MarchingCubes.o
	rm SPHFluid.o
	rm SPHRigidBody.o
	rm SPHParticles.o
	rm kernels.o
	#external
	rm stb_image_implementation.o
