#ifndef __FLUID_H__
#define __FLUID_H__

#include <string>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#define GLM_FORCE_RADIANS

#include "../../glm/glm/glm.hpp"
#include "../../glm/glm/gtc/matrix_transform.hpp"
#include "../../glm/glm/gtc/type_ptr.hpp"

#include "RenderData.h"
#include "Material.h"
#include "ObjParser.h"

#include "vector_types.h"


class Fluid
{
	private:
		ObjParser objParser;
		RenderData renderData;

		int numParticles;
		std::vector<float> vertices;
		std::vector<float> normals;

	public:
		bool isSimulationOn;
		bool isMarchingCubesOn;
		bool isWireframeOn;

		glm::mat4 model;

		Material material;

	public:
		Fluid(std::string objFilePath);
		~Fluid();

		void start();
		void processEvents(sf::Event event);
		void update(std::vector<float> vertices);
		void update(std::vector<float> vertices, std::vector<float> normals);

		int getNumParticles();
		std::vector<float> getVertices();

		RenderData& getRenderData();

	private:
		void toggleSimulation();
		void toggleMarchingCubes();
		void toggleWireframe();
		void restart();

};


#endif