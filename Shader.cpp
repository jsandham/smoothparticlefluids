#include <iostream>
#include "Shader.h"


Shader::Shader()
{
}


Shader::Shader(const GLchar* vertexShaderPath, const GLchar* fragmentShaderPath)
{
	compile(vertexShaderPath, fragmentShaderPath);
}



void Shader::use()
{
	if(!success){
		std::cout << "Must compile shader being using" << std::endl;
		return;
	}
	glUseProgram(this->Program);
}


bool Shader::compile(const GLchar* vertexShaderPath, const GLchar* fragmentShaderPath)
{
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if(err != GLEW_OK){
		std::cout << "ERROR: COULD NOT INITIALIZE GLEW" << std::endl;
		return false;
	}

	std::string vertexCode;
	std::string fragmentCode;
	std::ifstream vertexShaderFile;
	std::ifstream fragmentShaderFile;

	vertexShaderFile.exceptions(std::ifstream::badbit);
	fragmentShaderFile.exceptions(std::ifstream::badbit);

	try{
		vertexShaderFile.open(vertexShaderPath);
		fragmentShaderFile.open(fragmentShaderPath);
		std::stringstream vertexShaderStream, fragmentShaderStream;
		vertexShaderStream << vertexShaderFile.rdbuf();
		fragmentShaderStream << fragmentShaderFile.rdbuf();
		vertexShaderFile.close();
		fragmentShaderFile.close();
		vertexCode = vertexShaderStream.str();
		fragmentCode = fragmentShaderStream.str();
	}
	catch(std::ifstream::failure e){
		std::cout << "ERROR: SHADER FILE NOT SUCCESSFULLY READ" << std::endl;
		return false;
	}

	const GLchar* vertexShaderCode = vertexCode.c_str();
	const GLchar* fragmentShaderCode = fragmentCode.c_str();

	GLuint vertexShader, fragmentShader;
	GLchar infoLog[512];
	success = 0;

	// vertex shader
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexShaderCode, NULL);
	glCompileShader(vertexShader);
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
        return false;
    }

    // fragment shader
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderCode, NULL);
	glCompileShader(fragmentShader);
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
        return false;
    }

    // shader program
	Program = glCreateProgram();
	glAttachShader(Program, vertexShader);
	glAttachShader(Program, fragmentShader);
	glLinkProgram(Program);
	glGetProgramiv(Program, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(Program, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
        return false;
    }
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	GLint count;
	GLint size;
	GLenum type;
	GLchar name[16];
	GLsizei length;

	// fill vector of strings with attributes from shader program
	glGetProgramiv(Program, GL_ACTIVE_ATTRIBUTES, &count);
	for(int i = 0; i < count; i++){
		glGetActiveAttrib(Program, (GLuint)i, 16, &length, &size, &type, name);
		attributes.push_back(std::string(name));
		attributeTypes.push_back(type);
		//std::cout << "attribute type: " << type << std::endl;
		//std::cout << "GL_FLOAT: " << GL_FLOAT << " GL_FLOAT_MAT4: " << GL_FLOAT_MAT4 << std::endl;
	}

	// fill vector of strings with uniforms from shader program
	glGetProgramiv(Program, GL_ACTIVE_UNIFORMS, &count);
	for(int i = 0; i < count; i++){
		glGetActiveUniform(Program, (GLuint)i, 16, &length, &size, &type, name);
		uniforms.push_back(std::string(name));
		uniformTypes.push_back(type);
		//std::cout << "uniform type: " << type << std::endl;
	}

	return true; 
}


GLint Shader::getAttrib(const char *name) {
	GLint attribute = glGetAttribLocation(Program, name);
	// if(attribute == -1){
	// 	std::cout << "Could not bind attribute " << name << std::endl;
	// }
	return attribute;
}

GLint Shader::getUniform(const char *name) {
	GLint uniform = glGetUniformLocation(Program, name);
	// if(uniform == -1){
	// 	std::cout << "Could not bind uniform " << name << std::endl;
	// }
	return uniform;
}

std::vector<std::string> Shader::getAttributes()
{
	return attributes;
}


std::vector<std::string> Shader::getUniforms()
{
	return uniforms;
}


std::vector<GLenum> Shader::getAttributeTypes()
{
	return attributeTypes;
}


std::vector<GLenum> Shader::getUniformTypes()
{
	return uniformTypes;
}


void Shader::setBool(std::string name, bool value)
{
	glUniform1i(getUniform(name.c_str()), (int)value);
}


void Shader::setInt(std::string name, int value)
{
	glUniform1i(getUniform(name.c_str()), value);
}


void Shader::setFloat(std::string name, float value)
{
	glUniform1f(getUniform(name.c_str()), value);
}


void Shader::setVec2(std::string name, glm::vec2 &vec)
{
	glUniform2fv(getUniform(name.c_str()), 1, &vec[0]);
}


void Shader::setVec3(std::string name, glm::vec3 &vec) 
{
	glUniform3fv(getUniform(name.c_str()), 1, &vec[0]);
}


void Shader::setVec4(std::string name, glm::vec4 &vec)
{
	glUniform4fv(getUniform(name.c_str()), 1, &vec[0]);
}


void Shader::setMat2(std::string name, glm::mat2 &mat)
{
	glUniformMatrix2fv(getUniform(name.c_str()), 1, GL_FALSE, &mat[0][0]);
}


void Shader::setMat3(std::string name, glm::mat3 &mat)
{
	glUniformMatrix3fv(getUniform(name.c_str()), 1, GL_FALSE, &mat[0][0]);
}


void Shader::setMat4(std::string name, glm::mat4 &mat)
{
	glUniformMatrix4fv(getUniform(name.c_str()), 1, GL_FALSE, &mat[0][0]);
}