#include "Material.h"


Material::Material()
{
	// default values
	opacity = 1.0f;
	texFilename = "textures/default.png";
	shininess = 32.0f;
	ambient = glm::vec3(0.05, 0.05f, 0.05f);
	diffuse = glm::vec3(0.75f, 0.75f, 0.75f);
	specular = glm::vec3(1.0f, 1.0f, 1.0f);
}