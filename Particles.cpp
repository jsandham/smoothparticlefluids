#include <random>

#include "Particles.h"


Particles::Particles(std::string objFilePath)
{
	if(!objParser.loadObj(objFilePath)){
        std::cout << "Failed to load obj file for particles" << std::endl;
    }

    numParticles = 256*256;
    vertices.resize(3*numParticles);
}

Particles::~Particles()
{

}

void Particles::start()
{
	std::vector<float> meshVertices = objParser.getVertices();

	int numMeshVertices = meshVertices.size() / 3;

    // TODO: In the future, initialize particle positions based on voxelization of obj mesh.
    // For now just make a cube from the max and min vertices of the obj mesh.
    float3 min = { meshVertices[0], meshVertices[1], meshVertices[2] };
    float3 max = { meshVertices[0], meshVertices[1], meshVertices[2] };
    for(int i=1;i<numMeshVertices; i++){
    	if(meshVertices[3*i] > max.x){ max.x = meshVertices[3*i]; }
    	if(meshVertices[3*i+1] > max.y){ max.y = meshVertices[3*i+1]; }
    	if(meshVertices[3*i+2] > max.z){ max.z = meshVertices[3*i+2]; }

    	if(meshVertices[3*i] < min.x){ min.x = meshVertices[3*i]; }
    	if(meshVertices[3*i+1] < min.y){ min.y = meshVertices[3*i+1]; }
    	if(meshVertices[3*i+2] < min.z){ min.z = meshVertices[3*i+2]; }
    }

    std::default_random_engine generator;
	std::uniform_real_distribution<float> distWidth(0.0f, max.x-min.x);
	std::uniform_real_distribution<float> distHeight(0.0f, max.y-min.y);
	std::uniform_real_distribution<float> distDepth(0.0f, max.z-min.z);

	for(int i=0;i<numParticles;i++){
		vertices[3*i] = min.x + distWidth(generator);
		vertices[3*i+1] = min.y + distHeight(generator);
		vertices[3*i+2] = min.z + distDepth(generator);
	}

	renderData.drawUsage = GL_POINTS;
	renderData.drawMode = GL_DYNAMIC_DRAW;
	renderData.shader.compile("shaders/particle.vs", "shaders/particle.frag");
	renderData.material = material;

	renderData.centre = glm::vec3(0.0f, 0.0f, 0.0f);
	renderData.radius = 0.0f;
	renderData.positions = objParser.getVertices();

	renderData.model = model;
}


void Particles::processEvents(sf::Event event)
{
	if(event.type == sf::Event::KeyPressed){
		//if(event.key.code == sf::Keyboard::Return){
		//	toggleSimulation();
		//}
		//if(event.key.code == sf::Keyboard::Q){
		//	restart();
		//}
	}
}


void Particles::update(std::vector<float> vertices)
{
	renderData.positions = vertices;
}


void Particles::update(std::vector<float> vertices, std::vector<float> normals)
{
	renderData.positions = vertices;
	renderData.normals = normals;
}

int Particles::getNumParticles()
{
	return numParticles;
}


std::vector<float> Particles::getVertices()
{
	return vertices;
}

RenderData& Particles::getRenderData()
{
	return renderData;
}