#include <iostream>
#include <stack>

#include "OctTree.h"

// Node implementation

Node::Node(glm::vec3 point)
{
	this->point = point;
}



// OctTree implementation

OctTree::OctTree(glm::vec3 min, glm::vec3 max)
{
	minAABBPoint = min;
	maxAABBPoint = max;

	root = NULL;
}


OctTree::OctTree(const OctTree &tree)
{
	minAABBPoint = tree.minAABBPoint;
	maxAABBPoint = tree.maxAABBPoint;

}


OctTree& OctTree::operator=(const OctTree &tree)
{
	minAABBPoint = tree.minAABBPoint;
	maxAABBPoint = tree.maxAABBPoint;
}


OctTree::~OctTree()
{
	deleteTree();
}


void OctTree::insert(glm::vec3 point)
{
	glm::vec3 min = minAABBPoint;
	glm::vec3 max = maxAABBPoint;
	Node *curr = root;
	Node *prev = NULL;
	Node *newNode = new Node(point);
	int childPath;
	while(curr != NULL){
		childPath = ComputeQuadrant(min, max, point);

		prev = curr;
		curr = curr->children[childPath];

		if(curr == NULL){
			int q = ComputeQuadrant(min, max, prev->point);
			if(prev->children[q] == NULL){
				prev->children[q] = new Node(prev->point);
				if(childPath == q){
					curr = prev->children[q];
				}
			}
		}

		if((childPath & (1 << 0)) == 1){
			min.x = 0.5f * (min.x + max.x);
		}
		else{
			max.x = 0.5f * (min.x + max.x);
		}
		if((childPath & (1 << 1)) == 2){
			min.y = 0.5f * (min.y + max.y);
		}
		else{
			max.y = 0.5f * (min.y + max.y);
		}
		if((childPath & (1 << 2)) == 4){
			min.z = 0.5f * (min.z + max.z);
		}
		else{
			max.z = 0.5f * (min.z + max.z);
		}
	}

	if (prev == NULL){
		root = newNode;
	}
	else{
		prev->children[childPath] = newNode;
	}
	
}


void OctTree::deleteTree()
{
	std::stack<Node*> stack;

	if(root != NULL){
		stack.push(root);
	}

	while(stack.size() != 0){
		Node *node = stack.top();
		stack.pop();
		for(int i=0;i<8;i++){
			if(node->children[i] != NULL){
				stack.push(node->children[i]);
			}
		}

		delete node;
	}
}


int OctTree::ComputeQuadrant(glm::vec3 min, glm::vec3 max, glm::vec3 point)
{
	int q = 0;
	glm::vec3 centre = 0.5f*(min + max);
	if (point.z > centre.z){ q |= 1 << 2; }
	if (point.y > centre.y) { q |= 1 << 1; }
	if (point.x > centre.x){ q |= 1 << 0; }

	return q;
}


// int childPath = 0;
// if(point.x < 0.5f*(min.x + max.x)){
// 	childPath += 1;
// 	max.x = 0.5f*(min.x + max.x);
// }
// else{
// 	min.x = 0.5f*(min.x + max.x);
// }
// if(point.y < 0.5f*(min.y + max.y)){
// 	childPath += 2;
// 	max.y = 0.5f*(min.y + max.y);
// }
// else{
// 	min.y = 0.5f*(min.y + max.y);
// }
// if(point.z < 0.5f*(min.z + max.z)){
// 	childPath += 4;
// 	max.z = 0.5f*(min.z + max.z);
// }
// else{
// 	min.z = 0.5f*(min.z + max.z);
// }