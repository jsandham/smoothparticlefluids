#ifndef __MATERIAL_H__
#define __MATERIAL_H__

#include <string>

#define GLM_FORCE_RADIANS

#include "external/glm/glm/glm.hpp"
#include "external/glm/glm/gtc/matrix_transform.hpp"
#include "external/glm/glm/gtx/transform.hpp"
#include "external/glm/glm/gtc/type_ptr.hpp"


class Material
{
	public:
		std::string texFilename;
		float opacity;
		float shininess;
		glm::vec3 ambient;
		glm::vec3 diffuse;
		glm::vec3 specular;

	public:
		Material();
};

#endif