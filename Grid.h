#ifndef __GRID_H__
#define __GRID_H__

#include <vector>
#include <string>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#define GLM_FORCE_RADIANS

#include "../../glm/glm/glm.hpp"
#include "../../glm/glm/gtc/matrix_transform.hpp"
#include "../../glm/glm/gtc/type_ptr.hpp"

#include "ObjParser.h"
#include "RenderData.h"
#include "Material.h"

class Grid
{
	private:
		ObjParser objParser;
		RenderData renderData;

		float heightOfCamera;
		std::vector<float> vertices;

	public:
		Material material;

	public:
		Grid(std::string objFilePath);
		~Grid();

		void processEvents(sf::Event event);
		void start();
		void update();

		void setHeightOfCamera(float height);

		RenderData& getRenderData();

	private:
		int GetNumberOfVertices();
		std::vector<float> GetVertices();	
};


#endif