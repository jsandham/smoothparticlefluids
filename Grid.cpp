#include <iostream>
#include <stdlib.h>
#include <math.h>

#include "Grid.h"



Grid::Grid(std::string objFilePath)
{
	if(!objParser.loadObj(objFilePath)){
        std::cout << "Failed to load obj file for rigidbody" << std::endl;
    }

	heightOfCamera = 1.0f;
}


Grid::~Grid()
{
}


void Grid::processEvents(sf::Event event)
{

}


void Grid::start()
{
	vertices = objParser.getVertices();

	renderData.drawUsage = GL_TRIANGLES;
	renderData.drawMode = GL_DYNAMIC_DRAW;
	renderData.shader.compile("shaders/directional.vs", "shaders/directional.frag");
	renderData.material = material;
	renderData.centre = glm::vec3(0.0f, 0.0f, 0.0f);
	renderData.radius = 0.0f;
	renderData.positions = objParser.getVertices();
    renderData.normals = objParser.getNormals();
    renderData.texCoords = objParser.getTextures();
	renderData.model = glm::mat4(1.0f);
}


void Grid::update()
{
	renderData.positions = vertices;
}


void Grid::setHeightOfCamera(float height)
{
	heightOfCamera = height;
}


int Grid::GetNumberOfVertices()
{
	return vertices.size() / 3;
}


std::vector<float> Grid::GetVertices()
{
	return vertices;
}

RenderData& Grid::getRenderData()
{
	return renderData;
}