#include "debug.h"
#include "Camera.h"

const GLfloat Camera::PAN_SENSITIVITY = 0.01f;
const GLfloat Camera::SCROLL_SENSITIVITY = 0.1f;
const GLfloat Camera::TRANSLATE_SENSITIVITY = 0.1f;


Camera::Camera(glm::vec3 position, glm::vec3 front, glm::vec3 up)
{
	frustum = new Frustum();

	this->position = position;         
	this->front = glm::normalize(front);                 
	this->up = glm::normalize(up);		                  
	this->right = glm::normalize(glm::cross(front, up));  
	this->worldUp = up; 

	mouseDelta = glm::vec2(0.0f);
	lastPos = glm::vec2(0.0f);

	projection = glm::perspective(45.0f, 1.0f * 640 / 480, 0.1f, 100.0f);

	frustum->setPerspective(45.0f, 1.0f * 640 / 480, 0.1f, 100.0f);

#if DEBUG
	frustum->setCameraSlow(position, front, up, right);
#else
	frustum->setCamera(position, front, up, right);
#endif
}


Camera::Camera(const Camera &camera)
{

}


Camera& Camera::operator=(const Camera &camera)
{
	if(this != &camera){
		delete frustum;

		frustum = new Frustum();
	}

	return *this;
}


Camera::~Camera()
{
	delete frustum;
}


void Camera::processEvents(sf::Event event)
{
	if(event.type == sf::Event::MouseWheelMoved){
		processMouseScrollInput(event.mouseWheel.delta);
	}
	else if(event.type == sf::Event::MouseButtonPressed){
		sf::Vector2i mpos = sf::Mouse::getPosition();
		mouseButtonPressed(mpos.x, mpos.y);
	}
	else if(event.type == sf::Event::MouseButtonReleased){
		mouseButtonReleased();
	}

	if(event.type == sf::Event::KeyPressed){
		processKeyboardInput(event.key.code);
	}

	if(sf::Mouse::isButtonPressed(sf::Mouse::Left)){
		sf::Vector2i mpos = sf::Mouse::getPosition();
		processMouseButtonInput(mpos.x, mpos.y);
	}

#if DEBUG
	frustum->setCameraSlow(position, front, up, right);
#else
	frustum->setCamera(position, front, up, right);
#endif
}

glm::vec3 Camera::getPosition() const
{
	return position;
}

glm::vec3 Camera::getFront() const
{
	return front;
}

glm::vec3 Camera::getUp() const
{
	return up;
}


glm::vec3 Camera::getRight() const
{
	return right;
}


glm::mat4 Camera::getViewMatrix() const
{
	return glm::lookAt(position, position + front, up);
}


glm::mat4 Camera::getProjMatrix() const
{
	return projection;
}


void Camera::setPosition(glm::vec3 position)
{
	this->position = position;
}


void Camera::setFront(glm::vec3 front)
{
	this->front = front;
}


void Camera::setUp(glm::vec3 up)
{
	this->up = up;
}


float Camera::getHeight() const
{
	return position.z;
}


std::vector<float> Camera::getFrustumTriVertices()
{
	return frustum->getTriVertices();
}


std::vector<float> Camera::getFrustumTriNormals()
{
	return frustum->getTriNormals();
}


int Camera::checkPointInFrustum(glm::vec3 point)
{
	return frustum->checkPoint(point);
}


int Camera::checkSphereInFrustum(glm::vec3 centre, float radius)
{
	return frustum->checkSphere(centre, radius);
}


int Camera::checkAABBInFrustum(glm::vec3 min, glm::vec3 max)
{
	return frustum->checkAABB(min, max);
}


void Camera::processKeyboardInput(sf::Keyboard::Key key)
{
    if (key == sf::Keyboard::Up)
        position += front * TRANSLATE_SENSITIVITY;
    if (key == sf::Keyboard::Down)
        position -= front * TRANSLATE_SENSITIVITY;
    if (key == sf::Keyboard::Left)
        position -= right * TRANSLATE_SENSITIVITY;
    if (key == sf::Keyboard::Right)
        position += right * TRANSLATE_SENSITIVITY;
}


void Camera::processMouseButtonInput(GLfloat x, GLfloat y)
{
	mouseDelta.x = PAN_SENSITIVITY * (x - lastPos.x);
	mouseDelta.y = PAN_SENSITIVITY * (y - lastPos.y);

	// update();

	lastPos.x = x;
	lastPos.y = y;

	update();
}


void Camera::processMouseScrollInput(GLfloat scroll)
{
	position += SCROLL_SENSITIVITY * scroll * front;
}


void Camera::mouseButtonPressed(GLfloat x, GLfloat y)
{
	lastPos.x = x;
	lastPos.y = y;
}


void Camera::mouseButtonReleased()
{

}


void Camera::update()
{
	// rotation around the camera up vector
	front = glm::mat3(glm::rotate(glm::mat4(), -mouseDelta.x, up)) * front;
	front = glm::normalize(front);

	right = glm::normalize(glm::cross(front, up));

	// rotation around the camera right vector
	front = glm::mat3(glm::rotate(glm::mat4(), -mouseDelta.y, right)) * front;
	front = glm::normalize(front);

	up = glm::normalize(glm::cross(right, front));
}