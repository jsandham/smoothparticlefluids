#ifndef __SPHParticles_CUH__
#define __SPHParticles_CUH__


class SPHParticles
{
	private:
		// simulation parameters
		float dt;

		// particle grid 
		int numParticles, numCells;
		int3 particleGridDim;
		float3 particleGridSize;

		// host variables
		float4 *h_pos;
		float4 *h_vel;
		float4 *h_acc;
		float4 *h_spos;
		float4 *h_svel;
		float *h_output;
		int *h_cellStartIndex;
		int *h_cellEndIndex;
		int *h_cellIndex;
		int *h_particleIndex;

		// device variables
		float4 *d_pos;
		float4 *d_vel;
		float4 *d_acc;
		float4 *d_spos;
		float4 *d_svel;
		float *d_output;
		int *d_cellStartIndex;
		int *d_cellEndIndex;
		int *d_cellHash;
		int *d_particleIndex;

		// used for timing
		float elapsedTime;
		cudaEvent_t start, stop;

	public:
		SPHParticles();
		SPHParticles(int numParticles, int3 particleGridDim, float3 particleGridSize);
		SPHParticles(const SPHParticles &spf);
		SPHParticles& operator=(const SPHParticles &spf);
		~SPHParticles();

		void update();

		void setInitialCondition(float3 *pos);
		void initialCondition();

	private:
		void allocateMemory();
		void deallocateMemory();

		void UniformFiniteGridAlgorithm();
};

#endif
