#include "kernels.cuh"

#include "external/nvidia/helper_math.h"

#include <stdio.h>

#define PI 3.14159265358979323846264f

__device__ const int blockSize = 256;
__device__ const int warp = 32;
__device__ const int stackSize = 32;

__device__ const float f1 = 315.0f/(64.0f*PI);
__device__ const float f2 = 45.0f/PI;
__device__ const float nu = 3.5;


__device__ int3 calcGridPosition(float4 pos, int3 grid, float3 gridSize)
{
	int3 gridPosition;
	gridPosition.x = __float2int_rd(pos.x * grid.x / gridSize.x);
	gridPosition.y = __float2int_rd(pos.y * grid.y / gridSize.y);
	gridPosition.z = __float2int_rd(pos.z * grid.z / gridSize.z);

	return gridPosition;
}


// what index the given gridPos corresponds to in the 1D array of cells
__device__ int calcCellIndex(int3 gridPos, int3 grid)
{
	return grid.y*grid.x * gridPos.z + grid.x * gridPos.y + gridPos.x;
}


// calculate spatial has for infinite domains
__device__ int calcGridHash(int3 gridPos, int numBuckets)
{
	const uint p1 = 73856093;
	const uint p2 = 19349663;
	const uint p3 = 83492791;
	int n = p1*gridPos.x ^ p2*gridPos.y ^ p3*gridPos.z;
	n %= numBuckets;
	return n;
}


// calculate closest voxel vertex index into the 1D array of voxel vertices that the 
// input point is closest to
__device__ int calcClosestVoxelVertexIndex(float4 pos, int3 grid, float3 gridSize)
{
	int3 vertexPosition;
	vertexPosition.x = round(pos.x * grid.x / gridSize.x);
	vertexPosition.y = round(pos.y * grid.y / gridSize.y);
	vertexPosition.z = round(pos.z * grid.z / gridSize.z);

	return (grid.y+1)*(grid.x+1)*vertexPosition.z + (grid.x+1)*vertexPosition.y + vertexPosition.x;
}


// calculate the voxel vertex index into the 1D array of voxel vertices that the given 
// input voxel vertex grid position maps to.
__device__ int calcVoxelVertexIndex(int3 vertexGridPos, int3 grid)
{
	return (grid.y+1)*(grid.x+1)*vertexGridPos.z + (grid.x+1)*vertexGridPos.y + vertexGridPos.x;
}


// calculate voxel vertex grid position from the voxel vertex that is mapped to
// the 1D array of voxel vertices by the input voxel vertex index
__device__ int3 calcVoxelVertexPosition(int voxelVertexIndex, int3 grid)
{
	int3 vertexPosition;
	vertexPosition.z = voxelVertexIndex / ((grid.x+1) * (grid.y+1));  // integer division
	voxelVertexIndex = voxelVertexIndex % ((grid.x+1) * (grid.y+1));

	vertexPosition.y = voxelVertexIndex / (grid.x+1);  // integer division
	voxelVertexIndex = voxelVertexIndex % (grid.x + 1);

	vertexPosition.x = voxelVertexIndex;

	return vertexPosition; 
}


// calculate first voxel vertex grid position belonging to the voxel that is mapped to 
// the 1D array of voxels by the input voxel index
__device__ int3 calcFirstVoxelVertexPosition(int voxelIndex, int3 grid)
{
	int3 vertexPosition;
	vertexPosition.z = voxelIndex / (grid.x * grid.y);  // integer division
	voxelIndex = voxelIndex % (grid.x * grid.y);

	vertexPosition.y = voxelIndex / grid.x;  // integer division
	voxelIndex = voxelIndex % grid.x;

	vertexPosition.x = voxelIndex;

	return vertexPosition; 
}


// p = p1 + (isoValue - v1) * (p2 - p1) / (v2 - v1);
__device__ float3 vertexInterp(float3 p1, float3 p2, float v1, float v2, float isoValue)
{
	float t = (isoValue - v1) / (v2 - v1);
	return lerp(p1, p2, t);
}


// calculate normal vector of triangle from its three vertices
__device__ float3 calcNormal(float3 p0, float3 p1, float3 p2)
{
	float3 edge0 = p1 - p0;
    float3 edge1 = p2 - p0;
   
    return cross(edge0, edge1);
}


// distance squared between two points
__device__ float distanceSquared(float3 point1, float3 point2)
{
	float dx = point1.x - point2.x;
	float dy = point1.y - point2.y;
	float dz = point1.z - point2.z;
	return dx*dx + dy*dy+ dz*dz;
}


// determine if particle is within h radius of given quadrant number
__device__ bool InRangeOfQuadrant(float4 position, float3 min, float3 max, float h2, int quadrant)
{
	bool answer = false;
	float h = sqrt(h2);
	float3 center = make_float3(0.5f*(max.x + min.x), 0.5f*(max.y + min.y), 0.5f*(max.z + min.z));
	if(quadrant == 0){
		if(position.x + h > center.x && position.y + h > center.y && position.z + h > center.z){
			answer = true;
		}
	}
	if(quadrant == 1){
		if(position.x - h < center.x && position.y + h > center.y && position.z + h > center.z){
			answer = true;
		}
	}
	if(quadrant == 2){
		if(position.x + h > center.x && position.y - h < center.y && position.z + h > center.z){
			answer = true;
		}
	}
	if(quadrant == 3){
		if(position.x - h < center.x && position.y - h < center.y && position.z + h > center.z){
			answer = true;
		}
	}
	if(quadrant == 4){
		if(position.x + h > center.x && position.y + h > center.y && position.z - h < center.z){
			answer = true;
		}
	}
	if(quadrant == 5){
		if(position.x - h < center.x && position.y + h > center.y && position.z - h < center.z){
			answer = true;
		}
	}
	if(quadrant == 6){
		if(position.x + h > center.x && position.y - h < center.y && position.z - h < center.z){
			answer = true;
		}
	}
	if(quadrant == 7){
		if(position.x - h < center.x && position.y - h < center.y && position.z - h < center.z){
			answer = true;
		}
	}

	return answer;
}


// penalty force boundary conditions http://n-e-r-v-o-u-s.com/education/simulation/week6.php
__device__ void boundary(float4 *pos, float4 *vel, float h, float3 gridSize)
{
	float d;
	float n = 1.0f;

	d = -n*(gridSize.x - (*pos).x) + h;
	if(d > 0.0f){
		(*pos).x += d*(-0.5f);
		(*vel).x -= 1.9*(*vel).x*(-0.5f)*(-0.5f);

		// (*acc).x += 10000*(-n)*d;
		// (*acc).x += -0.9 * (*vel).x*(-n)*(-n);
	} 

	d = n*(0.0f - (*pos).x) + h;
	if(d > 0.0f){
		(*pos).x += d*(0.5f);
		(*vel).x -= 1.9*(*vel).x*(0.5f)*(0.5f);

		// (*acc).x += 10000*(n)*d;
		// (*acc).x += -0.9 * (*vel).x*(n)*(n);
	} 

	d = -n*(gridSize.y - (*pos).y) + h;
	if(d > 0.0f){
		(*pos).y += d*(-0.5f);
		(*vel).y -= 1.9*(*vel).y*(-0.5f)*(-0.5f);

		// (*acc).y += 10000*(-n)*d;
		// (*acc).y += -0.9 * (*vel).y*(-n)*(-n);
	} 

	d = n*(0.0f - (*pos).y) + h;
	if(d > 0.0f){
		(*pos).y += d*(0.5f);
		(*vel).y -= 1.9*(*vel).y*(0.5f)*(0.5f);

		// (*acc).y += 10000*(n)*d;
		// (*acc).y += -0.9 * (*vel).y*(n)*(n);
	} 

	d = -n*(gridSize.z - (*pos).z) + h;
	if(d > 0.0f){
		(*pos).z += d*(-0.5f);
		(*vel).z -= 1.9*(*vel).z*(-0.5f)*(-0.5f);

		// (*acc).z += 10000*(-n)*d;
		// (*acc).z += -0.9 * (*vel).z*(-n)*(-n);
	} 

	d = n*(0.0f - (*pos).z) + h;
	if(d > 0.0f){
		(*pos).z += d*(0.5f);
		(*vel).z -= 1.9*(*vel).z*(0.5f)*(0.5f);

		// (*acc).z += 10000*(n)*d;
		// (*acc).z += -0.9 * (*vel).z*(n)*(n);
	} 
}


__global__ void build_spatial_grid_kernel
	(
		float4 *pos, 
		int *particleIndex, 
		int *cellIndex, 
		int numParticles, 
		int3 grid,
		float3 gridSize
	)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int offset = 0;

	while(index + offset < numParticles){
		// find which grid cell the particle is in
		int3 gridPos = calcGridPosition(pos[index + offset], grid, gridSize);

		// compute cell index 
		int cindex = calcCellIndex(gridPos, grid);

		particleIndex[index + offset] = index + offset;
		cellIndex[index + offset] = cindex; 

		offset += blockDim.x*gridDim.x;
	} 
}


__global__ void reorder_particles_kernel
	(
		float4 *pos,
		float4 *spos,
		float4 *vel,
		float4 *svel,
		int *cellStartIndex,
		int *cellEndIndex,
		int *cellIndex,
		int *particleIndex,
		int numParticles
	)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int offset = 0;

	__shared__ int sharedcellIndex[blockSize+1];  //blockSize + 1

	while(index + offset < numParticles){
		sharedcellIndex[threadIdx.x] = cellIndex[index + offset];
		if(threadIdx.x == blockDim.x-1)
		{
			if(index + offset + 1 < numParticles){
				sharedcellIndex[threadIdx.x + 1] = cellIndex[index + offset + 1];
			}
			else{
				sharedcellIndex[threadIdx.x + 1] = -1;
			}
		}

		__syncthreads(); 
	
		if(sharedcellIndex[threadIdx.x] != sharedcellIndex[threadIdx.x + 1]){
			cellStartIndex[sharedcellIndex[threadIdx.x + 1]] = index + offset + 1;
			// cellEndIndex[sharedcellIndex[threadIdx.x]] = index + offset;
			cellEndIndex[sharedcellIndex[threadIdx.x]] = index + offset + 1;
		}

		// reorder position and velocity
		int p = particleIndex[index + offset];
		spos[index + offset] = pos[p];
		svel[index + offset] = vel[p];

		offset += blockDim.x*gridDim.x;
	}

	__syncthreads();

	if(threadIdx.x == 0 && blockIdx.x == 0){
		cellStartIndex[sharedcellIndex[0]] = 0;
	}
}



__global__ void calculate_particle_density_kernel
	(
		float4 *pos, 
		float *rho, 
		float *rho0,
		float *pres,
		int *cellStartIndex,
		int *cellEndIndex,
		int *cellIndex,
		int *particleIndex,
		int numParticles,
		float kappa,
		float h2,
		float h9,
		int3 grid
	)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int offset = 0;
	int maxIndex = grid.x*grid.y*grid.z;

	// compute density
	while(index + offset < numParticles){

		// add contributions from particles in cell (and all of its 26 neighour cells)
		float4 position = pos[index + offset];
		float density = 0.0f;  
		int c = cellIndex[index + offset];
		for(int k=-1; k<=1; k++){
			for(int j=-1; j<=1; j++){
				for(int i=-1; i<=1; i++){
					int hash = c + grid.y*grid.x*k + grid.x*j + i;
					if(hash >= 0 && hash < maxIndex){

						// loop through all particles in this cell
						for(int l=cellStartIndex[hash]; l<cellEndIndex[hash]; l++){ 
							float4 p = pos[l];
							float rx = position.x - p.x;//pos[l].x;
							float ry = position.y - p.y;//pos[l].y;
							float rz = position.z - p.z;//pos[l].z;
							float radius2 = rx*rx + ry*ry + rz*rz;
							if(radius2 < h2){
								density += (h2 - radius2) * (h2 - radius2) * (h2 - radius2);
							}
						}
					}
				}
			}
		}

		density = 0.02f*(f1/h9) * density;

		rho[index + offset] = density;
		pres[index + offset] = kappa*(density - rho0[index + offset]) / (density * density);
		offset += blockDim.x * gridDim.x;
	}
}


__global__ void calculate_pressure_acceleration_kernel
	(
		float4 *pos, 
		float4 *vel,
		float4 *acc, 
		float *rho,
		float *pres,
		int *cellStartIndex,
		int *cellEndIndex,
		int *cellIndex,
		int *particleIndex,
		int numParticles,
		float h,
		float h6,
		int3 grid
	)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int offset = 0;
	int maxIndex = grid.x*grid.y*grid.z;

	// compute pressure gradient and diffusion force
	while(index + offset < numParticles){

		// position, acceleration, and pressure-density squared ratio for this particle
		float4 position = pos[index + offset];
		float4 velocity = vel[index + offset]; 
		float density = rho[index + offset];   
		float prho2 = pres[index + offset];

		float4 fpressure = make_float4(0.0f, 0.0f, 0.0f, 0.0f);
		float4 fviscosity = make_float4(0.0f, 0.0f, 0.0f, 0.0f);

		// add contributions from particles in cell (and all of its 26 neighour cells)
		int c = cellIndex[index + offset];

		for(int k=-1; k<=1; k++){
			for(int j=-1; j<=1; j++){
				for(int i=-1; i<=1; i++){
					int hash = c + grid.y*grid.x*k + grid.x*j + i;
					if(hash >= 0 && hash < maxIndex){

						// loop through all particles in this cell
						for(int l=cellStartIndex[hash]; l<cellEndIndex[hash]; l++){
							if(l != index + offset){
								float4 p = pos[l];
								float rx = position.x - p.x;//pos[l].x;
								float ry = position.y - p.y;//pos[l].y;
								float rz = position.z - p.z;//pos[l].z;
								float radius = rx*rx + ry*ry + rz*rz;

								if(radius < h*h){
									radius = sqrt(radius);

									// pressure force contribution
									float presCoefficient = (prho2 + pres[l]) * (h - radius) * (h - radius) / radius;
									fpressure.x += presCoefficient * rx;
									fpressure.y += presCoefficient * ry;
									fpressure.z += presCoefficient * rz;

									// viscosity force contribution
									float diffCoefficient = (h - radius) / rho[l];
									float vx = (vel[l].x - velocity.x);
									float vy = (vel[l].y - velocity.y);
									float vz = (vel[l].z - velocity.z);
									fviscosity.x += diffCoefficient * vx;
									fviscosity.y += diffCoefficient * vy;
									fviscosity.z += diffCoefficient * vz;
								}	
							}
						}
					}
				}
			}
		}

		fviscosity.x = 0.02f * (f2/h6) * (fpressure.x + nu * fviscosity.x / density);
		fviscosity.y = 0.02f * (f2/h6) * (fpressure.y + nu * fviscosity.y / density);
		fviscosity.z = 0.02f * (f2/h6) * (fpressure.z + nu * fviscosity.z / density);

		acc[index + offset] = fviscosity;

		offset += blockDim.x * gridDim.x;
	}
}


__global__ void update_particles_kernel
	(
		float4 *pos, 
		float4 *vel, 
		float4 *acc, 
		float dt, 
		float h,
		int numParticles,
		float3 gridSize
	)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int offset = 0;

	while(index + offset < numParticles)
	{
		float4 my_pos = pos[index + offset];
	    float4 my_vel = vel[index + offset];
	    float4 my_acc = acc[index + offset];
	    my_vel.x += dt*my_acc.x;   
	    my_vel.y += dt*my_acc.y;
	    my_vel.z += dt*(-9.81f + my_acc.z);

	    my_pos.x += dt*my_vel.x; 
	    my_pos.y += dt*my_vel.y;
	    my_pos.z += dt*my_vel.z;

	    boundary(&my_pos, &my_vel, h, gridSize);

	    pos[index + offset] = my_pos;
	    vel[index + offset] = my_vel;

	    offset += blockDim.x * gridDim.x;
	}
}

__global__ void copy_sph_arrays_kernel
	(
		float4 *pos,
		float4 *spos,
		float4 *vel,
		float4 *svel,
		float *output,
		int numParticles
	)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int offset = 0;

	while(index + offset < numParticles)
	{
		float4 my_pos = pos[index + offset];
		output[3*(index + offset)] = my_pos.x;
		output[3*(index + offset) + 1] = my_pos.y;
		output[3*(index + offset) + 2] = my_pos.z;
		pos[index + offset] = spos[index + offset];
		vel[index + offset] = svel[index + offset];

		offset += blockDim.x * gridDim.x;
	}
}


__global__ void scan_particles_and_mark_voxel_vertices_kernel
	(
		float4 *pos,
		int *voxelVertexFlags,
		int numParticles,
	 	int numVoxels,
	 	int3 grid,
	 	float3 gridSize
	)
{
	int index = threadIdx.x + blockDim.x*blockIdx.x;
	int offset = 0;

	while(index + offset < numParticles){
		float4 position = pos[index+offset];

		int i = calcClosestVoxelVertexIndex(position,grid,gridSize);
		voxelVertexFlags[i] = 1;

		offset += blockDim.x*gridDim.x;
	} 
}



__global__ void generate_density_at_voxel_vertices
	(
		float4 *pos, 
		float *rho, 
		int *cellStartIndex,
		int *cellEndIndex,
		int *voxelVertexFlags, 
		float *voxelVertexValues, 
		int numVoxelVertices,
		float h2,
		float h9,
		int3 grid,
		float3 gridSize
	)
{
	int index = threadIdx.x + blockDim.x*blockIdx.x;
	int offset = 0;

	int maxCellIndex = grid.x*grid.y*grid.z;

	while(index + offset < numVoxelVertices){
		float density = 0.0f;

		if(voxelVertexFlags[index + offset] == 1){
			// add contributions from particles in cells sharing this vertex (8 cells in total)
			int3 voxelGridPos = calcVoxelVertexPosition(index+offset, grid);
			float3 vertexPosition;
			vertexPosition.x = (gridSize.x / grid.x)*voxelGridPos.x;
			vertexPosition.y = (gridSize.y / grid.y)*voxelGridPos.y;
			vertexPosition.z = (gridSize.z / grid.z)*voxelGridPos.z;
		
			int a = (index+offset) / ((grid.x+1)*(grid.y+1)); // integer division
			int vertex = index+offset - a * (grid.x+1)*(grid.y+1);

			for(int i=0;i<2;i++){
				for(int j=0;j<2;j++){
					for(int k=0;k<2;k++){
						int cellIndex = vertex - (vertex/(grid.x+1) + i) - j*grid.x + (a-k)*grid.x*grid.y;
						
						if(cellIndex >=0 && cellIndex < maxCellIndex){
	
							// loop through all particles in this cell
							for(int l=cellStartIndex[cellIndex]; l<cellEndIndex[cellIndex]; l++){ 
								float rx = vertexPosition.x - pos[l].x;
								float ry = vertexPosition.y - pos[l].y;
								float rz = vertexPosition.z - pos[l].z;
								float radius2 = rx*rx + ry*ry + rz*rz;
								if(radius2 < h2){
									density += (h2 - radius2) * (h2 - radius2) * (h2 - radius2);
								}
							}
						}
					}
				}
			}
		}

		voxelVertexValues[index + offset] = 0.01f*(f1/h9) * density;

		offset += blockDim.x*gridDim.x;
	}
}


__global__ void compute_bounding_box_kernel
	(
		float4 *pos, 
		float4 *maxPos, 
		float4 *minPos, 
		int *mutex, 
		int numParticles
	)
{
	int index = threadIdx.x + blockDim.x*blockIdx.x;
	int offset = 0;

	float4 position = pos[index];
	float3 min;
	float3 max;

	min.x = position.x;
	min.y = position.y;
	min.z = position.z;

	max.x = position.x;
	max.y = position.y;
	max.z = position.z;

	__shared__ float3 minCache[blockSize];
	__shared__ float3 maxCache[blockSize];

	while(index + offset < numParticles){
		position = pos[index + offset];
		min.x = fminf(min.x, position.x);
		min.y = fminf(min.y, position.y);
		min.z = fminf(min.z, position.z);

		max.x = fmaxf(max.x, position.x);
		max.y = fmaxf(max.y, position.y);
		max.z = fmaxf(max.z, position.z);

		offset += blockDim.x*gridDim.x;
	}

	minCache[threadIdx.x] = min;
	maxCache[threadIdx.x] = max;

	__syncthreads();

	// assumes blockDim.x is a power of 2
	int i = blockDim.x/2;
	while(i != 0){
		if(threadIdx.x < i){
			minCache[threadIdx.x].x = fminf(minCache[threadIdx.x].x, minCache[threadIdx.x + i].x);
			minCache[threadIdx.x].y = fminf(minCache[threadIdx.x].y, minCache[threadIdx.x + i].y);
			minCache[threadIdx.x].z = fminf(minCache[threadIdx.x].z, minCache[threadIdx.x + i].z);

			maxCache[threadIdx.x].x = fmaxf(maxCache[threadIdx.x].x, maxCache[threadIdx.x + i].x);
			maxCache[threadIdx.x].y = fmaxf(maxCache[threadIdx.x].y, maxCache[threadIdx.x + i].y);
			maxCache[threadIdx.x].z = fmaxf(maxCache[threadIdx.x].z, maxCache[threadIdx.x + i].z);
		}
		__syncthreads();
		i /= 2;
	}

	if(threadIdx.x == 0){
		while (atomicCAS(mutex, 0 ,1) != 0); // lock
		(*minPos).x = fminf((*minPos).x, minCache[0].x);
		(*minPos).y = fminf((*minPos).y, minCache[0].y);
		(*minPos).z = fminf((*minPos).z, minCache[0].z);

		(*maxPos).x = fmaxf((*maxPos).x, maxCache[0].x);
		(*maxPos).y = fmaxf((*maxPos).y, maxCache[0].y);
		(*maxPos).z = fmaxf((*maxPos).z, maxCache[0].z);		

		atomicExch(mutex, 0); // unlock
	}
}


__global__ void build_octtree_kernel
	(
		float4 *pos, 
		int *count,  
		int *child, 
		int *pindex, 
		float4 *minPos,
		float4 *maxPos, 
		int numParticles
	)
{
	int index = threadIdx.x + blockIdx.x*blockDim.x;
	int offset = 0;
	bool newBody = true;

	// build octtree
	float xmin; 
	float xmax; 
	float ymin; 
	float ymax;
	float zmin;
	float zmax;
	int childPath;
	int temp;
	offset = 0;
	while(index + offset < numParticles){
		count[index + offset] = 1;

		if(newBody){
			newBody = false;

			xmin = minPos->x;
			xmax = maxPos->x;
			ymin = minPos->y;
			ymax = maxPos->y;
			zmin = minPos->z;
			zmax = maxPos->z;

			temp = 0;
			childPath = 0;
			if(pos[index + offset].x < 0.5f*(xmin + xmax)){
				childPath += 1;
				xmax = 0.5f*(xmin + xmax);
			}
			else{
				xmin = 0.5f*(xmin + xmax);
			}
			if(pos[index + offset].y < 0.5f*(ymin + ymax)){
				childPath += 2;	
				ymax = 0.5f*(ymin + ymax);
			}
			else{
				ymin = 0.5f*(ymin + ymax);
			}
			if(pos[index + offset].z < 0.5f*(zmin + zmax)){
				childPath += 4;	
				zmax = 0.5f*(zmin + zmax);
			}
			else{
				zmin = 0.5f*(zmin + zmax);
			}
		}
		int childIndex = child[temp*8 + childPath];

		// traverse tree until we hit leaf node
		while(childIndex >= numParticles){
			temp = childIndex;
			childPath = 0;
			if(pos[index + offset].x < 0.5f*(xmin + xmax)){
				childPath += 1;
				xmax = 0.5f*(xmin + xmax);
			}
			else{
				xmin = 0.5f*(xmin + xmax);
			}
			if(pos[index + offset].y < 0.5f*(ymin + ymax)){
				childPath += 2;	
				ymax = 0.5f*(ymin + ymax);
			}
			else{
				ymin = 0.5f*(ymin + ymax);
			}
			if(pos[index + offset].z < 0.5f*(zmin + zmax)){
				childPath += 4;	
				zmax = 0.5f*(zmin + zmax);
			}
			else{
				zmin = 0.5f*(zmin + zmax);
			}

			atomicAdd(&count[temp], 1);
			// childIndex = child[8*temp - 8*numParticles + 8 + childPath];
			childIndex = child[8*temp + childPath];
		}

		if(childIndex != -2){
			int locked = temp*8 + childPath;
			// int locked = 8*temp - 8*numParticles + 8 + childPath;
			if(atomicCAS(&child[locked], childIndex, -2) == childIndex){
				if(childIndex == -1){
					child[locked] = index + offset;
				}
				else{
					int patch = 8*numParticles;
					while(childIndex >= 0 && childIndex < numParticles){
						
				 		int cell = atomicAdd(pindex,1);
				 		patch = min(patch, cell);
				 		if(patch != cell){
				 			child[8*temp + childPath] = cell;
				 			// child[8*temp - 8*numParticles + 8 + childPath] = cell;
				 		}

                     	// insert old particle
				 		childPath = 0;
				 		if(pos[childIndex].x < 0.5f*(xmin + xmax)){
							childPath += 1;
						}
						if(pos[childIndex].y < 0.5f*(ymin + ymax)){
							childPath += 2;	
						}
						if(pos[childIndex].z < 0.5f*(zmin + zmax)){
							childPath += 4;	
						}

						if(true){
				 			if(cell >= 2*numParticles+12000){
				 				printf("error cell index is too large!! cell is: %d\n", cell);
				 			}
				 		}

				 		count[cell] += count[childIndex];
				 		child[8*cell + childPath] = childIndex;
				 		// child[8*cell - 8*numParticles + 8 + childPath] = childIndex;

                     	// insert new particle
				 		temp = cell;
				 		childPath = 0;
				 		if(pos[index + offset].x < 0.5f*(xmin + xmax)){
							childPath += 1;
							xmax = 0.5f*(xmin + xmax);
						}
						else{
							xmin = 0.5f*(xmin + xmax);
						}
						if(pos[index + offset].y < 0.5f*(ymin + ymax)){
							childPath += 2;	
							ymax = 0.5f*(ymin + ymax);
						}
						else{
							ymin = 0.5f*(ymin + ymax);
						}
						if(pos[index + offset].z < 0.5f*(zmin + zmax)){
							childPath += 4;	
							zmax = 0.5f*(zmin + zmax);
						}
						else{
							zmin = 0.5f*(zmin + zmax);
						}

				 		count[cell] += count[index + offset];
				 		childIndex = child[8*temp + childPath];
				 		// childIndex = child[8*temp - 8*numParticles + 8 + childPath];
				 	}
				
				 	child[8*temp + childPath] = index + offset;
				 	// child[8*temp - 8*numParticles + 8 + childPath] = index + offset;

				 	// __threadfence();  // we have been writing to global memory arrays (child, x, y, mass) thus need to fence

				 	child[locked] = patch;
				}

				// __threadfence(); // we have been writing to global memory arrays (child, x, y, mass) thus need to fence

				offset += blockDim.x*gridDim.x;
				newBody = true;
			}
		}

		__syncthreads(); // not strictly needed 
	}
}


__global__ void sort_kernel
	(
		int *count, 
		int *start, 
		int *sorted, 
		int *child, 
		int *pindex, 
		int numParticles
	)
{
	int index = threadIdx.x + blockIdx.x*blockDim.x;
	int stride = blockDim.x*gridDim.x;
	int offset = 0;

	int s = 0;
	if(threadIdx.x == 0){
		for(int i=0;i<8;i++){
			int node = child[i];

			if(node >= numParticles){  // not a leaf node
				start[node] = s;
				s += count[node];
			}
			else if(node >= 0){  // leaf node
				sorted[s] = node;
				s++;
			}
		}
	}

	int cell = numParticles + index;
	int ind = *pindex;
	while((cell + offset) < ind){
		s = start[cell + offset];

		if(s >= 0){

			for(int i=0;i<8;i++){
				int node = child[8*(cell+offset) + i];

				if(node >= numParticles){  // not a leaf node
					start[node] = s;
					s += count[node];
				}
				else if(node >= 0){  // leaf node
					sorted[s] = node;
					s++;
				}
			}
			offset += stride;
		}
	}
}


__global__ void calculate_particle_density_octtree_kernel
	(
		float4 *pos, 
		float *rho,
		float *rho0,
		float *pres,  
		int *sorted, 
		int *child, 
		int *nodeStack,
		float3 *minStack,
		float3 *maxStack,
		float4 *minPos,
		float4 *maxPos, 
		int numParticles,
		float kappa,
		float h2,
		float h9
	)
{
	int index = threadIdx.x + blockIdx.x*blockDim.x;
	int offset = 0; 

	// __shared__ int sharedStack[32*blockSize];

	while(index + offset < numParticles){
		int sortedIndex = sorted[index + offset];

		float4 position = pos[sortedIndex];
		float density = 0.0f;  

		int stackStartIndex = index;
		// int stackStartIndex = threadIdx.x;
		int top = stackStartIndex; 
		int node = 0;
		float3 dmin = make_float3(minPos->x, minPos->y, minPos->z);
		float3 dmax = make_float3(maxPos->x, maxPos->y, maxPos->z);

		__syncthreads(); 

		// while stack is not empty
		while(top >= stackStartIndex){
			for(int i=0;i<8;i++){
				int ch = child[8*node + i];

				if(ch != -1 && InRangeOfQuadrant(position, dmin, dmax, h2, i)){
					if(ch < numParticles /* leaf node */){
						float rx = position.x - pos[ch].x;
						float ry = position.y - pos[ch].y;
						float rz = position.z - pos[ch].z;
						float radius2 = rx*rx + ry*ry + rz*rz;
						if(radius2 <= h2){
							density += (h2 - radius2) * (h2 - radius2) * (h2 - radius2);
						}
					}
					else{ /*not a leaf node */
						float3 ddmin = dmin;
						float3 ddmax = dmax;

						if(i & (1 << 0)){ 
							ddmax.x = 0.5f*(dmax.x + dmin.x); 
						}
						else{
							ddmin.x = 0.5f*(dmax.x + dmin.x);
						}
						if(i & (1 << 1)){ 
							ddmax.y = 0.5f*(dmax.y + dmin.y); 
						}
						else{
							ddmin.y = 0.5f*(dmax.y + dmin.y);
						}
						if(i & (1 << 2)){ 
							ddmax.z = 0.5f*(dmax.z + dmin.z);
						}
						else{
							ddmin.z = 0.5f*(dmax.z + dmin.z); 
						}

						nodeStack[top] = ch;
						// sharedStack[top] = ch;
						minStack[top] = ddmin;
						maxStack[top] = ddmax;

						top += numParticles;
						// top += blockSize;
					}
				}
			}

			top -= numParticles;
			if(top >= 0){ 
				node = nodeStack[top]; 
				dmin = minStack[top];
				dmax = maxStack[top];
			}

			// top -= blockSize;
			// if(top >= 0){ node = sharedStack[top]; }
		}

 		density = 0.01f*(f1/h9) * density;

		rho[sortedIndex] = density;
		pres[sortedIndex] = kappa*(density - rho0[sortedIndex]) / (density * density);
		offset += blockDim.x * gridDim.x;    

   		__syncthreads();
   	}
}


__global__ void calculate_particle_forces_octtree_kernel
	(
		float4 *pos,
		float4 *vel,
		float4 *acc, 
		float *rho,
		float *pres,  
		int *sorted, 
		int *child, 
		int *nodeStack,
		float3 *minStack,
		float3 *maxStack,
		float4 *minPos,
		float4 *maxPos, 
		int numParticles,
		float h,
		float h6
	)
{
	int index = threadIdx.x + blockIdx.x*blockDim.x;
	int offset = 0; 

	while(index + offset < numParticles){
		int sortedIndex = sorted[index + offset];

		float4 position = pos[sortedIndex];
		float4 velocity = vel[sortedIndex]; 
		float density = rho[sortedIndex];   
		float prho2 = pres[sortedIndex];

		float4 fpressure = make_float4(0.0f, 0.0f, 0.0f, 0.0f);
		float4 fviscosity = make_float4(0.0f, 0.0f, 0.0f, 0.0f);

		int stackStartIndex = index;
		int top = stackStartIndex;
		int node = 0;
		float3 dmin = make_float3(minPos->x, minPos->y, minPos->z);
		float3 dmax = make_float3(maxPos->x, maxPos->y, maxPos->z);

		__syncthreads();

		// while stack is not empty
		while(top >= stackStartIndex){
			for(int i=0;i<8;i++){
				int ch = child[8*node + i];
			
				if(ch != -1 && InRangeOfQuadrant(position, dmin, dmax, h*h, i)){
					if(ch < numParticles /* leaf node */){
						float rx = position.x - pos[ch].x;
						float ry = position.y - pos[ch].y;
						float rz = position.z - pos[ch].z;
						float radius = rx*rx + ry*ry + rz*rz;
						if(radius <= h*h && ch != sortedIndex){
							radius = sqrt(radius);

							// pressure force contribution
							float presCoefficient = (prho2 + pres[ch]) * (h - radius) * (h - radius) / radius;
							fpressure.x += presCoefficient * rx;
							fpressure.y += presCoefficient * ry;
							fpressure.z += presCoefficient * rz;

							// viscosity force contribution
							float diffCoefficient = (h - radius) / rho[ch];
							float vx = (vel[ch].x - velocity.x);
							float vy = (vel[ch].y - velocity.y);
							float vz = (vel[ch].z - velocity.z);
							fviscosity.x += diffCoefficient * vx;
							fviscosity.y += diffCoefficient * vy;
							fviscosity.z += diffCoefficient * vz;
						}
					}
					else{ /*not a leaf node */
						float3 ddmin = dmin;
						float3 ddmax = dmax;

						if(i & (1 << 0)){ 
							ddmax.x = 0.5f*(dmax.x + dmin.x); 
						}
						else{
							ddmin.x = 0.5f*(dmax.x + dmin.x);
						}
						if(i & (1 << 1)){ 
							ddmax.y = 0.5f*(dmax.y + dmin.y); 
						}
						else{
							ddmin.y = 0.5f*(dmax.y + dmin.y);
						}
						if(i & (1 << 2)){ 
							ddmax.z = 0.5f*(dmax.z + dmin.z);
						}
						else{
							ddmin.z = 0.5f*(dmax.z + dmin.z); 
						}

						nodeStack[top] = ch;
						// sharedStack[top] = ch;
						minStack[top] = ddmin;
						maxStack[top] = ddmax;

						top += numParticles;
						// top += blockSize;
					}
				}
			}

			top -= numParticles;
			if(top >= 0){ 
				node = nodeStack[top]; 
				dmin = minStack[top];
				dmax = maxStack[top];
			}
		}

 		fviscosity.x = 0.01f * (f2/h6) * (fpressure.x + nu * fviscosity.x / density);
		fviscosity.y = 0.01f * (f2/h6) * (fpressure.y + nu * fviscosity.y / density);
		fviscosity.z = 0.01f * (f2/h6) * (fpressure.z + nu * fviscosity.z / density);

		acc[sortedIndex] = fviscosity;

		offset += blockDim.x * gridDim.x;   

   		__syncthreads();
   	}
}


__global__ void update_particles_octtree_kernel
	(
		float4 *pos, 
		float4 *vel, 
		float4 *acc,
		float *output,
		float dt, 
		float h,
		int numParticles,
		float3 gridSize
	)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int offset = 0;

	while(index + offset < numParticles)
	{
		float4 my_pos = pos[index + offset];
	    float4 my_vel = vel[index + offset];
	    float4 my_acc = acc[index + offset];
	    my_vel.x += dt*my_acc.x;   
	    my_vel.y += dt*my_acc.y;
	    my_vel.z += dt*(-9.81f + my_acc.z);

	    my_pos.x += dt*my_vel.x; 
	    my_pos.y += dt*my_vel.y;
	    my_pos.z += dt*my_vel.z;

	    boundary(&my_pos, &my_vel, h, gridSize);

	    pos[index + offset] = my_pos;
	    vel[index + offset] = my_vel;

	    output[3*(index + offset)] = my_pos.x;
		output[3*(index + offset) + 1] = my_pos.y;
		output[3*(index + offset) + 2] = my_pos.z;

	    offset += blockDim.x * gridDim.x;
	}
}






















// Marching Cubes reference cube
// 
//                   vertices                             edges
//                  ----------                           -------
//
//           v4                     v5                      e4          
//            /-------------------/                /-------------------/
//           /|                  /|               /|                  /|
//          / |                 / |              / |                 / |
//         /  |                /  |           e7/  |              e5/  |
//        /   |               /   |            /   |e8             /   |e9
//    v7 /    |              /    |           /    |    e6        /    |
//      |-------------------/v6   |          |-------------------/     |
//      |     |             |     |          |     |             |     |  
//      |   v0|_____________|_____|          |     |_____________|_____|
//      |    /              |    / v1        |    /       e0     |    /
//      |   /               |   /         e11|   /               |   /
//      |  /                |  /             |  /e3           e10|  /e1
//      | /                 | /              | /                 | /
//      |/__________________|/               |/__________________|/
//     v3                     v2                       e2


__constant__ int numTrianglesByCase[256] = 
{0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 2, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 3,  
1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 3, 2, 3, 3, 2, 3, 4, 4, 3, 3, 4, 4, 3, 4, 5, 5, 2,  
1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 3, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 4,  
2, 3, 3, 4, 3, 4, 2, 3, 3, 4, 4, 5, 4, 5, 3, 2, 3, 4, 4, 3, 4, 5, 3, 2, 4, 5, 5, 4, 5, 2, 4, 1,  
1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 3, 2, 3, 3, 4, 3, 4, 4, 5, 3, 2, 4, 3, 4, 3, 5, 2,  
2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 4, 3, 4, 4, 3, 4, 5, 5, 4, 4, 3, 5, 2, 5, 4, 2, 1,  
2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 2, 3, 3, 2, 3, 4, 4, 5, 4, 5, 5, 2, 4, 3, 5, 4, 3, 2, 4, 1,  
3, 4, 4, 5, 4, 5, 3, 4, 4, 5, 5, 2, 3, 4, 2, 1, 2, 3, 3, 2, 3, 4, 2, 1, 3, 2, 4, 1, 2, 1, 1, 0};


__constant__ int triangleTable[256][15] = 
{{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 1, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 8, 3, 9, 8, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 8, 3, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{9, 2, 10, 0, 2, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{2, 8, 3, 2, 10, 8, 10, 9, 8, -1, -1, -1, -1, -1, -1},
{3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 11, 2, 8, 11, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 9, 0, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 11, 2, 1, 9, 11, 9, 8, 11, -1, -1, -1, -1, -1, -1},
{3, 10, 1, 11, 10, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 10, 1, 0, 8, 10, 8, 11, 10, -1, -1, -1, -1, -1, -1},
{3, 9, 0, 3, 11, 9, 11, 10, 9, -1, -1, -1, -1, -1, -1},
{9, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{4, 3, 0, 7, 3, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 1, 9, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{4, 1, 9, 4, 7, 1, 7, 3, 1, -1, -1, -1, -1, -1, -1},
{1, 2, 10, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{3, 4, 7, 3, 0, 4, 1, 2, 10, -1, -1, -1, -1, -1, -1},
{9, 2, 10, 9, 0, 2, 8, 4, 7, -1, -1, -1, -1, -1, -1},
{2, 10, 9, 2, 9, 7, 2, 7, 3, 7, 9, 4, -1, -1, -1},
{8, 4, 7, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{11, 4, 7, 11, 2, 4, 2, 0, 4, -1, -1, -1, -1, -1, -1},
{9, 0, 1, 8, 4, 7, 2, 3, 11, -1, -1, -1, -1, -1, -1},
{4, 7, 11, 9, 4, 11, 9, 11, 2, 9, 2, 1, -1, -1, -1},
{3, 10, 1, 3, 11, 10, 7, 8, 4, -1, -1, -1, -1, -1, -1},
{1, 11, 10, 1, 4, 11, 1, 0, 4, 7, 11, 4, -1, -1, -1},
{4, 7, 8, 9, 0, 11, 9, 11, 10, 11, 0, 3, -1, -1, -1},
{4, 7, 11, 4, 11, 9, 9, 11, 10, -1, -1, -1, -1, -1, -1},
{9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{9, 5, 4, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 5, 4, 1, 5, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{8, 5, 4, 8, 3, 5, 3, 1, 5, -1, -1, -1, -1, -1, -1},
{1, 2, 10, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{3, 0, 8, 1, 2, 10, 4, 9, 5, -1, -1, -1, -1, -1, -1},
{5, 2, 10, 5, 4, 2, 4, 0, 2, -1, -1, -1, -1, -1, -1},
{2, 10, 5, 3, 2, 5, 3, 5, 4, 3, 4, 8, -1, -1, -1},
{9, 5, 4, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 11, 2, 0, 8, 11, 4, 9, 5, -1, -1, -1, -1, -1, -1},
{0, 5, 4, 0, 1, 5, 2, 3, 11, -1, -1, -1, -1, -1, -1},
{2, 1, 5, 2, 5, 8, 2, 8, 11, 4, 8, 5, -1, -1, -1},
{10, 3, 11, 10, 1, 3, 9, 5, 4, -1, -1, -1, -1, -1, -1},
{4, 9, 5, 0, 8, 1, 8, 10, 1, 8, 11, 10, -1, -1, -1},
{5, 4, 0, 5, 0, 11, 5, 11, 10, 11, 0, 3, -1, -1, -1},
{5, 4, 8, 5, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1},
{9, 7, 8, 5, 7, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{9, 3, 0, 9, 5, 3, 5, 7, 3, -1, -1, -1, -1, -1, -1},
{0, 7, 8, 0, 1, 7, 1, 5, 7, -1, -1, -1, -1, -1, -1},
{1, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{9, 7, 8, 9, 5, 7, 10, 1, 2, -1, -1, -1, -1, -1, -1},
{10, 1, 2, 9, 5, 0, 5, 3, 0, 5, 7, 3, -1, -1, -1},
{8, 0, 2, 8, 2, 5, 8, 5, 7, 10, 5, 2, -1, -1, -1},
{2, 10, 5, 2, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1},
{7, 9, 5, 7, 8, 9, 3, 11, 2, -1, -1, -1, -1, -1, -1},
{9, 5, 7, 9, 7, 2, 9, 2, 0, 2, 7, 11, -1, -1, -1},
{2, 3, 11, 0, 1, 8, 1, 7, 8, 1, 5, 7, -1, -1, -1},
{11, 2, 1, 11, 1, 7, 7, 1, 5, -1, -1, -1, -1, -1, -1},
{9, 5, 8, 8, 5, 7, 10, 1, 3, 10, 3, 11, -1, -1, -1},
{5, 7, 0, 5, 0, 9, 7, 11, 0, 1, 0, 10, 11, 10, 0},
{11, 10, 0, 11, 0, 3, 10, 5, 0, 8, 0, 7, 5, 7, 0},
{11, 10, 5, 7, 11, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 8, 3, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{9, 0, 1, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 8, 3, 1, 9, 8, 5, 10, 6, -1, -1, -1, -1, -1, -1},
{1, 6, 5, 2, 6, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 6, 5, 1, 2, 6, 3, 0, 8, -1, -1, -1, -1, -1, -1},
{9, 6, 5, 9, 0, 6, 0, 2, 6, -1, -1, -1, -1, -1, -1},
{5, 9, 8, 5, 8, 2, 5, 2, 6, 3, 2, 8, -1, -1, -1},
{2, 3, 11, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{11, 0, 8, 11, 2, 0, 10, 6, 5, -1, -1, -1, -1, -1, -1},
{0, 1, 9, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1, -1, -1},
{5, 10, 6, 1, 9, 2, 9, 11, 2, 9, 8, 11, -1, -1, -1},
{6, 3, 11, 6, 5, 3, 5, 1, 3, -1, -1, -1, -1, -1, -1},
{0, 8, 11, 0, 11, 5, 0, 5, 1, 5, 11, 6, -1, -1, -1},
{3, 11, 6, 0, 3, 6, 0, 6, 5, 0, 5, 9, -1, -1, -1},
{6, 5, 9, 6, 9, 11, 11, 9, 8, -1, -1, -1, -1, -1, -1},
{5, 10, 6, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{4, 3, 0, 4, 7, 3, 6, 5, 10, -1, -1, -1, -1, -1, -1},
{1, 9, 0, 5, 10, 6, 8, 4, 7, -1, -1, -1, -1, -1, -1},
{10, 6, 5, 1, 9, 7, 1, 7, 3, 7, 9, 4, -1, -1, -1},
{6, 1, 2, 6, 5, 1, 4, 7, 8, -1, -1, -1, -1, -1, -1},
{1, 2, 5, 5, 2, 6, 3, 0, 4, 3, 4, 7, -1, -1, -1},
{8, 4, 7, 9, 0, 5, 0, 6, 5, 0, 2, 6, -1, -1, -1},
{7, 3, 9, 7, 9, 4, 3, 2, 9, 5, 9, 6, 2, 6, 9},
{3, 11, 2, 7, 8, 4, 10, 6, 5, -1, -1, -1, -1, -1, -1},
{5, 10, 6, 4, 7, 2, 4, 2, 0, 2, 7, 11, -1, -1, -1},
{0, 1, 9, 4, 7, 8, 2, 3, 11, 5, 10, 6, -1, -1, -1},
{9, 2, 1, 9, 11, 2, 9, 4, 11, 7, 11, 4, 5, 10, 6},
{8, 4, 7, 3, 11, 5, 3, 5, 1, 5, 11, 6, -1, -1, -1},
{5, 1, 11, 5, 11, 6, 1, 0, 11, 7, 11, 4, 0, 4, 11},
{0, 5, 9, 0, 6, 5, 0, 3, 6, 11, 6, 3, 8, 4, 7},
{6, 5, 9, 6, 9, 11, 4, 7, 9, 7, 11, 9, -1, -1, -1},
{10, 4, 9, 6, 4, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{4, 10, 6, 4, 9, 10, 0, 8, 3, -1, -1, -1, -1, -1, -1},
{10, 0, 1, 10, 6, 0, 6, 4, 0, -1, -1, -1, -1, -1, -1},
{8, 3, 1, 8, 1, 6, 8, 6, 4, 6, 1, 10, -1, -1, -1},
{1, 4, 9, 1, 2, 4, 2, 6, 4, -1, -1, -1, -1, -1, -1},
{3, 0, 8, 1, 2, 9, 2, 4, 9, 2, 6, 4, -1, -1, -1},
{0, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{8, 3, 2, 8, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1},
{10, 4, 9, 10, 6, 4, 11, 2, 3, -1, -1, -1, -1, -1, -1},
{0, 8, 2, 2, 8, 11, 4, 9, 10, 4, 10, 6, -1, -1, -1},
{3, 11, 2, 0, 1, 6, 0, 6, 4, 6, 1, 10, -1, -1, -1},
{6, 4, 1, 6, 1, 10, 4, 8, 1, 2, 1, 11, 8, 11, 1},
{9, 6, 4, 9, 3, 6, 9, 1, 3, 11, 6, 3, -1, -1, -1},
{8, 11, 1, 8, 1, 0, 11, 6, 1, 9, 1, 4, 6, 4, 1},
{3, 11, 6, 3, 6, 0, 0, 6, 4, -1, -1, -1, -1, -1, -1},
{6, 4, 8, 11, 6, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{7, 10, 6, 7, 8, 10, 8, 9, 10, -1, -1, -1, -1, -1, -1},
{0, 7, 3, 0, 10, 7, 0, 9, 10, 6, 7, 10, -1, -1, -1},
{10, 6, 7, 1, 10, 7, 1, 7, 8, 1, 8, 0, -1, -1, -1},
{10, 6, 7, 10, 7, 1, 1, 7, 3, -1, -1, -1, -1, -1, -1},
{1, 2, 6, 1, 6, 8, 1, 8, 9, 8, 6, 7, -1, -1, -1},
{2, 6, 9, 2, 9, 1, 6, 7, 9, 0, 9, 3, 7, 3, 9},
{7, 8, 0, 7, 0, 6, 6, 0, 2, -1, -1, -1, -1, -1, -1},
{7, 3, 2, 6, 7, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{2, 3, 11, 10, 6, 8, 10, 8, 9, 8, 6, 7, -1, -1, -1},
{2, 0, 7, 2, 7, 11, 0, 9, 7, 6, 7, 10, 9, 10, 7},
{1, 8, 0, 1, 7, 8, 1, 10, 7, 6, 7, 10, 2, 3, 11},
{11, 2, 1, 11, 1, 7, 10, 6, 1, 6, 7, 1, -1, -1, -1},
{8, 9, 6, 8, 6, 7, 9, 1, 6, 11, 6, 3, 1, 3, 6},
{0, 9, 1, 11, 6, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{7, 8, 0, 7, 0, 6, 3, 11, 0, 11, 6, 0, -1, -1, -1},
{7, 11, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{3, 0, 8, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 1, 9, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{8, 1, 9, 8, 3, 1, 11, 7, 6, -1, -1, -1, -1, -1, -1},
{10, 1, 2, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 2, 10, 3, 0, 8, 6, 11, 7, -1, -1, -1, -1, -1, -1},
{2, 9, 0, 2, 10, 9, 6, 11, 7, -1, -1, -1, -1, -1, -1},
{6, 11, 7, 2, 10, 3, 10, 8, 3, 10, 9, 8, -1, -1, -1},
{7, 2, 3, 6, 2, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{7, 0, 8, 7, 6, 0, 6, 2, 0, -1, -1, -1, -1, -1, -1},
{2, 7, 6, 2, 3, 7, 0, 1, 9, -1, -1, -1, -1, -1, -1},
{1, 6, 2, 1, 8, 6, 1, 9, 8, 8, 7, 6, -1, -1, -1},
{10, 7, 6, 10, 1, 7, 1, 3, 7, -1, -1, -1, -1, -1, -1},
{10, 7, 6, 1, 7, 10, 1, 8, 7, 1, 0, 8, -1, -1, -1},
{0, 3, 7, 0, 7, 10, 0, 10, 9, 6, 10, 7, -1, -1, -1},
{7, 6, 10, 7, 10, 8, 8, 10, 9, -1, -1, -1, -1, -1, -1},
{6, 8, 4, 11, 8, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{3, 6, 11, 3, 0, 6, 0, 4, 6, -1, -1, -1, -1, -1, -1},
{8, 6, 11, 8, 4, 6, 9, 0, 1, -1, -1, -1, -1, -1, -1},
{9, 4, 6, 9, 6, 3, 9, 3, 1, 11, 3, 6, -1, -1, -1},
{6, 8, 4, 6, 11, 8, 2, 10, 1, -1, -1, -1, -1, -1, -1},
{1, 2, 10, 3, 0, 11, 0, 6, 11, 0, 4, 6, -1, -1, -1},
{4, 11, 8, 4, 6, 11, 0, 2, 9, 2, 10, 9, -1, -1, -1},
{10, 9, 3, 10, 3, 2, 9, 4, 3, 11, 3, 6, 4, 6, 3},
{8, 2, 3, 8, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1},
{0, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 9, 0, 2, 3, 4, 2, 4, 6, 4, 3, 8, -1, -1, -1},
{1, 9, 4, 1, 4, 2, 2, 4, 6, -1, -1, -1, -1, -1, -1},
{8, 1, 3, 8, 6, 1, 8, 4, 6, 6, 10, 1, -1, -1, -1},
{10, 1, 0, 10, 0, 6, 6, 0, 4, -1, -1, -1, -1, -1, -1},
{4, 6, 3, 4, 3, 8, 6, 10, 3, 0, 3, 9, 10, 9, 3},
{10, 9, 4, 6, 10, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{4, 9, 5, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 8, 3, 4, 9, 5, 11, 7, 6, -1, -1, -1, -1, -1, -1},
{5, 0, 1, 5, 4, 0, 7, 6, 11, -1, -1, -1, -1, -1, -1},
{11, 7, 6, 8, 3, 4, 3, 5, 4, 3, 1, 5, -1, -1, -1},
{9, 5, 4, 10, 1, 2, 7, 6, 11, -1, -1, -1, -1, -1, -1},
{6, 11, 7, 1, 2, 10, 0, 8, 3, 4, 9, 5, -1, -1, -1},
{7, 6, 11, 5, 4, 10, 4, 2, 10, 4, 0, 2, -1, -1, -1},
{3, 4, 8, 3, 5, 4, 3, 2, 5, 10, 5, 2, 11, 7, 6},
{7, 2, 3, 7, 6, 2, 5, 4, 9, -1, -1, -1, -1, -1, -1},
{9, 5, 4, 0, 8, 6, 0, 6, 2, 6, 8, 7, -1, -1, -1},
{3, 6, 2, 3, 7, 6, 1, 5, 0, 5, 4, 0, -1, -1, -1},
{6, 2, 8, 6, 8, 7, 2, 1, 8, 4, 8, 5, 1, 5, 8},
{9, 5, 4, 10, 1, 6, 1, 7, 6, 1, 3, 7, -1, -1, -1},
{1, 6, 10, 1, 7, 6, 1, 0, 7, 8, 7, 0, 9, 5, 4},
{4, 0, 10, 4, 10, 5, 0, 3, 10, 6, 10, 7, 3, 7, 10},
{7, 6, 10, 7, 10, 8, 5, 4, 10, 4, 8, 10, -1, -1, -1},
{6, 9, 5, 6, 11, 9, 11, 8, 9, -1, -1, -1, -1, -1, -1},
{3, 6, 11, 0, 6, 3, 0, 5, 6, 0, 9, 5, -1, -1, -1},
{0, 11, 8, 0, 5, 11, 0, 1, 5, 5, 6, 11, -1, -1, -1},
{6, 11, 3, 6, 3, 5, 5, 3, 1, -1, -1, -1, -1, -1, -1},
{1, 2, 10, 9, 5, 11, 9, 11, 8, 11, 5, 6, -1, -1, -1},
{0, 11, 3, 0, 6, 11, 0, 9, 6, 5, 6, 9, 1, 2, 10},
{11, 8, 5, 11, 5, 6, 8, 0, 5, 10, 5, 2, 0, 2, 5},
{6, 11, 3, 6, 3, 5, 2, 10, 3, 10, 5, 3, -1, -1, -1},
{5, 8, 9, 5, 2, 8, 5, 6, 2, 3, 8, 2, -1, -1, -1},
{9, 5, 6, 9, 6, 0, 0, 6, 2, -1, -1, -1, -1, -1, -1},
{1, 5, 8, 1, 8, 0, 5, 6, 8, 3, 8, 2, 6, 2, 8},
{1, 5, 6, 2, 1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 3, 6, 1, 6, 10, 3, 8, 6, 5, 6, 9, 8, 9, 6},
{10, 1, 0, 10, 0, 6, 9, 5, 0, 5, 6, 0, -1, -1, -1},
{0, 3, 8, 5, 6, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{10, 5, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{11, 5, 10, 7, 5, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{11, 5, 10, 11, 7, 5, 8, 3, 0, -1, -1, -1, -1, -1, -1},
{5, 11, 7, 5, 10, 11, 1, 9, 0, -1, -1, -1, -1, -1, -1},
{10, 7, 5, 10, 11, 7, 9, 8, 1, 8, 3, 1, -1, -1, -1},
{11, 1, 2, 11, 7, 1, 7, 5, 1, -1, -1, -1, -1, -1, -1},
{0, 8, 3, 1, 2, 7, 1, 7, 5, 7, 2, 11, -1, -1, -1},
{9, 7, 5, 9, 2, 7, 9, 0, 2, 2, 11, 7, -1, -1, -1},
{7, 5, 2, 7, 2, 11, 5, 9, 2, 3, 2, 8, 9, 8, 2},
{2, 5, 10, 2, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1},
{8, 2, 0, 8, 5, 2, 8, 7, 5, 10, 2, 5, -1, -1, -1},
{9, 0, 1, 5, 10, 3, 5, 3, 7, 3, 10, 2, -1, -1, -1},
{9, 8, 2, 9, 2, 1, 8, 7, 2, 10, 2, 5, 7, 5, 2},
{1, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 8, 7, 0, 7, 1, 1, 7, 5, -1, -1, -1, -1, -1, -1},
{9, 0, 3, 9, 3, 5, 5, 3, 7, -1, -1, -1, -1, -1, -1},
{9, 8, 7, 5, 9, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{5, 8, 4, 5, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1},
{5, 0, 4, 5, 11, 0, 5, 10, 11, 11, 3, 0, -1, -1, -1},
{0, 1, 9, 8, 4, 10, 8, 10, 11, 10, 4, 5, -1, -1, -1},
{10, 11, 4, 10, 4, 5, 11, 3, 4, 9, 4, 1, 3, 1, 4},
{2, 5, 1, 2, 8, 5, 2, 11, 8, 4, 5, 8, -1, -1, -1},
{0, 4, 11, 0, 11, 3, 4, 5, 11, 2, 11, 1, 5, 1, 11},
{0, 2, 5, 0, 5, 9, 2, 11, 5, 4, 5, 8, 11, 8, 5},
{9, 4, 5, 2, 11, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{2, 5, 10, 3, 5, 2, 3, 4, 5, 3, 8, 4, -1, -1, -1},
{5, 10, 2, 5, 2, 4, 4, 2, 0, -1, -1, -1, -1, -1, -1},
{3, 10, 2, 3, 5, 10, 3, 8, 5, 4, 5, 8, 0, 1, 9},
{5, 10, 2, 5, 2, 4, 1, 9, 2, 9, 4, 2, -1, -1, -1},
{8, 4, 5, 8, 5, 3, 3, 5, 1, -1, -1, -1, -1, -1, -1},
{0, 4, 5, 1, 0, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{8, 4, 5, 8, 5, 3, 9, 0, 5, 0, 3, 5, -1, -1, -1},
{9, 4, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{4, 11, 7, 4, 9, 11, 9, 10, 11, -1, -1, -1, -1, -1, -1},
{0, 8, 3, 4, 9, 7, 9, 11, 7, 9, 10, 11, -1, -1, -1},
{1, 10, 11, 1, 11, 4, 1, 4, 0, 7, 4, 11, -1, -1, -1},
{3, 1, 4, 3, 4, 8, 1, 10, 4, 7, 4, 11, 10, 11, 4},
{4, 11, 7, 9, 11, 4, 9, 2, 11, 9, 1, 2, -1, -1, -1},
{9, 7, 4, 9, 11, 7, 9, 1, 11, 2, 11, 1, 0, 8, 3},
{11, 7, 4, 11, 4, 2, 2, 4, 0, -1, -1, -1, -1, -1, -1},
{11, 7, 4, 11, 4, 2, 8, 3, 4, 3, 2, 4, -1, -1, -1},
{2, 9, 10, 2, 7, 9, 2, 3, 7, 7, 4, 9, -1, -1, -1},
{9, 10, 7, 9, 7, 4, 10, 2, 7, 8, 7, 0, 2, 0, 7},
{3, 7, 10, 3, 10, 2, 7, 4, 10, 1, 10, 0, 4, 0, 10},
{1, 10, 2, 8, 7, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{4, 9, 1, 4, 1, 7, 7, 1, 3, -1, -1, -1, -1, -1, -1},
{4, 9, 1, 4, 1, 7, 0, 8, 1, 8, 7, 1, -1, -1, -1},
{4, 0, 3, 7, 4, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{4, 8, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{9, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{3, 0, 9, 3, 9, 11, 11, 9, 10, -1, -1, -1, -1, -1, -1},
{0, 1, 10, 0, 10, 8, 8, 10, 11, -1, -1, -1, -1, -1, -1},
{3, 1, 10, 11, 3, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 2, 11, 1, 11, 9, 9, 11, 8, -1, -1, -1, -1, -1, -1},
{3, 0, 9, 3, 9, 11, 1, 2, 9, 2, 11, 9, -1, -1, -1},
{0, 2, 11, 8, 0, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{3, 2, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{2, 3, 8, 2, 8, 10, 10, 8, 9, -1, -1, -1, -1, -1, -1},
{9, 10, 2, 0, 9, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{2, 3, 8, 2, 8, 10, 0, 1, 8, 1, 10, 8, -1, -1, -1},
{1, 10, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{1, 3, 8, 9, 1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 9, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{0, 3, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}};


__global__ void classify_voxels_kernel
	(
		float *voxelVertexValues,
		int *triPerVoxel, 
		int *voxelOccupied,
		int numVoxels,
		int3 grid,
		float isoValue
	)
{
	int index = threadIdx.x + blockDim.x*blockIdx.x;
	int offset = 0;

	int caseIndex;
	while(index + offset < numVoxels){
		int3 v = calcFirstVoxelVertexPosition(index+offset, grid);

		int field[8];
		// field[0] = calcVoxelVertexIndex(v + make_int3(0,0,1), grid);  //v0
		// field[1] = calcVoxelVertexIndex(v + make_int3(1,0,1), grid);  //v1
		// field[2] = calcVoxelVertexIndex(v + make_int3(1,0,0), grid);  //v2
		// field[3] = calcVoxelVertexIndex(v, grid);                     //v3
		// field[4] = calcVoxelVertexIndex(v + make_int3(0,1,1), grid);  //v4
		// field[5] = calcVoxelVertexIndex(v + make_int3(1,1,1), grid);  //v5
		// field[6] = calcVoxelVertexIndex(v + make_int3(1,1,0), grid);  //v6
		// field[7] = calcVoxelVertexIndex(v + make_int3(0,1,0), grid);  //v7
		field[0] = calcVoxelVertexIndex(v, grid);  
		field[1] = calcVoxelVertexIndex(v + make_int3(1,0,0), grid);  
		field[2] = calcVoxelVertexIndex(v + make_int3(1,1,0), grid);  
		field[3] = calcVoxelVertexIndex(v + make_int3(0,1,0), grid);  
		field[4] = calcVoxelVertexIndex(v + make_int3(0,0,1), grid);  
		field[5] = calcVoxelVertexIndex(v + make_int3(1,0,1), grid);  
		field[6] = calcVoxelVertexIndex(v + make_int3(1,1,1), grid);  
		field[7] = calcVoxelVertexIndex(v + make_int3(0,1,1), grid);  

		//caseIndex 
		caseIndex =  int(voxelVertexValues[field[0]] < isoValue);
	    caseIndex += int(voxelVertexValues[field[1]] < isoValue)*2;
	    caseIndex += int(voxelVertexValues[field[2]] < isoValue)*4;
	    caseIndex += int(voxelVertexValues[field[3]] < isoValue)*8;
	    caseIndex += int(voxelVertexValues[field[4]] < isoValue)*16;
	    caseIndex += int(voxelVertexValues[field[5]] < isoValue)*32;
	    caseIndex += int(voxelVertexValues[field[6]] < isoValue)*64;
	    caseIndex += int(voxelVertexValues[field[7]] < isoValue)*128;


	    int numberOfTrianglesInVoxel = numTrianglesByCase[caseIndex];

		triPerVoxel[index+offset] = numberOfTrianglesInVoxel;

		if(numberOfTrianglesInVoxel > 0){
			voxelOccupied[index + offset] = 1;
		}
		else{
			voxelOccupied[index + offset] = 0;
		}

		offset += blockDim.x*gridDim.x;
	}
}


__global__ void compact_voxels_kernel
	(
		int *compactVoxels,
		int *voxelOccupied,
		int *voxelOccupiedScan,
		int numVoxels
	)
{
	int index = threadIdx.x + blockDim.x*blockIdx.x;
	int offset = 0;

	while(index + offset < numVoxels){

		if(voxelOccupied[index + offset] == 1){
			compactVoxels[voxelOccupiedScan[index + offset]] = index + offset;
		}
		offset += blockDim.x * gridDim.x;
	}
}

__global__ void generate_triangles_kernel
	(
		float *triangleVertices,
		float *normals,
		float *voxelVertexValues, 
		int *compactVoxels,
		int *triPerVoxelScan,
		int numVoxels,
		int numActiveVoxels,
		int3 grid,
		float3 gridSize,
		float isoValue
	)
{
	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int offset = 0;

	int caseIndex;
	while(index + offset < numActiveVoxels){
		int3 vertexGridPos = calcFirstVoxelVertexPosition(compactVoxels[index+offset], grid);

		float3 h;
		h.x = gridSize.x / grid.x;
		h.y = gridSize.y / grid.y;
		h.z = gridSize.z / grid.z;

		float3 p;
		p.x = vertexGridPos.x * h.x;
		p.y = vertexGridPos.y * h.y;
		p.z = vertexGridPos.z * h.z;

		// find voxel vertices
		float3 v[8];
		// v[0] = p + make_float3(0, 0, h.z);
		// v[1] = p + make_float3(h.x, 0, h.z);
		// v[2] = p + make_float3(h.x, 0, 0);
		// v[3] = p;
		// v[4] = p + make_float3(0, h.y, h.z);
		// v[5] = p + make_float3(h.x, h.y, h.z);
		// v[6] = p + make_float3(h.x, h.y, 0);
		// v[7] = p + make_float3(0, h.y, 0);
		v[0] = p;
		v[1] = p + make_float3(h.x, 0.0f, 0.0f);
		v[2] = p + make_float3(h.x, h.y, 0.0f);
		v[3] = p + make_float3(0.0f, h.y, 0.0f);
		v[4] = p + make_float3(0.0f, 0.0f, h.z);
		v[5] = p + make_float3(h.x, 0.0f, h.z);
		v[6] = p + make_float3(h.x, h.y, h.z);
		v[7] = p + make_float3(0.0f, h.y, h.z);

		// find values at voxel vertices
		float vertexValue[8];
		// vertexValue[0] = voxelVertexValues[calcVoxelVertexIndex(vertexGridPos + make_int3(0,0,1), grid)];
		// vertexValue[1] = voxelVertexValues[calcVoxelVertexIndex(vertexGridPos + make_int3(1,0,1), grid)];
		// vertexValue[2] = voxelVertexValues[calcVoxelVertexIndex(vertexGridPos + make_int3(1,0,0), grid)];
		// vertexValue[3] = voxelVertexValues[calcVoxelVertexIndex(vertexGridPos, grid)];
		// vertexValue[4] = voxelVertexValues[calcVoxelVertexIndex(vertexGridPos + make_int3(0,1,1), grid)];
		// vertexValue[5] = voxelVertexValues[calcVoxelVertexIndex(vertexGridPos + make_int3(1,1,1), grid)];
		// vertexValue[6] = voxelVertexValues[calcVoxelVertexIndex(vertexGridPos + make_int3(1,1,0), grid)];
		// vertexValue[7] = voxelVertexValues[calcVoxelVertexIndex(vertexGridPos + make_int3(0,1,0), grid)];
		vertexValue[0] = voxelVertexValues[calcVoxelVertexIndex(vertexGridPos, grid)];
		vertexValue[1] = voxelVertexValues[calcVoxelVertexIndex(vertexGridPos + make_int3(1,0,0), grid)];
		vertexValue[2] = voxelVertexValues[calcVoxelVertexIndex(vertexGridPos + make_int3(1,1,0), grid)];
		vertexValue[3] = voxelVertexValues[calcVoxelVertexIndex(vertexGridPos + make_int3(0,1,0), grid)];
		vertexValue[4] = voxelVertexValues[calcVoxelVertexIndex(vertexGridPos + make_int3(0,0,1), grid)];
		vertexValue[5] = voxelVertexValues[calcVoxelVertexIndex(vertexGridPos + make_int3(1,0,1), grid)];
		vertexValue[6] = voxelVertexValues[calcVoxelVertexIndex(vertexGridPos + make_int3(1,1,1), grid)];
		vertexValue[7] = voxelVertexValues[calcVoxelVertexIndex(vertexGridPos + make_int3(0,1,1), grid)];

		// calculate case index
		caseIndex =  int(vertexValue[0] < isoValue);
	    caseIndex += int(vertexValue[1] < isoValue)*2;
	    caseIndex += int(vertexValue[2] < isoValue)*4;
	    caseIndex += int(vertexValue[3] < isoValue)*8;
	    caseIndex += int(vertexValue[4] < isoValue)*16;
	    caseIndex += int(vertexValue[5] < isoValue)*32;
	    caseIndex += int(vertexValue[6] < isoValue)*64;
	    caseIndex += int(vertexValue[7] < isoValue)*128;

	    // find intersection vertices along edges of cube
	    float3 vertlist[12];
	    vertlist[0] = vertexInterp(v[0], v[1], vertexValue[0], vertexValue[1], isoValue);
	    vertlist[1] = vertexInterp(v[1], v[2], vertexValue[1], vertexValue[2], isoValue);
	    vertlist[2] = vertexInterp(v[2], v[3], vertexValue[2], vertexValue[3], isoValue);
	    vertlist[3] = vertexInterp(v[3], v[0], vertexValue[3], vertexValue[0], isoValue);

	    vertlist[4] = vertexInterp(v[4], v[5], vertexValue[4], vertexValue[5], isoValue);
	    vertlist[5] = vertexInterp(v[5], v[6], vertexValue[5], vertexValue[6], isoValue);
	    vertlist[6] = vertexInterp(v[6], v[7], vertexValue[6], vertexValue[7], isoValue);
	    vertlist[7] = vertexInterp(v[7], v[4], vertexValue[7], vertexValue[4], isoValue);

	    vertlist[8] = vertexInterp(v[0], v[4], vertexValue[0], vertexValue[4], isoValue);
	    vertlist[9] = vertexInterp(v[1], v[5], vertexValue[1], vertexValue[5], isoValue);
	    vertlist[10] = vertexInterp(v[2], v[6], vertexValue[2], vertexValue[6], isoValue);
	    vertlist[11] = vertexInterp(v[3], v[7], vertexValue[3], vertexValue[7], isoValue);

	    int numberOfTrianglesInVoxel = numTrianglesByCase[caseIndex];
	    for(int i=0;i<numberOfTrianglesInVoxel;i++){
	    	int j = triPerVoxelScan[compactVoxels[index+offset]] + i;
	    	int edge;
	    	float3 v0, v1, v2, n;
	    	edge = triangleTable[caseIndex][3*i];
	    	v0 = vertlist[edge];

	    	edge = triangleTable[caseIndex][3*i+1];
	    	v1 = vertlist[edge];

	    	edge = triangleTable[caseIndex][3*i+2];
	    	v2 = vertlist[edge];

	    	triangleVertices[9*j] = v0.x;
	    	triangleVertices[9*j+1] = v0.y;
	    	triangleVertices[9*j+2] = v0.z;

	    	triangleVertices[9*j+3] = v1.x;
	    	triangleVertices[9*j+4] = v1.y;
	    	triangleVertices[9*j+5] = v1.z;

	    	triangleVertices[9*j+6] = v2.x;
	    	triangleVertices[9*j+7] = v2.y;
	    	triangleVertices[9*j+8] = v2.z;

	    	n = calcNormal(v0, v1, v2);
	    	normals[9*j] = n.x;
	    	normals[9*j+1] = n.y;
	    	normals[9*j+2] = n.z;

	    	normals[9*j+3] = n.x;
	    	normals[9*j+4] = n.y;
	    	normals[9*j+5] = n.z;

	    	normals[9*j+6] = n.x;
	    	normals[9*j+7] = n.y;
	    	normals[9*j+8] = n.z;
	    }

		offset += blockDim.x*gridDim.x;
	}
}




// voxelize triangular mesh
__global__ void voxelize_kernel
	(
		float* triangleVertices, 
		int *voxelTable, 
		int numVertices,
		int numVoxels,
		float h,
		int3 grid
	)
{
	int index = threadIdx.x + blockDim.x * blockIdx.x;
	int offset = 0;

	float3 dp = make_float3(h, h, h);
	float3 criticalPoint = make_float3(0.0f, 0.0f, 0.0f);

	int numTriangles = numVertices / 9;
	while(index + offset < numTriangles){
		int i = index * 9;

		// triangle vertices
		float3 v0 = make_float3(triangleVertices[i], triangleVertices[i+1], triangleVertices[i+2]);
		float3 v1 = make_float3(triangleVertices[i+3], triangleVertices[i+4], triangleVertices[i+5]);
		float3 v2 = make_float3(triangleVertices[i+6], triangleVertices[i+7], triangleVertices[i+8]);

		// triangle edges
		float3 e0 = v1 - v0;
		float3 e1 = v2 - v1;
		float3 e2 = v0 - v2;

		// triangle normal vector normalized
		float3 n = normalize(cross(e0, e1));

		// find AABB of triangle
		float3 min = fminf(v0, fminf(v1, v2));
		float3 max = fmaxf(v0, fmaxf(v1, v2));
		int3 min_grid;
		int3 max_grid;
		min_grid.x = __float2int_rd(min.x / h);
		min_grid.y = __float2int_rd(min.y / h);
		min_grid.z = __float2int_rd(min.z / h);
		max_grid.x = __float2int_rd(max.x / h);
		max_grid.y = __float2int_rd(max.y / h);
		max_grid.z = __float2int_rd(max.z / h);
		//int3 min_grid = clamp(min / h, make_float3(0.0f, 0.0f, 0.0f), make_float3(grid.x-1, grid.y-1, grid.z-1));
		//int3 max_grid = clamp(max / h, make_float3(0.0f, 0.0f, 0.0f), make_float3(grid.x-1, grid.y-1, grid.z-1));


		if (n.x > 0.0f) { criticalPoint.x = h; }
		if (n.y > 0.0f) { criticalPoint.y = h; }
		if (n.z > 0.0f) { criticalPoint.z = h; }
		float d1 = dot(n, (criticalPoint - v0));
		float d2 = dot(n, ((dp - criticalPoint) - v0));

		// xy plane
		float2 n_xy_e0 = make_float2(-1.0f*e0.y, e0.x);
		float2 n_xy_e1 = make_float2(-1.0f*e1.y, e1.x);
		float2 n_xy_e2 = make_float2(-1.0f*e2.y, e2.x);
		if (n.z < 0.0f) {
			n_xy_e0 = -n_xy_e0;
			n_xy_e1 = -n_xy_e1;
			n_xy_e2 = -n_xy_e2;
		}
		float d_xy_e0 = (-1.0f * dot(n_xy_e0, make_float2(v0.x, v0.y))) + fmaxf(0.0f, h*n_xy_e0.x) + fmaxf(0.0f, h*n_xy_e0.y);
		float d_xy_e1 = (-1.0f * dot(n_xy_e1, make_float2(v1.x, v1.y))) + fmaxf(0.0f, h*n_xy_e1.x) + fmaxf(0.0f, h*n_xy_e1.y);
		float d_xy_e2 = (-1.0f * dot(n_xy_e2, make_float2(v2.x, v2.y))) + fmaxf(0.0f, h*n_xy_e2.x) + fmaxf(0.0f, h*n_xy_e2.y);

		// yz plane
		float2 n_yz_e0 = make_float2(-1.0f*e0.z, e0.y);
		float2 n_yz_e1 = make_float2(-1.0f*e1.z, e1.y);
		float2 n_yz_e2 = make_float2(-1.0f*e2.z, e2.y);
		if (n.x < 0.0f) {
			n_yz_e0 = -n_yz_e0;
			n_yz_e1 = -n_yz_e1;
			n_yz_e2 = -n_yz_e2;
		}
		float d_yz_e0 = (-1.0f * dot(n_yz_e0, make_float2(v0.y, v0.z))) + fmaxf(0.0f, h*n_yz_e0.x) + fmaxf(0.0f, h*n_yz_e0.y);
		float d_yz_e1 = (-1.0f * dot(n_yz_e1, make_float2(v1.y, v1.z))) + fmaxf(0.0f, h*n_yz_e1.x) + fmaxf(0.0f, h*n_yz_e1.y);
		float d_yz_e2 = (-1.0f * dot(n_yz_e2, make_float2(v2.y, v2.z))) + fmaxf(0.0f, h*n_yz_e2.x) + fmaxf(0.0f, h*n_yz_e2.y);
		
		// zx plane
		float2 n_zx_e0 = make_float2(-1.0f*e0.x, e0.z);
		float2 n_zx_e1 = make_float2(-1.0f*e1.x, e1.z);
		float2 n_zx_e2 = make_float2(-1.0f*e2.x, e2.z);
		if (n.y < 0.0f) {
			n_zx_e0 = -n_zx_e0;
			n_zx_e1 = -n_zx_e1;
			n_zx_e2 = -n_zx_e2;
		}
		float d_xz_e0 = (-1.0f * dot(n_zx_e0, make_float2(v0.z, v0.x))) + fmaxf(0.0f, h*n_zx_e0.x) + fmaxf(0.0f, h*n_zx_e0.y);
		float d_xz_e1 = (-1.0f * dot(n_zx_e1, make_float2(v1.z, v1.x))) + fmaxf(0.0f, h*n_zx_e1.x) + fmaxf(0.0f, h*n_zx_e1.y);
		float d_xz_e2 = (-1.0f * dot(n_zx_e2, make_float2(v2.z, v2.x))) + fmaxf(0.0f, h*n_zx_e2.x) + fmaxf(0.0f, h*n_zx_e2.y);

		// test possible grid boxes for overlap
		for (int z = min_grid.z; z <= max_grid.z; z++){
			for (int y = min_grid.y; y <= max_grid.y; y++){
				for (int x = min_grid.x; x <= max_grid.x; x++){
					
					// triangle plane through box test
					float3 p = make_float3(x*h, y*h, z*h);
					float nDOTp = dot(n, p);
					if((nDOTp + d1) * (nDOTp + d2) > 0.0f){ continue; }

					// projection tests
					// xy
					float2 p_xy = make_float2(p.x, p.y);
					if ((dot(n_xy_e0, p_xy) + d_xy_e0) < 0.0f){ continue; }
					if ((dot(n_xy_e1, p_xy) + d_xy_e1) < 0.0f){ continue; }
					if ((dot(n_xy_e2, p_xy) + d_xy_e2) < 0.0f){ continue; }

					// yz
					float2 p_yz = make_float2(p.y, p.z);
					if ((dot(n_yz_e0, p_yz) + d_yz_e0) < 0.0f){ continue; }
					if ((dot(n_yz_e1, p_yz) + d_yz_e1) < 0.0f){ continue; }
					if ((dot(n_yz_e2, p_yz) + d_yz_e2) < 0.0f){ continue; }

					// xz	
					float2 p_zx = make_float2(p.z, p.x);
					if ((dot(n_zx_e0, p_zx) + d_xz_e0) < 0.0f){ continue; }
					if ((dot(n_zx_e1, p_zx) + d_xz_e1) < 0.0f){ continue; }
					if ((dot(n_zx_e2, p_zx) + d_xz_e2) < 0.0f){ continue; }

					int location = x + y*grid.x + z*grid.x*grid.y;
					// setBit(voxel_table, location);
					voxelTable[location] = 1;
					continue;
				}
			}
		}

		offset += blockDim.x * gridDim.x;
	}
}




__global__ void	calculate_collisions_kernel
	(
		float4 *pos,  
		int *cellStartIndex,
		int *cellEndIndex,
		int *cellIndex,
		int *particleIndex,
		int numParticles,
		int3 grid
	)
{
	
}






























































// __global__ void calculate_particle_forces_octtree_kernel
// 	(
// 		float4 *pos,
// 		float4 *vel,
// 		float4 *acc, 
// 		float *rho,
// 		float *pres,  
// 		int *sorted, 
// 		int *child, 
// 		float4 *minPos,
// 		float4 *maxPos, 
// 		int numParticles,
// 		float h,
// 		float h6
// 	)
// {
	// int index = threadIdx.x + blockIdx.x*blockDim.x;
	// int offset = 0;

	// // __shared__ float depth[stackSize*blockSize/warp]; 
	// __shared__ float3 min[stackSize*blockSize/warp];
	// __shared__ float3 max[stackSize*blockSize/warp];
	// __shared__ int stack[stackSize*blockSize/warp];  // stack controlled by one thread per warp 

	// float3 init_min = make_float3(minPos->x, minPos->y, minPos->z);
	// float3 init_max = make_float3(maxPos->x, maxPos->y, maxPos->z);

	// int counter = threadIdx.x % warp;
	// int stackStartIndex = stackSize*(threadIdx.x / warp);
	// while(index + offset < numParticles){
	// 	int sortedIndex = sorted[index + offset];

	// 	float4 position = pos[sortedIndex];
	// 	float density = 0.0f;  

	// 	int jj = -1;
	// 	for(int i=0;i<8;i++){
	// 		if(child[i] != -1 && InRangeOfQuadrant(position, init_min, init_max, h2, i)){     
	// 			jj++;       
	// 		}               
	// 	}

	// 	// initialize stack
	// 	int top = jj + stackStartIndex;
	// 	if(counter == 0){
	// 		int temp = 0;
	// 		for(int i=0;i<8;i++){
	// 			if(child[i] != -1 && InRangeOfQuadrant(position, init_min, init_max, h2, i)){
	// 				// printf("r: %d\n", i);
	// 				stack[stackStartIndex + temp] = child[i];
	// 				min[stackStartIndex + temp] = init_min;
	// 				max[stackStartIndex + temp] = init_max;
	// 				//depth[stackStartIndex + temp] = radius;
	// 				temp++;
	// 			}
	// 		}
	// 	}

	// 	__syncthreads();

	// 	// while stack is not empty
	// 	while(top >= stackStartIndex){
	// 		int node = stack[top];
	// 		float3 dmin = min[top];
	// 		float3 dmax = max[top];
	// 		// float dp = depth[top];
	// 		for(int i=0;i<8;i++){
	// 			int ch = child[8*node + i];
			
	// 			if(ch != -1 && InRangeOfQuadrant(position, dmin, dmax, h2, i)){
	// 				if(ch < numParticles /* leaf node */){
	// 					float rx = position.x - pos[ch].x;
	// 					float ry = position.y - pos[ch].y;
	// 					float rz = position.z - pos[ch].z;
	// 					float radius2 = rx*rx + ry*ry + rz*rz;
	// 					// printf("radius2: %f\n", radius2);
	// 					if(__all(radius2 <= h2)){
	// 						density += (h2 - radius2) * (h2 - radius2) * (h2 - radius2);
	// 					}
	// 				}
	// 				else{ /*not a leaf node */
	// 					if(counter == 0){
	// 						stack[top] = ch;
							
	// 						dmin.x = 0.5f*(dmax.x + dmin.x);
	// 						dmin.y = 0.5f*(dmax.y + dmin.y);
	// 						dmax.z = 0.5f*(dmax.z + dmin.z);
	// 						if(i & (1 << 1)){ dmax.x = 0.5f*(dmax.x + dmin.x); }
	// 						if(i & (1 << 2)){ dmax.y = 0.5f*(dmax.y + dmin.y); }
	// 						if(i & (1 << 3)){ dmin.z = 0.5f*(dmax.z + dmin.z); }

	// 						min[top] = dmin;
	// 						max[top] = dmax;
	// 						//depth[top] = 0.5f*dp;
	// 					}
	// 					top++;
	// 				}
	// 			}
	// 		}

	// 		top--;
	// 	}

 // 		density = 0.01f*(f1/h9) * density;

	// 	rho[sortedIndex] = density;
	// 	pres[sortedIndex] = kappa*(density - rho0[sortedIndex]) / (density * density);
	// 	offset += blockDim.x * gridDim.x;    

 //   		__syncthreads();
 //   	}

// }