#ifndef __PARTICLES_H__
#define __PARTICLES_H__

#include <string>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#define GLM_FORCE_RADIANS

#include "../../glm/glm/glm.hpp"
#include "../../glm/glm/gtc/matrix_transform.hpp"
#include "../../glm/glm/gtc/type_ptr.hpp"

#include "RenderData.h"
#include "Material.h"
#include "ObjParser.h"

#include "vector_types.h"

class Particles
{
	private:
		ObjParser objParser;
		RenderData renderData;

		int numParticles;
		std::vector<float> vertices;

	public:
		glm::mat4 model;

		Material material;

	public:
		Particles(std::string objFilePath);
		~Particles();

		void start();
		void processEvents(sf::Event event);
		void update(std::vector<float> vertices);
		void update(std::vector<float> vertices, std::vector<float> normals);

		int getNumParticles();
		std::vector<float> getVertices();

		RenderData& getRenderData();

};

#endif