#ifndef __SCENEENTITY_H__
#define __SCENEENTITY_H__
#include <map>
#include <string>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GL/gl.h>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#include "Shader.h"

#define GLM_FORCE_RADIANS

#include "external/glm/glm/glm.hpp"
#include "external/glm/glm/gtc/matrix_transform.hpp"
#include "external/glm/glm/gtc/type_ptr.hpp"

typedef enum EntityType
{
	FLUID,
	RIGIDBODY

}EntityType;


class SceneEntity
{
	protected:
		int numVertices;
		float* vertices;
		float* normals;
		GLuint vao;
		GLuint vbo[2];

		GLint position;
		GLint normal;
		GLint modelLoc;
		GLint viewLoc;
		GLint projLoc;
		GLint entityColorLoc;
		GLint lightPosLoc;
		GLint lightColorLoc;
		GLint cameraPosLoc;

		GLenum drawMode;
		GLenum drawUsage;
		Shader shaders[10];

		bool isReady;
		bool isActive;
		bool physics;

		// copies from scene
		glm::mat4 view;
		glm::mat4 projection;
		glm::vec3 lightPos;
		glm::vec3 lightColor;
		glm::vec3 cameraPos;

	public:
		glm::vec3 entityColor;
		glm::mat4 model;

	public:
		SceneEntity();
		virtual ~SceneEntity() = 0;

		virtual void processEvents(sf::Event event) = 0;
		virtual void start() = 0;
		virtual void update() = 0;
		virtual void render() = 0;

		bool hasPhysics();
		void setActive(bool value);
		void copyViewToEntity(glm::mat4 viewMatrix);
		void copyProjectionToEntity(glm::mat4 projMatrix);
		void copyLightToEntity(glm::vec3 position, glm::vec3 color);
		void copyCameraPositionToEntity(glm::vec3 position);
};


#endif