#include "SPHParticles.cuh"
#include "kernels.cuh"
#include "util_kernels.cuh"
#include "cuda_util.cuh"

#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/copy.h>

#include <stdio.h>

#include <cuda.h>

#include <random>

SPHParticles::SPHParticles()
{

}


SPHParticles::SPHParticles(int numParticles, int3 particleGridDim, float3 particleGridSize)
{
	this->numParticles = numParticles;
	this->particleGridDim = particleGridDim;
	this->particleGridSize = particleGridSize;

	dt = 0.0075f;
	// h = particleGridSize.x / particleGridDim.x;
	numCells = particleGridDim.x * particleGridDim.y * particleGridDim.z;

	// allocate memory 
	allocateMemory();
}

SPHParticles::SPHParticles(const SPHParticles &spf)
{
	// allocate memory 
	allocateMemory();
}

SPHParticles& SPHParticles::operator=(const SPHParticles &spf)
{
	if(this != &spf){
		// free old memory 
		deallocateMemory();

		// allocate memory 
		allocateMemory();
	}

	return *this;
}


SPHParticles::~SPHParticles()
{
	// free old memory 
	deallocateMemory();
}

void SPHParticles::allocateMemory()
{
	// allocate memory on host
	h_pos = new float4[numParticles];
	h_vel = new float4[numParticles];
	h_acc = new float4[numParticles];
	h_spos = new float4[numParticles];
	h_svel = new float4[numParticles];
	h_cellStartIndex = new int[numCells];
	h_cellEndIndex = new int[numCells];	
	h_cellIndex = new int[numParticles];
	h_particleIndex = new int[numParticles];

	// allocate memory on device
	gpuErrchk(cudaMalloc((void**)&d_pos, numParticles*sizeof(float4)));
	gpuErrchk(cudaMalloc((void**)&d_vel, numParticles*sizeof(float4)));
	gpuErrchk(cudaMalloc((void**)&d_acc, numParticles*sizeof(float4)));
	gpuErrchk(cudaMalloc((void**)&d_spos, numParticles*sizeof(float4)));
	gpuErrchk(cudaMalloc((void**)&d_svel, numParticles*sizeof(float4)));
	gpuErrchk(cudaMalloc((void**)&d_cellStartIndex, numCells*sizeof(int)));
	gpuErrchk(cudaMalloc((void**)&d_cellEndIndex, numCells*sizeof(int)));
	gpuErrchk(cudaMalloc((void**)&d_cellHash, numParticles*sizeof(int)));
	gpuErrchk(cudaMalloc((void**)&d_particleIndex, numParticles*sizeof(int)));
}


void SPHParticles::deallocateMemory()
{
	// free memory on host
	delete [] h_pos;
	delete [] h_vel;
	delete [] h_acc;
	delete [] h_spos;
	delete [] h_svel;
	delete [] h_cellStartIndex;
	delete [] h_cellEndIndex;
	delete [] h_cellIndex;
	delete [] h_particleIndex;

	// free memory on device
	gpuErrchk(cudaFree(d_pos));
	gpuErrchk(cudaFree(d_vel));
	gpuErrchk(cudaFree(d_acc));	
	gpuErrchk(cudaFree(d_spos));
	gpuErrchk(cudaFree(d_svel));
	gpuErrchk(cudaFree(d_cellStartIndex));
	gpuErrchk(cudaFree(d_cellEndIndex));
	gpuErrchk(cudaFree(d_cellHash));
	gpuErrchk(cudaFree(d_particleIndex));
}

void SPHParticles::initialCondition()
{
	std::default_random_engine generator;
	std::uniform_real_distribution<float> distributionx(0.0f, particleGridSize.x);
	std::uniform_real_distribution<float> distributiony(0.0f, particleGridSize.y);
	std::uniform_real_distribution<float> distributionz(0.0f, particleGridSize.z);

	// loop through all particles
	for (int i = 0; i < numParticles; i++){
		h_pos[i].x = 1.0f*distributionx(generator);
		h_pos[i].y = 0.25f*distributiony(generator);
		h_pos[i].z = 0.5f*distributionz(generator);
		h_pos[i].w = 0.0f;

		h_vel[i].x = 0.0f;
		h_vel[i].y = 0.0f;
		h_vel[i].z = 0.0f;
		h_vel[i].w = 0.0f;
	}

	// set initial position in host output array
	for(int i=0;i<numParticles;i++){
		h_output[3*i] = h_pos[i].x;
		h_output[3*i+1] = h_pos[i].y;
		h_output[3*i+2] = h_pos[i].z;
	}

	gpuErrchk(cudaMemcpy(d_pos, h_pos, numParticles*sizeof(float4), cudaMemcpyHostToDevice));
	gpuErrchk(cudaMemcpy(d_vel, h_vel, numParticles*sizeof(float4), cudaMemcpyHostToDevice));
}


void SPHParticles::setInitialCondition(float3 *pos)
{
	for(int i = 0; i < numParticles; i++){
		h_pos[i].x = pos[i].x;
		h_pos[i].y = pos[i].y;
		h_pos[i].z = pos[i].z;
		h_pos[i].w = 0.0f;

		h_vel[i].x = 0.0f;
		h_vel[i].y = 0.0f;
		h_vel[i].z = 0.0f;
		h_vel[i].w = 0.0f;
	}

	// set initial position in host output array
	for(int i = 0; i < numParticles; i++){
		h_output[3*i] = h_pos[i].x;
		h_output[3*i+1] = h_pos[i].y;
		h_output[3*i+2] = h_pos[i].z;
	}

	gpuErrchk(cudaMemcpy(d_pos, h_pos, numParticles*sizeof(float4), cudaMemcpyHostToDevice));
	gpuErrchk(cudaMemcpy(d_vel, h_vel, numParticles*sizeof(float4), cudaMemcpyHostToDevice));
}

void SPHParticles::update()
{
	elapsedTime = 0;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	cudaEventRecord(start,0);

	UniformFiniteGridAlgorithm();

	cudaEventRecord(stop,0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&elapsedTime, start, stop);
}

void SPHParticles::UniformFiniteGridAlgorithm()
{
	dim3 gridSize(256,1,1);
	dim3 blockSize(256,1,1);
	build_spatial_grid_kernel<<< gridSize, blockSize >>>
		(
			d_pos, 
			d_particleIndex, 
			d_cellHash, 
			numParticles, 
			particleGridDim,
			particleGridSize
		);

	thrust::device_ptr<int> t_a(d_cellHash);
	thrust::device_ptr<int> s_a(d_particleIndex);
	thrust::sort_by_key(t_a, t_a + numParticles, s_a);

	reorder_particles_kernel<<< gridSize, blockSize >>>
		(
			d_pos,
			d_spos,
			d_vel,
			d_svel,
			d_cellStartIndex,
			d_cellEndIndex,
			d_cellHash,
			d_particleIndex,
			numParticles
		);

	// calculate_collisions_kernel<<< gridSize, blockSize >>>
	// 	(
	// 		d_spos,  
	// 		d_cellStartIndex,
	// 		d_cellEndIndex,
	// 		d_cellHash,
	// 		d_particleIndex,
	// 		numParticles,
	// 		particleGridDim
	// 	);

	// update_particles_kernel<<< gridSize, blockSize >>>
	// 	(
	// 		d_spos,
	// 		d_svel,
	// 		d_acc,
	// 		dt,
	// 		h,
	// 		numParticles,
	// 		particleGridSize
	// 	);

	copy_sph_arrays_kernel<<< gridSize, blockSize >>>
		(
			d_pos,
			d_spos,
			d_vel,
			d_svel,
			d_output,
			numParticles
		);

	set_array_to_value<int><<< gridSize, blockSize >>>(d_cellStartIndex, -1, numCells);
	set_array_to_value<int><<< gridSize, blockSize >>>(d_cellEndIndex, -1, numCells);

	gpuErrchk(cudaMemcpy(h_output, d_output, 3*numParticles*sizeof(float), cudaMemcpyDeviceToHost));
	gpuErrchk(cudaMemcpy(h_pos, d_pos, numParticles*sizeof(float4), cudaMemcpyDeviceToHost));
}