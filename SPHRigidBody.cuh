#ifndef __SPHRIGIDBODY_H__
#define __SPHRIGIDBODY_H__


class SPHRigidBody
{
	private:
		int numParticles;
		float3 particleGridSize;

		float* vertices;
		int *voxelTable;

	public:
		SPHRigidBody();
		~SPHRigidBody();

		void voxelize(float *vertices, int numVertices);

};


#endif