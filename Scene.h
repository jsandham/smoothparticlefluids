#ifndef __SCENE_H__
#define __SCENE_H__

#include "debug.h"

#include <map>
#include <string>
#include <ctime>

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

#define GLM_FORCE_RADIANS

#include "external/glm/glm/glm.hpp"
#include "external/glm/glm/gtc/matrix_transform.hpp"
#include "external/glm/glm/gtx/transform.hpp"
#include "external/glm/glm/gtc/type_ptr.hpp"

#include "Renderer.h"
#include "PhysicsEngine.h"
#include "Grid.h"
#include "Camera.h"
#include "Light.h"
#include "Particles.h"
#include "Fluid.h"
#include "RigidBody.h"


class Scene
{
	private:
		sf::ContextSettings *settings;
		sf::RenderWindow *window;
		Camera *camera;
		Grid *grid;
		PhysicsEngine *physicsEngine;
		Renderer *renderer;

#if DEBUG
		sf::RenderWindow *debugWindow;
		Camera *debugCamera;
#endif

		sf::Font font;
		sf::Text timeText;
		sf::Text versionText;

		std::map<std::string, DirectionalLight*> directionalLights;
		std::map<std::string, PointLight*> pointLights;
		std::map<std::string, SpotLight*> spotLights;
		std::map<std::string, AreaLight*> areaLights;
		std::map<std::string, Particles*> particles;
		std::map<std::string, Fluid*> fluids;
		std::map<std::string, RigidBody*> rigidBodies;

		std::string sceneName;

		glm::vec3 cameraPos;
		glm::vec3 cameraFront;
		glm::vec3 cameraUp;

		clock_t startTime;
		clock_t physicsEndTime;
		clock_t renderStartTime;
		clock_t endTime;

		int openglFrame;
		int totalElapsedFrames;
		float totalElapsedTime;
		float timePerFrame;
		float physicsTimePerFrame;
		float renderTimePerFrame;
		float fps;

	public:
		glm::vec3 backgroundColor;

	public:
		Scene();
		Scene(std::string name);
		Scene(const Scene &scene);
		Scene& operator=(const Scene &scene);
		~Scene();

		void run();
		void loadScene(std::string filename);
		void add(std::string name, DirectionalLight *light);
		void add(std::string name, PointLight *light);
		void add(std::string name, SpotLight *light);
		void add(std::string name, AreaLight *light);
		void add(std::string name, Particles *particle);
		void add(std::string name, Fluid* fluid);
		void add(std::string name, RigidBody* rigidBody);
		void remove(std::string name);
		bool contains(std::string name);
		int size();

		void setTimeText(std::string text);
		void setVersionText(std::string text);
		void setSceneName(std::string name);
		void setCamera(glm::vec3 position, glm::vec3 front, glm::vec3 up);

	private:
		void allocateWindow();
		void deallocateWindow();
		void setDefaultConfig();

		void start();
		void updatePhysics();
		void processEvents();
		void updateScene();
		void render();
		void updateTimer();

		bool isWindowOpen();
};

#endif