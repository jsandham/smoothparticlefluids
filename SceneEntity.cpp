#include "SceneEntity.h"


SceneEntity::SceneEntity()
{
	isReady = false;
	isActive = true;

	entityColor = glm::vec3(1.0f, 1.0f, 1.0f);
	model = glm::mat4(1.0f);
	view = glm::mat4(1.0f);
	projection = glm::mat4(1.0f);	
}

SceneEntity::~SceneEntity()
{

}

bool SceneEntity::hasPhysics()
{
	return physics;
}


void SceneEntity::setActive(bool value)
{
	isActive = value;
}


void SceneEntity::copyViewToEntity(glm::mat4 viewMatrix)
{
	view = viewMatrix;
}


void SceneEntity::copyProjectionToEntity(glm::mat4 projMatrix)
{
	projection = projMatrix;
}


void SceneEntity::copyLightToEntity(glm::vec3 position, glm::vec3 color)
{
	lightPos = position;
	lightColor = color;
}


void SceneEntity::copyCameraPositionToEntity(glm::vec3 position)
{
	cameraPos = position;
}