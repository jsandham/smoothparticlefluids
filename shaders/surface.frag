#version 130

in vec3 FragPos;
in vec3 Normal;

uniform vec3 lightPos;
uniform vec3 lightColor;
uniform vec3 objectColor;
uniform vec3 cameraPos;

void main(void) {
	vec3 lightDir = normalize(lightPos - FragPos);

	float ambientStrength = 0.1f;
	float diffuseStrength = max(dot(Normal, lightDir), 0.0);
	float specularStrength = 1.0f;

	vec3 viewDir = normalize(cameraPos - FragPos);
	vec3 reflectDir = reflect(-lightDir, Normal);

	float spec = pow(max(dot(viewDir, reflectDir), 0.0f), 32);

	vec3 ambient = ambientStrength * lightColor;
	vec3 diffuse = diffuseStrength * lightColor;
	vec3 specular = specularStrength * spec * lightColor;

	float attenuation = 1.0f / (1.0f + 0.01f*pow(length(lightPos - FragPos), 2));

	vec3 result = ambient + attenuation * (diffuse + specular);

	result = result * objectColor;

	gl_FragColor = vec4(result, 1.0f);
}