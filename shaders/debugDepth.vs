#version 130

in vec3 position;
in vec2 atexture;

out vec2 TexCoord;

void main()
{
	gl_Position = vec4(position, 1.0);
	TexCoord = atexture;
}