#version 130

#define NUM_OF_LIGHTS 2

struct Material
{
	float shininess;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct DirLight
{
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};
 
out vec4 FragColor;

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoord;
in vec4 FragPosLightSpace;

uniform Material material;
uniform DirLight dirLight;

uniform vec3 cameraPos;

uniform sampler2D texture1;
uniform sampler2D shadowMap;

vec2 poissonDisk[4] = vec2[](
  vec2( -0.94201624, -0.39906216 ),
  vec2( 0.94558609, -0.76890725 ),
  vec2( -0.094184101, -0.92938870 ),
  vec2( 0.34495938, 0.29387760 )
);

float CalcShadow(vec4 FragPosLightSpace);
vec3 CalcDirLight(Material material, DirLight light, vec3 normal, vec3 viewDir);

void main(void) 
{
	vec3 viewDir = normalize(cameraPos - FragPos);

	vec3 finalLight = CalcDirLight(material, dirLight, Normal, viewDir);

	FragColor = texture(texture1, TexCoord) * vec4(finalLight, 1.0f);
}


float CalcShadow(vec4 fragPosLightSpace)
{
	// only actually needed when using perspective projection for the light
	vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;

  	// projCoord is in [-1,1] range. Convert it ot [0,1] range.
	projCoords = projCoords * 0.5f + 0.5f;

	float bias = 0.005f;
	float shadow = 0.0f;
	for(int i=0;i<4;i++){
		if(texture(shadowMap, projCoords.xy + poissonDisk[i]/700.0).r < projCoords.z - bias){
			shadow += 0.2f;
		}
	}

	return shadow;
}


vec3 CalcDirLight(Material material, DirLight light, vec3 normal, vec3 viewDir)
{
	vec3 lightDir = normalize(-light.direction);

	vec3 reflectDir = reflect(-lightDir, normal);
    
    float ambientStrength = 1.0f;
    float diffuseStrength = max(dot(normal, lightDir), 0.0);
    float specularStrength = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);

    float shadow = CalcShadow(FragPosLightSpace);

	vec3 ambient = light.ambient * material.ambient * ambientStrength;
	vec3 diffuse = (1.0f - shadow) * light.diffuse * material.diffuse * diffuseStrength;
	vec3 specular = (1.0f - shadow) * light.specular * material.specular * specularStrength;

    return (ambient + diffuse + specular);
}