#version 130

struct Material
{
    float shininess;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct DirLight
{
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

in vec2 TexCoord;

out vec4 FragColor;

uniform Material material;
uniform DirLight dirLight;
uniform sampler2D texture1;

void main(void) {
    vec3 Normal;
    // Normal = texture(texture1, TexCoord);
    Normal.xy = gl_PointCoord * vec2(2.0, -2.0) + vec2(-1.0, 1.0);
    float mag = dot(Normal.xy, Normal.xy);

    if (mag > 1.0f) discard;

    Normal.z = sqrt(1.0-mag);

    vec3 lightDir = normalize(-dirLight.direction);

    float diffuseStrength = max(dot(Normal, lightDir), 0.0);

    vec3 diffuse = dirLight.diffuse * material.diffuse * diffuseStrength;

    FragColor = texture(texture1, TexCoord.xy) * vec4(diffuse, 1.0f);
}

