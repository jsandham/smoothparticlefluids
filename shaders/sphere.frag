#version 130

in vec3 FragPos;
in vec3 Normal;

uniform vec3 lightPos;
uniform vec3 lightColor;
uniform vec3 objectColor;

void main(void) {
	vec3 lightDir = normalize(lightPos - FragPos);

	float ambientStrength = 0.1f;
	float diffuseStrength = max(dot(Normal, lightDir), 0.0);

	vec3 ambient = ambientStrength * lightColor;
	vec3 diffuse = diffuseStrength * lightColor;

	vec3 result = (ambient + diffuse) * objectColor;

	gl_FragColor = vec4(result, 1.0f);
}