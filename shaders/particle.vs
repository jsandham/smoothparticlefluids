#version 130 

in vec3 position;
in vec2 atexture;

out vec2 TexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform float pointRadius;
uniform float pointScale; 

void main()
{
    gl_Position = projection * view * model * vec4(position.x, position.y, position.z, 1.0f);

    gl_PointSize = pointRadius / pointScale;

    TexCoord = atexture;
}