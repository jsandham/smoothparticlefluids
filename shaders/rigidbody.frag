#version 130

#define NUM_OF_LIGHTS 2

struct Material
{
	float shininess;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct DirLight
{
	vec3 direction;

	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct PointLight
{
	vec3 position;

	float constant;
    float linear;
    float quadratic;
	
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct SpotLight {
    vec3 position;
    vec3 direction;
    float cutOff;
    float outerCutOff;
  
    float constant;
    float linear;
    float quadratic;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;       
};

struct AreaLight
{
	vec3 position;
	vec3 ambient;
    vec3 diffuse;
    vec3 specular;   
};
 
out vec4 FragColor;

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoord;
in vec4 FragPosLightSpace;

uniform Material material;
uniform DirLight dirLight;
uniform PointLight pointLight;
uniform SpotLight spotLight;
uniform AreaLight areaLight;

uniform vec3 cameraPos;

uniform sampler2D texture1;
uniform sampler2D shadowMap;

float CalcShadow(vec4 FragPosLightSpace);
vec3 CalcDirLight(Material material, DirLight light, vec3 normal, vec3 viewDir);
vec3 CalcPointLight(Material material, PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);
vec3 CalcSpotLight(Material material, SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir);
vec3 CalcAreaLight(Material material, AreaLight light);

void main(void) 
{
	vec3 viewDir = normalize(cameraPos - FragPos);

	vec3 finalLight = vec3(0.0f, 0.0f, 0.0f);

	finalLight += CalcDirLight(material, dirLight, Normal, viewDir);

	finalLight += CalcPointLight(material, pointLight, Normal, FragPos, viewDir);

	finalLight += CalcSpotLight(material, spotLight, Normal, FragPos, viewDir);

	finalLight += CalcAreaLight(material, areaLight);

	FragColor = texture2D(texture1, TexCoord) * vec4(finalLight, 1.0f);
}


float CalcShadow(vec4 fragPosLightSpace)
{
	// only actually needed when using perspective projection for the light
	vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;

  	// projCoord is in [-1,1] range. Convert it ot [0,1] range.
	projCoords = projCoords * 0.5f + 0.5f;

	float closestDepth = texture(shadowMap, projCoords.xy).r;

	float currentDepth = projCoords.z;

	float bias = 0.005f;
	float shadow = currentDepth - bias > closestDepth  ? 1.0 : 0.0;

	return shadow;

}


vec3 CalcDirLight(Material material, DirLight light, vec3 normal, vec3 viewDir)
{
	vec3 lightDir = normalize(-light.direction);

	vec3 reflectDir = reflect(-lightDir, normal);
    
    float ambientStrength = 1.0f;
    float diffuseStrength = max(dot(normal, lightDir), 0.0);
    float specularStrength = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);

    float shadow = CalcShadow(FragPosLightSpace);

	vec3 ambient = light.ambient * material.ambient * ambientStrength;
	vec3 diffuse = (1.0f - shadow) * light.diffuse * material.diffuse * diffuseStrength;
	vec3 specular = (1.0f - shadow) * light.specular * material.specular * specularStrength;

    return (ambient + diffuse + specular);
}


vec3 CalcPointLight(Material material, PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	vec3 lightDir = normalize(light.position - fragPos);

	vec3 reflectDir = reflect(-lightDir, normal);

	float ambientStrength = 1.0f;
	float diffuseStrength = max(dot(normal, lightDir), 0.0);
	float specularStrength = pow(max(dot(viewDir, reflectDir), 0.0f), material.shininess);

	float attenuation = 1.0f / (1.0f + 0.01f*pow(length(light.position - fragPos), 2));

	vec3 ambient = light.ambient * material.ambient * ambientStrength;
	vec3 diffuse = light.diffuse * material.diffuse * diffuseStrength;
	vec3 specular = light.specular * material.specular * specularStrength;

	ambient *= attenuation;
	diffuse *= attenuation;
	specular *= attenuation;

	return vec3(ambient + diffuse + specular);
}


vec3 CalcSpotLight(Material material, SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
	vec3 lightDir = normalize(light.position - fragPos);

	vec3 reflectDir = reflect(-lightDir, normal);

	float ambientStrength = 0.05f;
	float diffuseStrength = max(dot(normal, lightDir), 0.0);
	float specularStrength = pow(max(dot(viewDir, reflectDir), 0.0f), material.shininess);

	float attenuation = 1.0f / (1.0f + 0.01f*pow(length(light.position - fragPos), 2));

	float theta = dot(lightDir, normalize(-light.direction));
	float epsilon = light.cutOff - light.outerCutOff;
	float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0f, 1.0f);

	vec3 ambient = light.ambient * material.ambient * ambientStrength;
	vec3 diffuse = light.diffuse * material.diffuse * diffuseStrength;
	vec3 specular = light.specular * material.specular * specularStrength;

	ambient *= attenuation * intensity;
	diffuse *= attenuation * intensity;
	specular *= attenuation * intensity;

	return vec3(ambient + diffuse + specular);
}


vec3 CalcAreaLight(Material material, AreaLight light)
{
	// return vec3(0.0f, 0.0f, 0.0f);
	return vec3(2*material.ambient);
}



















// #version 130

// in vec3 FragPos;
// in vec3 Normal;

// uniform vec3 lightPos;
// uniform vec3 lightColor;
// uniform vec3 objectColor;
// uniform vec3 cameraPos;

// void main(void) {
// 	vec3 lightDir = normalize(lightPos - FragPos);

// 	float ambientStrength = 0.1f;
// 	float diffuseStrength = max(dot(Normal, lightDir), 0.0);
// 	float specularStrength = 1.0f;

// 	vec3 viewDir = normalize(cameraPos - FragPos);
// 	vec3 reflectDir = reflect(-lightDir, Normal);

// 	float spec = pow(max(dot(viewDir, reflectDir), 0.0f), 32);

// 	vec3 ambient = ambientStrength * lightColor;
// 	vec3 diffuse = diffuseStrength * lightColor;
// 	vec3 specular = specularStrength * spec * lightColor;

// 	float attenuation = 1.0f / (1.0f + 0.01f*pow(length(lightPos - FragPos), 2));

// 	vec3 result = ambient + attenuation * (diffuse + specular);

// 	result = result * objectColor;

// 	gl_FragColor = vec4(result, 1.0f);
// }