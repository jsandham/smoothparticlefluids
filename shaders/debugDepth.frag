#version 130

out vec4 FragColor;

in vec2 TexCoord;

uniform sampler2D depthMapTex;

void main()
{
	float depthValue = texture(depthMapTex, TexCoord).r;
    FragColor = vec4(vec3(depthValue), 1.0);
}