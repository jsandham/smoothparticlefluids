#include "Light.h"


DirectionalLight::DirectionalLight()
{
	direction = glm::vec3(-2.0f, 4.0f, 1.0f);
	ambient = glm::vec3(0.1f, 0.1f, 0.1f);
	diffuse = glm::vec3(1.0f, 1.0f, 1.0f);
	specular = glm::vec3(1.0f, 1.0f, 1.0f);
}

LightType DirectionalLight::getType()
{
	return DIRECTIONAL;
}


PointLight::PointLight()
{
	constant = 1.0f;
	linear = 0.1f;
	quadratic = 0.032f;
	position = glm::vec3(0.0f, 0.0f, 0.0f);
	ambient = glm::vec3(0.1f, 0.1f, 0.1f);
	diffuse = glm::vec3(1.0f, 1.0f, 1.0f);
	specular = glm::vec3(1.0f, 1.0f, 1.0f);
}


LightType PointLight::getType()
{
	return POINT;
}


SpotLight::SpotLight()
{
	constant = 1.0f;
	linear = 0.1f;
	quadratic = 0.032f;
	cutOff = glm::cos(glm::radians(12.5f));
	outerCutOff = glm::cos(glm::radians(15.0f));
	position = glm::vec3(0.0f, 0.0f, 0.0f);
	direction = glm::vec3(0.0f, 0.0f, 0.0f);
	ambient = glm::vec3(0.1f, 0.1f, 0.1f);
	diffuse = glm::vec3(1.0f, 1.0f, 1.0f);
	specular = glm::vec3(1.0f, 1.0f, 1.0f);
}

LightType SpotLight::getType()
{
	return SPOT;
}


AreaLight::AreaLight()
{
	position = glm::vec3(0.0f, 0.0f, 0.0f);
	ambient = glm::vec3(0.1f, 0.1f, 0.1f);
	diffuse = glm::vec3(1.0f, 1.0f, 1.0f);
	specular = glm::vec3(1.0f, 1.0f, 1.0f);
}

LightType AreaLight::getType()
{
	return AREA;
}
