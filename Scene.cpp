#include <string>
#include <sstream>
#include <iomanip>
#include <cstddef>

#include <cuda.h>
#include <cuda_runtime.h>

#include "Scene.h"


Scene::Scene()
{
	sceneName = "SPH Physics Engine";

	setDefaultConfig();

	allocateWindow();
}


Scene::Scene(std::string name)
{
	sceneName = name;

	setDefaultConfig();

	allocateWindow();
}


Scene::Scene(const Scene &scene)
{
	sceneName = scene.sceneName;

	setDefaultConfig();

	allocateWindow();
}


Scene& Scene::operator=(const Scene &scene)
{
	if(this != &scene)
	{
		deallocateWindow();

		sceneName = scene.sceneName;

		setDefaultConfig();

		allocateWindow();
	}

	return *this;
}


Scene::~Scene()
{
	deallocateWindow();
}


void Scene::loadScene(std::string filename)
{

}


void Scene::add(std::string name, DirectionalLight *light)
{
	directionalLights[name] = light;
}


void Scene::add(std::string name, PointLight *light)
{
	pointLights[name] = light;
}


void Scene::add(std::string name, SpotLight *light)
{
	spotLights[name] = light;
}	


void Scene::add(std::string name, AreaLight *light)
{
	areaLights[name] = light;
}


void Scene::add(std::string name, Particles *particle)
{
	particles[name] = particle;
}


void Scene::add(std::string name, Fluid *fluid)
{
	fluids[name] = fluid;
}


void Scene::add(std::string name, RigidBody *rigidBody)
{
	rigidBodies[name] = rigidBody;	
}


void Scene::remove(std::string name)
{
	rigidBodies.erase(name);
}


bool Scene::contains(std::string name)
{
	return rigidBodies.count(name) == 1;
}


int Scene::size()
{
	return rigidBodies.size();
}


void Scene::run()
{
	start();

	while(isWindowOpen()){
		processEvents();

		startTime = std::clock();		

		updatePhysics();

		physicsEndTime = std::clock();

		// processEvents();

		updateScene();

		renderStartTime = std::clock();

		render();

		endTime = std::clock();
		// if(isSimulationOn){
		if(true){
			totalElapsedFrames++;
			totalElapsedTime += (endTime - startTime) / (double)CLOCKS_PER_SEC;
			timePerFrame = 1000*(endTime - startTime) / (double)CLOCKS_PER_SEC;
			physicsTimePerFrame = 1000*(physicsEndTime - startTime) / (double)CLOCKS_PER_SEC;
			renderTimePerFrame = 1000*(endTime - renderStartTime) / (double)CLOCKS_PER_SEC;
			fps = 1.0f / ((endTime - startTime) / (double)CLOCKS_PER_SEC);
		}
		openglFrame++;

		updateTimer();
	}
}


void Scene::start()
{
	std::map<std::string, Particles*>::iterator itp;
	std::map<std::string, Fluid*>::iterator itf;
	std::map<std::string, RigidBody*>::iterator itr;

	grid->start();

	// run through all particles, call start, and pass to physics engine
	for(itp = particles.begin(); itp != particles.end(); itp++){
		Particles *particles = itp->second;

		particles->start();

		physicsEngine->add(itp->first, itp->second);
	}

	// run through all fluids, call start, and pass to physics engine
	for(itf = fluids.begin(); itf != fluids.end(); itf++){
		Fluid *fluid = itf->second;

		fluid->start();

		physicsEngine->add(itf->first, itf->second);
	}

	// run through all rigid bodies, call start, and pass to physics engine
	for(itr = rigidBodies.begin(); itr != rigidBodies.end(); itr++){
		RigidBody *rigidBody = itr->second;

		rigidBody->start();

		physicsEngine->add(itr->first, itr->second);
	}

	// initialize physics engine
	physicsEngine->init();

	// pass lights to renderer
	std::map<std::string, DirectionalLight*>::iterator it1;
	for(it1 = directionalLights.begin(); it1 != directionalLights.end(); it1++){
		renderer->add(it1->second);
	}

	std::map<std::string, PointLight*>::iterator it2;
	for(it2 = pointLights.begin(); it2 != pointLights.end(); it2++){
		renderer->add(it2->second);
	}

	std::map<std::string, SpotLight*>::iterator it3;
	for(it3 = spotLights.begin(); it3 != spotLights.end(); it3++){
		renderer->add(it3->second);
	}

	std::map<std::string, AreaLight*>::iterator it4;
	for(it4 = areaLights.begin(); it4 != areaLights.end(); it4++){
		renderer->add(it4->second);
	}

	// pass render data to renderer
	renderer->add("grid", &grid->getRenderData());

	for(itp = particles.begin(); itp != particles.end(); itp++){
		Particles *particles = itp->second;
		renderer->add(itp->first, &particles->getRenderData());
	}

	for(itf = fluids.begin(); itf != fluids.end(); itf++){
		Fluid *fluid = itf->second;
		renderer->add(itf->first, &fluid->getRenderData());
	}
	
	for(itr = rigidBodies.begin(); itr != rigidBodies.end(); itr++){
		RigidBody *rigidBody = itr->second;
		renderer->add(itr->first, &rigidBody->getRenderData());
	}

	// initialize renderer (loads textures etc)
	renderer->init();
}


void Scene::updatePhysics()
{
	// physicsEngine->update();
} 


void Scene::processEvents()
{
	sf::Event event;
	while(window->pollEvent(event)){
		camera->processEvents(event);

		if(event.type == sf::Event::Closed){
			window->close();
		}

		if(event.type == sf::Event::KeyPressed){
			if(event.key.code == sf::Keyboard::Escape){
				window->close();
			}
			else if(event.key.code == sf::Keyboard::P){
				//physicsEngine->pause();
			}
			else if(event.key.code == sf::Keyboard::Q){
				physicsEngine->restart();
			}

		}

		grid->processEvents(event);

		// run through all particles and process events for each one
		std::map<std::string, Particles*>::iterator itp;
		for(itp = particles.begin(); itp != particles.end(); itp++){
			Particles *particles = itp->second;

			particles->processEvents(event);
		}

		// run through all fluids and process events for each one
		std::map<std::string, Fluid*>::iterator itf;
		for(itf = fluids.begin(); itf != fluids.end(); itf++){
			Fluid *fluid = itf->second;

			fluid->processEvents(event);
		}

		// run through all rigid bodies and process events for each one
		std::map<std::string, RigidBody*>::iterator itr;
		for(itr = rigidBodies.begin(); itr != rigidBodies.end(); itr++){
			RigidBody *rigidBody = itr->second;

			rigidBody->processEvents(event);
		}
	}
}


void Scene::updateScene()
{
	glm::vec3 position = camera->getPosition();
	//std::cout << "camera position: " << position.x << " " << position.y << " " << position.z << std::endl;

	grid->setHeightOfCamera(camera->getHeight());
	grid->update();
}


void Scene::render()
{
	window->setActive(true);
	
	renderer->view = camera->getViewMatrix();

	std::map<std::string, Particles*>::iterator itp;
	for(itp = particles.begin(); itp != particles.end(); itp++){
		Particles *particles = itp->second;
		renderer->add(itp->first, &particles->getRenderData());
	}

	std::map<std::string, Fluid*>::iterator itf;
	for(itf = fluids.begin(); itf != fluids.end(); itf++){
		Fluid *fluid = itf->second;
		renderer->add(itf->first, &fluid->getRenderData());
	}

	// std::cout << "" << std::endl;
	// glm::vec3 position = camera->getPosition();
	// glm::vec3 front = camera->getFront();
	// glm::vec3 up = camera->getUp();
	// glm::vec3 right = camera->getRight();
	// std::cout << "position: " << position.x << " " << position.y << " " << position.z << std::endl;
	// std::cout << "front: " << front.x << " " << front.y << " " << front.z << std::endl;
	// std::cout << "up: " << up.x << " " << up.y << " " << up.z << std::endl;
	// std::cout << "right: " << right.x << " " << right.y << " " << right.z << std::endl;
	
	std::map<std::string, RigidBody*>::iterator itr;
	for(itr = rigidBodies.begin(); itr != rigidBodies.end(); itr++){
		RigidBody *rigidBody = itr->second;
		RenderData *data = &rigidBody->getRenderData();
		//int x = camera->checkSphereInFrustum(data->centre, data->radius);
		//std::cout << "sphere in frustum: " << x << std::endl; 

		if(camera->checkSphereInFrustum(data->centre, data->radius) != -1){
			renderer->add(itr->first, &rigidBody->getRenderData());
		}
		else{
			renderer->remove(itr->first);
		}
	}

	renderer->render();

    if(true){
    	window->pushGLStates();
		window->draw(timeText);
		window->draw(versionText);
		window->popGLStates();
    }

    window->display();

#if DEBUG
	debugWindow->setActive(true);

	RenderData frustumRenderData;
	frustumRenderData.drawUsage = GL_TRIANGLES;
 	frustumRenderData.drawMode = GL_DYNAMIC_DRAW;
	frustumRenderData.shader.compile("shaders/rigidbody.vs", "shaders/rigidbody.frag");

	frustumRenderData.positions = camera->getFrustumTriVertices();
	frustumRenderData.normals = camera->getFrustumTriNormals();
	frustumRenderData.model = glm::mat4(1.0f);

	renderer->view = debugCamera->getViewMatrix();

	renderer->add("frustum", &frustumRenderData);

	renderer->render();

	debugWindow->display();

	renderer->remove("frustum");
#endif
}


void Scene::setTimeText(std::string text)
{
	timeText.setString(text);
}


void Scene::setVersionText(std::string text)
{
	versionText.setString(text);
}


void Scene::setSceneName(std::string name)
{
	sceneName = name;
	window->setTitle(name);
}


void Scene::setCamera(glm::vec3 position, glm::vec3 front, glm::vec3 up)
{
	camera->setPosition(position);
	camera->setFront(front);
	camera->setUp(up);

	cameraPos = position;
	cameraFront = front;
	cameraUp = up;
}


bool Scene::isWindowOpen()
{
	return window->isOpen();
}


void Scene::allocateWindow()
{
	settings = new sf::ContextSettings(0,0,0,3,0,0);
	settings->depthBits = 24;
    settings->stencilBits = 8;
    std::string title = sceneName;
	window = new sf::RenderWindow(sf::VideoMode(1200, 1000, 32), title, sf::Style::Titlebar | sf::Style::Close, *settings);
	camera = new Camera(cameraPos, cameraFront, cameraUp);
	window->setFramerateLimit(60);

#if DEBUG
	debugWindow = new sf::RenderWindow(sf::VideoMode(600, 600, 32), "Debug window", sf::Style::Titlebar | sf::Style::Close);
	debugCamera = new Camera(glm::vec3(0.0, 0.0f, 18.0f), glm::vec3(0.0, 0.0, -1.0), glm::vec3(0.0, 1.0, 0.0));
	debugWindow->setFramerateLimit(60);
#endif

	if(!font.loadFromFile("external/fonts/arial.ttf")){
		std::cout << "Error: Could not load font" << std::endl;
	}

	timeText.setFont(font);
	timeText.setCharacterSize(14);
	timeText.setColor(sf::Color::White);
	timeText.setPosition(0,0);

	versionText.setFont(font);
	versionText.setCharacterSize(14);
	versionText.setColor(sf::Color::White);
	versionText.setPosition(0,window->getSize().y - 105);

	grid = new Grid("external/obj/square.obj");

	physicsEngine = new PhysicsEngine();

	renderer = new Renderer();

	renderer->view = camera->getViewMatrix();
	renderer->projection = camera->getProjMatrix();
	renderer->cameraPos = cameraPos;
}


void Scene::deallocateWindow()
{
	delete settings;
	delete window;
	delete camera;
	delete grid;
	delete physicsEngine;
	delete renderer;

#if DEBUG
	delete debugWindow;
	delete debugCamera;
#endif
}


void Scene::setDefaultConfig()
{
	cameraPos = glm::vec3(0.5, -2.0f, 1.5f);
	cameraFront = glm::vec3(0.0, 0.8, -0.4);
	cameraUp = glm::vec3(0.0, 1.0, 0.0);
	
	backgroundColor = glm::vec3(0.15f, 0.15f, 0.15f);

	openglFrame = 0;
	totalElapsedFrames = 0;
	totalElapsedTime = 0.0f;
	timePerFrame = 0.0f;
	physicsTimePerFrame = 0.0f;
	renderTimePerFrame = 0.0f;
	fps = 0.0f;
	// physicsCudaTimePerFrame = 0.0f;
	// renderCudaTimePerFrame = 0.0f;
}


void Scene::updateTimer()
{
	if(openglFrame%10==1){
		std::stringstream stream;
		stream << "Total elapsed frames: " << totalElapsedFrames << "\n";
		stream << "Total elapsed time: " << totalElapsedTime << " sec\n";
		stream << "Time per frame: " << timePerFrame << " ms/frame\n";
		stream << "Physics time per frame: " << physicsTimePerFrame << " ms/frame\n";
		stream << "Render time per frame: " << renderTimePerFrame << " ms/frame\n";
		stream << "FPS: " << fps << "\n";
		stream << "\n";

		// stream << "Fluid cuda kernel time: " << physicsCudaTimePerFrame << " ms/frame\n";
		// stream << "Render cuda kernel time: " << renderCudaTimePerFrame << " ms/frame\n";

		std::string s = stream.str();
		setTimeText(s);

		stream.str(std::string());
		stream.clear();

		// Get CUDA device properties
		cudaDeviceProp properties;
		cudaGetDeviceProperties(&properties,0);

		int driverVersion, runtimeVersion;

		cudaDriverGetVersion(&driverVersion);
    	cudaRuntimeGetVersion(&runtimeVersion);

		stream << "OpenGL verson: " << glGetString(GL_VERSION) << "\n";
		stream << "OpenGL shading language version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << "\n";
		stream << "OpenGL renderer: " << glGetString(GL_RENDERER) << "\n";
		stream << "OpenGL vendor: " << glGetString(GL_VENDOR) << "\n";
		stream << "CUDA driver/runtime version: " <<  driverVersion/1000 << "." << (driverVersion%100)/10 << "/" << runtimeVersion/1000 << "." << (runtimeVersion%100)/10 << "\n";
		stream << "Graphics card: " << properties.name << "\n";
		s = stream.str();
		setVersionText(s);
	}
}